### Properties {#properties}
The `channel_rayleigh` object contains the following properties:
 - `channel_rayleigh.name` 
 - `channel_rayleigh.channel_matrix` 
 - `channel_rayleigh.num_antennas_transmit` 
 - `channel_rayleigh.num_antennas_receive` 
 - `channel_rayleigh.array_transmit` 
 - `channel_rayleigh.array_receive` 
 - `channel_rayleigh.carrier_frequency` 
 - `channel_rayleigh.carrier_wavelength` 
 - `channel_rayleigh.propagation_velocity` 

### Methods {#methods}
The `channel_rayleigh` object contains the following methods:
 - [`channel_rayleigh.H`](#H)
 - [`channel_rayleigh.Nr`](#Nr)
 - [`channel_rayleigh.Nt`](#Nt)
 - [`channel_rayleigh.addlistener`](#addlistener)
 - [`channel_rayleigh.channel_rayleigh`](#channel_rayleigh)
 - [`channel_rayleigh.channel_realization`](#channel_realization)
 - [`channel_rayleigh.create`](#create)
 - [`channel_rayleigh.delete`](#delete)
 - [`channel_rayleigh.enforce_channel_energy_normalization`](#enforce_channel_energy_normalization)
 - [`channel_rayleigh.eq`](#eq)
 - [`channel_rayleigh.findobj`](#findobj)
 - [`channel_rayleigh.findprop`](#findprop)
 - [`channel_rayleigh.ge`](#ge)
 - [`channel_rayleigh.get_channel_matrix`](#get_channel_matrix)
 - [`channel_rayleigh.gt`](#gt)
 - [`channel_rayleigh.isvalid`](#isvalid)
 - [`channel_rayleigh.le`](#le)
 - [`channel_rayleigh.listener`](#listener)
 - [`channel_rayleigh.lt`](#lt)
 - [`channel_rayleigh.ne`](#ne)
 - [`channel_rayleigh.notify`](#notify)
 - [`channel_rayleigh.realization`](#realization)
 - [`channel_rayleigh.set_array_receive`](#set_array_receive)
 - [`channel_rayleigh.set_array_transmit`](#set_array_transmit)
 - [`channel_rayleigh.set_arrays`](#set_arrays)
 - [`channel_rayleigh.set_carrier_frequency`](#set_carrier_frequency)
 - [`channel_rayleigh.set_channel_matrix`](#set_channel_matrix)
 - [`channel_rayleigh.set_propagation_velocity`](#set_propagation_velocity)
 - [`channel_rayleigh.set_receive_array`](#set_receive_array)
 - [`channel_rayleigh.set_transmit_array`](#set_transmit_array)

#### channel_rayleigh.H {#H}
```
  H Returns the channel matrix.
 
  Usage:
   val = H()
 
  Returns:
   val: the channel matrix

Help for channel_rayleigh/H is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.Nr {#Nr}
```
  Nr Returns the number of receive antennas out of the channel.
 
  Usage:
   val = Nr()
 
  Returns:
   val: the number of receive antennas out of the channel

Help for channel_rayleigh/Nr is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.Nt {#Nt}
```
  Nt Returns the number of transmit antennas into the channel.
 
  Usage:
   val = Nt()
 
  Returns:
   val: the number of transmit antennas into the channel

Help for channel_rayleigh/Nt is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.addlistener {#addlistener}
```
 ADDLISTENER  Add listener for event.
    el = ADDLISTENER(hSource, Eventname, callbackFcn) creates a listener
    for the event named Eventname.  The source of the event is the handle 
    object hSource.  If hSource is an array of source handles, the listener
    responds to the named event on any handle in the array.  callbackFcn
    is a function handle that is invoked when the event is triggered.
 
    el = ADDLISTENER(hSource, PropName, Eventname, Callback) adds a 
    listener for a property event.  Eventname must be one of
    'PreGet', 'PostGet', 'PreSet', or 'PostSet'. Eventname can be
    a string scalar or character vector.  PropName must be a single 
    property name specified as string scalar or character vector, or a 
    collection of property names specified as a cell array of character 
    vectors or a string array, or as an array of one or more 
    meta.property objects.  The properties must belong to the class of 
    hSource.  If hSource is scalar, PropName can include dynamic 
    properties.
    
    For all forms, addlistener returns an event.listener.  To remove a
    listener, delete the object returned by addlistener.  For example,
    delete(el) calls the handle class delete method to remove the listener
    and delete it from the workspace.
 
    ADDLISTENER binds the listener's lifecycle to the object that is the 
    source of the event.  Unless you explicitly delete the listener, it is
    destroyed only when the source object is destroyed.  To control the
    lifecycle of the listener independently from the event source object, 
    use listener or the event.listener constructor to create the listener.
 
    See also LISTENER, EVENT.LISTENER, CHANNEL_RAYLEIGH, NOTIFY, DELETE, META.PROPERTY, EVENTS

Help for channel_rayleigh/addlistener is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/addlistener


```
[Back to methods](#methods)

#### channel_rayleigh.channel_rayleigh {#channel_rayleigh}
```
  CHANNEL Creates a MIMO channel object.
  
  Usage:
   obj = CHANNEL()
   obj = CHANNEL(name)
  
  Args:
   name: an optional name for the object
  
  Returns:
   obj: an object representing a MIMO channel

```
[Back to methods](#methods)

#### channel_rayleigh.channel_realization {#channel_realization}
```
  CHANNEL_REALIZATION Realizes a Rayleigh channel matrix
  where each entry in the channel matrix is drawn i.i.d. from a
  complex normal distribution.
 
  Usage:
   H = CHANNEL_REALIZATION()
  
  Returns:
   H: a channel matrix whose entries are Rayleigh-faded

```
[Back to methods](#methods)

#### channel_rayleigh.create {#create}
```
  CREATE Creates a channel object of a specific type.
 
  Usage:
   c = channel.create()
   c = channel.create(type)

Help for channel_rayleigh.create is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.delete {#delete}
```
 DELETE   Delete a handle object.
    The DELETE method deletes a handle object but does not clear the handle
    from the workspace.  A deleted handle is no longer valid.
 
    DELETE(H) deletes the handle object H, where H is a scalar handle.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/ISVALID, CLEAR

Help for channel_rayleigh/delete is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/delete


```
[Back to methods](#methods)

#### channel_rayleigh.enforce_channel_energy_normalization {#enforce_channel_energy_normalization}
```
  ENFORCE_CHANNEL_ENERGY_NORMALIZATION Normalizes the channel
  matrix so that its total energy (squared Frobenius norm) is
  equal to the size of the matrix (number of transmit antennas
  times number of receive antennas). This is equivalent to
  normalizing it such that the average power of any entry in
  the channel matrix is one.
 
  Usage:
   ENFORCE_CHANNEL_ENRGY_NORMALIZATION()

Help for channel_rayleigh/enforce_channel_energy_normalization is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.eq {#eq}
```
 == (EQ)   Test handle equality.
    Handles are equal if they are handles for the same object.
 
    H1 == H2 performs element-wise comparisons between handle arrays H1 and
    H2.  H1 and H2 must be of the same dimensions unless one is a scalar.
    The result is a logical array of the same dimensions, where each
    element is an element-wise equality result.
 
    If one of H1 or H2 is scalar, scalar expansion is performed and the 
    result will match the dimensions of the array that is not scalar.
 
    TF = EQ(H1, H2) stores the result in a logical array of the same 
    dimensions.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/GE, CHANNEL_RAYLEIGH/GT, CHANNEL_RAYLEIGH/LE, CHANNEL_RAYLEIGH/LT, CHANNEL_RAYLEIGH/NE

Help for channel_rayleigh/eq is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/eq


```
[Back to methods](#methods)

#### channel_rayleigh.findobj {#findobj}
```
 FINDOBJ   Find objects matching specified conditions.
    The FINDOBJ method of the HANDLE class follows the same syntax as the 
    MATLAB FINDOBJ command, except that the first argument must be an array
    of handles to objects.
 
    HM = FINDOBJ(H, <conditions>) searches the handle object array H and 
    returns an array of handle objects matching the specified conditions.
    Only the public members of the objects of H are considered when 
    evaluating the conditions.
 
    See also FINDOBJ, CHANNEL_RAYLEIGH

Help for channel_rayleigh/findobj is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/findobj


```
[Back to methods](#methods)

#### channel_rayleigh.findprop {#findprop}
```
 FINDPROP   Find property of MATLAB handle object.
    p = FINDPROP(H,PROPNAME) finds and returns the META.PROPERTY object
    associated with property name PROPNAME of scalar handle object H.
    PROPNAME can be a string scalar or character vector.  It can be the 
    name of a property defined by the class of H or a dynamic property 
    added to scalar object H.
   
    If no property named PROPNAME exists for object H, an empty 
    META.PROPERTY array is returned.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/FINDOBJ, DYNAMICPROPS, META.PROPERTY

Help for channel_rayleigh/findprop is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/findprop


```
[Back to methods](#methods)

#### channel_rayleigh.ge {#ge}
```
 >= (GE)   Greater than or equal relation for handles.
    H1 >= H2 performs element-wise comparisons between handle arrays H1 and
    H2.  H1 and H2 must be of the same dimensions unless one is a scalar.
    The result is a logical array of the same dimensions, where each
    element is an element-wise >= result.
 
    If one of H1 or H2 is scalar, scalar expansion is performed and the 
    result will match the dimensions of the array that is not scalar.
 
    TF = GE(H1, H2) stores the result in a logical array of the same 
    dimensions.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/EQ, CHANNEL_RAYLEIGH/GT, CHANNEL_RAYLEIGH/LE, CHANNEL_RAYLEIGH/LT, CHANNEL_RAYLEIGH/NE

Help for channel_rayleigh/ge is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/ge


```
[Back to methods](#methods)

#### channel_rayleigh.get_channel_matrix {#get_channel_matrix}
```
  GET_CHANNEL_MATRIX Returns the channel matrix.
 
  Usage:
   H = GET_CHANNEL_MATRIX()
  
  Returns:
   H: channel matrix

Help for channel_rayleigh/get_channel_matrix is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.gt {#gt}
```
 > (GT)   Greater than relation for handles.
    H1 > H2 performs element-wise comparisons between handle arrays H1 and 
    H2.  H1 and H2 must be of the same dimensions unless one is a scalar.  
    The result is a logical array of the same dimensions, where each
    element is an element-wise > result.
 
    If one of H1 or H2 is scalar, scalar expansion is performed and the 
    result will match the dimensions of the array that is not scalar.
 
    TF = GT(H1, H2) stores the result in a logical array of the same 
    dimensions.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/EQ, CHANNEL_RAYLEIGH/GE, CHANNEL_RAYLEIGH/LE, CHANNEL_RAYLEIGH/LT, CHANNEL_RAYLEIGH/NE

Help for channel_rayleigh/gt is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/gt


```
[Back to methods](#methods)

#### channel_rayleigh.isvalid {#isvalid}
```
 ISVALID   Test handle validity.
    TF = ISVALID(H) performs an element-wise check for validity on the 
    handle elements of H.  The result is a logical array of the same 
    dimensions as H, where each element is the element-wise validity 
    result.
 
    A handle is invalid if it has been deleted or if it is an element
    of a handle array and has not yet been initialized.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/DELETE

Help for channel_rayleigh/isvalid is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/isvalid


```
[Back to methods](#methods)

#### channel_rayleigh.le {#le}
```
 <= (LE)   Less than or equal relation for handles.
    Handles are equal if they are handles for the same object.  All 
    comparisons use a number associated with each handle object.  Nothing
    can be assumed about the result of a handle comparison except that the
    repeated comparison of two handles in the same MATLAB session will 
    yield the same result.  The order of handle values is purely arbitrary 
    and has no connection to the state of the handle objects being 
    compared.
 
    H1 <= H2 performs element-wise comparisons between handle arrays H1 and
    H2.  H1 and H2 must be of the same dimensions unless one is a scalar.
    The result is a logical array of the same dimensions, where each
    element is an element-wise >= result.
 
    If one of H1 or H2 is scalar, scalar expansion is performed and the 
    result will match the dimensions of the array that is not scalar.
 
    TF = LE(H1, H2) stores the result in a logical array of the same 
    dimensions.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/EQ, CHANNEL_RAYLEIGH/GE, CHANNEL_RAYLEIGH/GT, CHANNEL_RAYLEIGH/LT, CHANNEL_RAYLEIGH/NE

Help for channel_rayleigh/le is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/le


```
[Back to methods](#methods)

#### channel_rayleigh.listener {#listener}
```
 LISTENER  Add listener for event without binding the listener to the source object.
    el = LISTENER(hSource, Eventname, callbackFcn) creates a listener
    for the event named Eventname.  The source of the event is the handle  
    object hSource.  If hSource is an array of source handles, the listener
    responds to the named event on any handle in the array.  callbackFcn
    is a function handle that is invoked when the event is triggered.
 
    el = LISTENER(hSource, PropName, Eventname, callback) adds a 
    listener for a property event.  Eventname must be one of  
    'PreGet', 'PostGet', 'PreSet', or 'PostSet'. Eventname can be a 
    string sclar or character vector.  PropName must be either a single 
    property name specified as a string scalar or character vector, or 
    a collection of property names specified as a cell array of character 
    vectors or a string array, or as an array of one ore more 
    meta.property objects. The properties must belong to the class of 
    hSource.  If hSource is scalar, PropName can include dynamic 
    properties.
    
    For all forms, listener returns an event.listener.  To remove a
    listener, delete the object returned by listener.  For example,
    delete(el) calls the handle class delete method to remove the listener
    and delete it from the workspace.  Calling delete(el) on the listener
    object deletes the listener, which means the event no longer causes
    the callback function to execute. 
 
    LISTENER does not bind the listener's lifecycle to the object that is
    the source of the event.  Destroying the source object does not impact
    the lifecycle of the listener object.  A listener created with LISTENER
    must be destroyed independently of the source object.  Calling 
    delete(el) explicitly destroys the listener. Redefining or clearing 
    the variable containing the listener can delete the listener if no 
    other references to it exist.  To tie the lifecycle of the listener to 
    the lifecycle of the source object, use addlistener.
 
    See also ADDLISTENER, EVENT.LISTENER, CHANNEL_RAYLEIGH, NOTIFY, DELETE, META.PROPERTY, EVENTS

Help for channel_rayleigh/listener is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/listener


```
[Back to methods](#methods)

#### channel_rayleigh.lt {#lt}
```
 < (LT)   Less than relation for handles.
    H1 < H2 performs element-wise comparisons between handle arrays H1 and
    H2.  H1 and H2 must be of the same dimensions unless one is a scalar.
    The result is a logical array of the same dimensions, where each
    element is an element-wise < result.
 
    If one of H1 or H2 is scalar, scalar expansion is performed and the 
    result will match the dimensions of the array that is not scalar.
 
    TF = LT(H1, H2) stores the result in a logical array of the same 
    dimensions.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/EQ, CHANNEL_RAYLEIGH/GE, CHANNEL_RAYLEIGH/GT, CHANNEL_RAYLEIGH/LE, CHANNEL_RAYLEIGH/NE

Help for channel_rayleigh/lt is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/lt


```
[Back to methods](#methods)

#### channel_rayleigh.ne {#ne}
```
 ~= (NE)   Not equal relation for handles.
    Handles are equal if they are handles for the same object and are 
    unequal otherwise.
 
    H1 ~= H2 performs element-wise comparisons between handle arrays H1 
    and H2.  H1 and H2 must be of the same dimensions unless one is a 
    scalar.  The result is a logical array of the same dimensions, where 
    each element is an element-wise equality result.
 
    If one of H1 or H2 is scalar, scalar expansion is performed and the 
    result will match the dimensions of the array that is not scalar.
 
    TF = NE(H1, H2) stores the result in a logical array of the same
    dimensions.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/EQ, CHANNEL_RAYLEIGH/GE, CHANNEL_RAYLEIGH/GT, CHANNEL_RAYLEIGH/LE, CHANNEL_RAYLEIGH/LT

Help for channel_rayleigh/ne is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/ne


```
[Back to methods](#methods)

#### channel_rayleigh.notify {#notify}
```
 NOTIFY   Notify listeners of event.
    NOTIFY(H, eventname) notifies listeners added to the event named 
    eventname for handle object array H that the event is taking place. 
    eventname can be a string scalar or character vector.  
    H is the array of handles to the event source objects, and 'eventname'
    must be a character vector.
 
    NOTIFY(H,eventname,ed) provides a way of encapsulating information 
    about an event which can then be accessed by each registered listener.
    ed must belong to the EVENT.EVENTDATA class.
 
    See also CHANNEL_RAYLEIGH, CHANNEL_RAYLEIGH/ADDLISTENER, CHANNEL_RAYLEIGH/LISTENER, EVENT.EVENTDATA, EVENTS

Help for channel_rayleigh/notify is inherited from superclass HANDLE

    Reference page in Doc Center
       doc channel_rayleigh/notify


```
[Back to methods](#methods)

#### channel_rayleigh.realization {#realization}
```
  REALIZATION Realizes a Rayleigh channel matrix where each 
  entry in the channel matrix is drawn i.i.d. from a complex 
  normal distribution.
 
  Usage:
   H = REALIZATION()
  
  Returns:
   H: a channel matrix whose entries are Rayleigh-faded

```
[Back to methods](#methods)

#### channel_rayleigh.set_array_receive {#set_array_receive}
```
  SET_ARRAY_RECEIVE Sets the receive array object. Also sets
  the number of receive antennas accordingly.
  
  Usage:
   SET_ARRAY_RECEIVE(array)
  
  Args:
   array: an array object

Help for channel_rayleigh/set_array_receive is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.set_array_transmit {#set_array_transmit}
```
  SET_ARRAY_TRANSMIT Sets the transmit array object. Also sets
  the number of transmit antennas accordingly.
  
  Usage:
   SET_ARRAY_TRANSMIT(array)
  
  Args:
   array: an array object

Help for channel_rayleigh/set_array_transmit is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.set_arrays {#set_arrays}
```
  SET_ARRAYS Sets the transmit and receive arrays at the input
  and output of the channel.
 
  Usage:
   SET_ARRAYS(array_transmit,array_receive)
 
  Args:
   array_transmit: an array object at the channel input
   array_receive: an array object at the channel output

Help for channel_rayleigh/set_arrays is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.set_carrier_frequency {#set_carrier_frequency}
```
  SET_CARRIER_FREQUENCY Sets the carrier frequency of the
  channel.
 
  Usage:
   SET_CARRIER_FREQUENCY(fc)
 
  Args:
   fc: carrier frequency (Hz)
 
  Notes:
   Also updates carrier wavelength.

Help for channel_rayleigh/set_carrier_frequency is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.set_channel_matrix {#set_channel_matrix}
```
  SET_CHANNEL_MATRIX Sets the channel matrix.
 
  Usage:
   SET_CHANNEL_MATRIX(H)
  
  Args:
   H: channel matrix

Help for channel_rayleigh/set_channel_matrix is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.set_propagation_velocity {#set_propagation_velocity}
```
  SET_PROPAGATION_VELOCITY Sets the propagation velocity of the
  channel.
 
  Usage:
   SET_PROPAGATION_VELOCITY(val)
 
  Args:
   val: propagation velocity (meters/sec)

Help for channel_rayleigh/set_propagation_velocity is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.set_receive_array {#set_receive_array}
```
  SET_RECEIVE_ARRAY Sets the receive array object. Also sets
  the number of receive antennas accordingly (LEGACY).
  
  Usage:
   SET_RECEIVE_ARRAY(array)
  
  Args:
   array: an array object

Help for channel_rayleigh/set_receive_array is inherited from superclass CHANNEL

```
[Back to methods](#methods)

#### channel_rayleigh.set_transmit_array {#set_transmit_array}
```
  SET_TRANSMIT_ARRAY Sets the transmit array object. Also sets
  the number of transmit antennas accordingly (LEGACY).
  
  Usage:
   SET_TRANSMIT_ARRAY(array)
  
  Args:
   array: an array object

Help for channel_rayleigh/set_transmit_array is inherited from superclass CHANNEL

```
[Back to methods](#methods)

