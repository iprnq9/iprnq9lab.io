---
layout: doc
title:  "Array"
object: array
file: obj/array/array.m
summary: An arbitrary antenna array of one or more elements.
super: true
---

#### On This Page
- [About](#about)
- [Video Tutorial](#video)
- [Creating an Array](#creating-array)
- [Viewing the Array](#viewing-array)
- [Adding and Removing Elements](#adding-removing-elements)
- [Modifying the Array](#modifying-array)
- [Getting the Array Response](#getting-array-response)
- [Weighting the Array](#weighting-array)
- [Array Gain](#array-gain)
- [Getting the Weighted Array Response](#getting-weighted-array-response)
- [Viewing the Array Pattern](#viewing-array-pattern)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `array` object is used to represent an antenna array. Currently, MFM only supports isotropic antenna elements.

{% include image.html file="obj/array/array_with_weights.svg" %}

An antenna array is largely characterized by the relative geometry of its elements. 
MFM's `array` object is not restricted to creating uniform linear arrays or uniform planar arrays; the array's elements _**can be placed arbitrarily**_, and MFM will automatically compute the array response based on their placement. This means users can easily experiment with abnormal/arbitrary array designs.


### Video Tutorial {#video}

{% include youtube.html id='cA9QYVOoSOU' %}


### Creating an Array {#creating-array}

There are a few different ways to create an `array` object. 


#### Empty Array

To create an empty array object, you can simply call

```matlab
a = array.create()
```


#### Uniform Linear Array

To create an `N`-element, half-wavelength spaced uniform linear array, use

```matlab
a = array.create(N,ax)
```

where `ax` is either `'x'` (default), `'y'`, or `'z'` specifying which axis to create the ULA along.

{% include image.html file="obj/array/array_show_3d_ula_8.svg" caption="An 8-element uniform linear array created with MFM." %}


#### Uniform Planar Array

To create a half-wavelength spaced uniform planar array with `N` rows of `M` elements, use

```matlab
a = array.create(N,M,plane)
```

where `plane` is either `'xz'` (default), `'xy'`, or `'yz'`, defining which plane to create the array in.

{% include image.html file="obj/array/array_show_3d_upa_8_8.svg" caption="An 8-by-8 uniform planar array created with MFM." %}

By default, ULAs and UPAs are centered at the origin when created.


### Viewing the Array {#viewing-array}

To view the array elements in 2-D space (in the x-z plane by default), use

```matlab
a.show_2d()
```

or

```matlab
a.show_2d([],plane)
```

where `plane` is either `'xz'` (default), `'xy'`, or `'yz'` specifying which plane to plot the array in.

{% include image.html file="obj/array/array_show_2d_upa_8_8.svg" caption="An 8-by-8 uniform planar array shown in 2-D space (x-z plane)." %}

To view the array elements in 3-D space, use

```matlab
a.show_3d()
```

{% include image.html file="obj/array/array_show_3d_upa_8_8.svg" caption="An 8-by-8 uniform planar array shown in 3-D space." %}


### Adding and Removing Elements {#adding-removing-elements}

To add elements to an `array` object `a`, use

```matlab
a.add_element(x,y,z)
```

where `x`, `y`, and `z` are vectors of (x,y,z) coordinates of the elements (in wavelengths) to add to the array.

Since antenna arrays are characterized by the elements' positions relative to the carrier frequency, all Cartesian coordinates associated with an array are in units of carrier wavelengths.

To remove an element from the `array` object `a`, use

```matlab
a.remove_element(idx)
```

where `idx` is the index of the element to remove. If `idx` is not passed, the last element of the array will be removed.


### Modifying the Array {#modifying-array}

To shift all elements of an `array` object `a`, use

```matlab
a.translate(x,y,z)
```

where `x`, `y`, and `z` are the amounts (in wavelengths) to move all the array elements in the x, y, and z directions.

If `x`, `y`, and `z` are not passed, then the following

```matlab
a.translate()
```

centers the array at the origin.

{% include image.html file="obj/array/array_show_3d_ula_8_translated_10_10_10.svg" caption="An 8-element uniform linear array translated by 10 wavelengths along all three axes." %}


To rotate the entire `array` object `a` about the three axes, use

```
a.rotate(rot_x,rot_y,rot_z)
```

where `rot_x`, `rot_y`, and `rot_z` are rotations to make along the x-, y-, and z-axes, respectively, in radians.

{% include image.html file="obj/array/array_show_3d_upa_8_8_rotated_0_0_-pi4.svg" caption="An 8-by-8 uniform planar array rotated by -45 deg. along the z-axis." %}


### Geting the Array Response {#getting-array-response}

The relative phase shift experienced by the $$i$$-th array element located at some $$(x_i,y_i,z_i)$$ from the origin due to a plane wave in the direction $$(\theta,\phi)$$ is

$$
\begin{align}
a_i(\theta,\phi) = \exp \left(\mathrm{j} \cdot \frac{2 \pi}{\lambda} \cdot \zeta(x_i,y_i,z_i,\theta,\phi)\right)
\end{align}
$$

where $$\lambda$$ is the carrier wavelength and

$$
\begin{align}
\zeta(x,y,z,\theta,\phi) = x \sin \theta \cos \phi + y \cos \theta \cos \phi + z \sin \phi

\end{align}
$$

{% include image.html file="obj/array/array_response.svg" %}

Instead of referencing the true origin to compute the array response, MFM refers the location of each antenna element to that of the first element in the antenna array.
Thus, the array response vector is constructed by collecting the relative phase shift seen by each of the array's $$N$$ elements as

$$
\begin{align}
\mathbf{a}(\theta,\phi) = 
\begin{bmatrix}
a_1(\theta,\phi) \\
a_2(\theta,\phi) \\
\vdots \\
a_{N}(\theta,\phi) \\
\end{bmatrix}
\cdot
\frac{1}{a_1(\theta,\phi)}
\end{align}
$$

To obtain the array response---the relative phase shift across elements---at a particular azimuth angle and elevation angle, use

```matlab
v = a.get_array_response(az,el)
```

where `az` and `el` are azimuth and elevation angles in radians and `v` is the consequent array response (a column vector). MFM will compute the array response behind-the-scenes.


### Weighting the Array {#weighting-array}

To set the array weights, use

```matlab
a.set_weights(w)
```

where `w` is a vector of complex weights where the `i`-th array element is weighted by the `i`-th weight in `w`.

{% include image.html file="obj/array/array_weights.svg" %}


### Array Gain {#array-gain}

The array gain $$g$$ in a particular direction can be described mathematically by stating that the gain of an array with weights $\mathbf{w}$ in the direction $$(\theta,\phi)$$ is

$$
\begin{align}
g(\theta,\phi) = \mathbf{w}^{\mathrm{T}} \mathbf{a}(\theta,\phi)
\end{align}
$$

where $$(\cdot)^{\mathrm{T}}$$ denotes the transpose operation (not conjugate transpose).

{% include image.html file="obj/array/array_gain.svg" %}

In MFM, to evaluate the gain of the array (with weights applied) in a particular azimuth and elevation, use

```matlab
g = a.get_array_gain(az,el)
```

where `az` and `el` are azimuth and elevation angles in radians and `g` is the complex gain of the weighted array.


### Geting the Weighted Array Response {#getting-weighted-array-response}

The array response vector _**with array weights applied**_ is simply the array response vector multiplied element-wise by the array weights

$$
\mathbf{a}_{\mathbf{w}}(\theta,\phi) = \mathbf{a}(\theta,\phi) \odot \mathbf{w}
$$

{% include image.html file="obj/array/array_weighted_response.svg" %}

To retrieve the weighted array response, simply use 

```
v = a.get_weighted_array_response(az,el)
```

where `az` and `el` are azimuth and elevation angles in radians and `v` is the consequent weighted array response (a column vector).

By using this, the array weights can be used more broadly than for beamforming---e.g., for capturing phase or gain inconsistencies across array elements.


### Viewing the Array Pattern {#viewing-array-pattern}

The `array` object has several functions that can be used to view its array pattern---the array response (magnitude and phase) as a function of azimuth angle and elevation angle.

Note that these all plot the _**weighted**_ array pattern, meaning they capture any weights set to the array.

#### Azimuth and Elevation Cuts

To view the magnitude and phase of the array response as a function of azimuth angle and elevation angle, use
 
```matlab
a.show_array_pattern()
```

or
 
```matlab
a.show_array_pattern(az,el)
```

where `el` is the elevation angle (in radians) to use when evaluating the azimuth pattern and `az` is the azimuth angle (in radians) to use when evaluating the elevation pattern.

{% include image.html file="obj/array/array_show_array_pattern_upa_8_8.svg" caption="The array pattern of an 8-by-8 uniform planar array." %}

#### Azimuth Cut

To view the magnitude of the array response as a function of azimuth angle alone, use

```matlab
a.show_array_pattern_azimuth()
```

or

```matlab
a.show_array_pattern_azimuth([],el)
```

where `el` is the elevation angle (in radians) to use when evaluating the azimuth pattern.

{% include image.html file="obj/array/array_show_array_pattern_azimuth_upa_8_8.svg" caption="The magnitude of the azimuth array pattern of an 8-by-8 uniform planar array." %}

#### Elevation Cut

To view the magnitude of the array response as a function of elevation angle alone, use

```matlab
a.show_array_pattern_elevation()
```

or

```matlab
a.show_array_pattern_elevation([],az)
```

where `az` is the azimuth angle (in radians) to use when evaluating the elevation pattern.

{% include image.html file="obj/array/array_show_array_pattern_elevation_upa_8_8.svg" caption="The magnitude of the elevation array pattern of an 8-by-8 uniform planar array." %}

#### Azimuth Cut and Elevation Cut in a Polar Plot

To view the array patterns _**in a polar plot**_, use

```matlab
a.show_polar_array_pattern_azimuth()
a.show_polar_array_pattern_elevation()
```

or

```matlab
a.show_polar_array_pattern_azimuth([],el)
a.show_polar_array_pattern_elevation([],az)
```

which shows the magnitude of the array response as a function of azimuth angle and of elevation angle.

{% include image.html file="obj/array/array_show_polar_array_pattern_azimuth_ula_8.svg" caption="The magnitude of the azimuth array pattern of an 8-element uniform linear array steered toward 30 degrees." %}


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}

