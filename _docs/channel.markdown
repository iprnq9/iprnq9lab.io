---
layout: doc
title:  "Channel"
object: channel
file: obj/channel/channel.m
summary: A generic MIMO channel.
super: true
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "superclass", page.object %}
{% unless subs == empty %}Subclasses:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Video Tutorial](#video)
- [Creating a Channel](#creating)
- [Key Properties](#key-properties)
- [Setting the Transmit and Receive Arrays](#setting-arrays)
- [Setting the Carrier Frequency](#setting-carrier-frequency)
- [Setting the Propagation Velocity](#setting-propagation-velocity)
- [Forcing Channel Energy Normalization](#forcing-energy-normalization)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `channel` object represents a generic MIMO channel. It does not hold any value in representing a particular channel model but rather acts as the foundation for all other channel models within MFM.

{% include image.html file="obj/channel/channel.svg"  %}

MFM currently only supports frequency-flat channels, meaning the channel can be largely characterized by a single matrix $$\mathbf{H}$$. Support for frequency-selectivity may exist in future releases of MFM.


### Video Tutorial {#video}

{% include youtube.html id='tF_R-PSKFHg' %}


### Creating a Generic Channel {#creating}

While the `channel` object on its own isn't all that useful, it can be created via

```matlab
c = channel.create()
```

As mentioned, more useful channel models are subclasses of the `channel` object, meaning they will inherit the properties and methods of the `channel` object. Moreover, the `channel` object includes some of the key setup functions that most---if not all---other channel objects will use.


### Key Properties {#key-properties}

The `channel` object (and its subclasses) contain the following important properties.

Each `channel` requires knowledge of the transmit array at the channel input, which is captured in

```matlab
c.array_transmit
```

Similarly, the receive array at the channel output is captured in

```matlab
c.array_receive
```

The number of antennas at the transmit array and receive array, respectively, are captured by the properties

```matlab
c.num_antennas_transmit
c.num_antennas_receive
```

The carrier frequency (in Hertz), carrier wavelength (in wavelengths), and propagation velocity (in meters per second) of the channel---more accurately, of the signals we are interested in---are captured by

```matlab
c.carrier_frequency
c.carrier_wavelength
c.propagation_velocity
```

While not all channel models require knowledge of these three properties (e.g., a Rayleigh-faded channel), several channels do and thus, for simplicity, MFM includes them in the generic `channel` object.

Finally, the channel matrix $$\mathbf{H}$$ is stored in 

```matlab
c.channel_matrix
```

### Setting the Transmit and Receive Arrays {#setting-arrays}

To set the transmit and receive arrays of a `channel` object `c`, simply use

```matlab
c.set_arrays(atx,arx)
```

where `atx` and `arx` are `array` objects corresponding to the antenna arrays at the channel input and output, respectively.

To set the transmit and receive arrays individually, one can use

```matlab
c.set_array_transmit(atx)
c.set_array_receive(arx)
```

Upon setting the transmit array and receive array, the number of transmit antennas (`c.num_antennas_transmit`) and the number of receive antennas (`c.num_antennas_receive`) are automatically updated based on the number of elements in the `array` objects.

While the channel is aware of the transmit and receive arrays, it is important to note that the channel represents solely the _**over-the-air**_ propagation and does not account for any phase profiles inherent to the arrays themselves. At the very least, the `channel` object will merely take note of the number of antennas at the transmit array and receive array. Some channel models (e.g., the ray/cluster channel model) use the transmit and receive `array` objects to obtain array response vectors.


### Setting the Carrier Frequency {#setting-carrier-frequency}

To set the carrier frequency of the channel, use

```matlab
c.set_carrier_frequency(fc)
```

where `fc` is the carrier frequency (in Hz) of the signals into the channel. This will automatically update the carrier wavelength based on the current propagation velocity.


### Setting the Propagation Velocity {#setting-propagation-velocity}

To set the propagation velocity of the channel, use

```matlab
c.set_propagation_velocity(vel)
```

where `vel` is the propagation velocity (in m/s) of the signals into the channel.

It is common to use `vel = 3e8` for electromagnetic signals, for example. 

Since this parameter can be set by the user, MFM may support other communication systems and/or other propagation media, such as underwater acoustics where the propagation velocity is commonly approximated to `vel = 1.5e3` for oceanic environments.


### Example Setup {#example-setup}

A typical `channel` object setup looks something similar to

```matlab
c = channel.create()
c.set_carrier_frequency(fc)
c.set_propagation_velocity(vel)
c.set_arrays(atx,arx)
```


### Forcing Channel Energy Normalization {#forcing-energy-normalization}

Sometimes, channel matrices are normalized to a specific energy (Frobenius norm squared). To force MFM to normalize channel matrices to a fixed value, use

```
c.set_force_channel_energy_normalization(true)
```

and use 

```
c.set_normalized_channel_energy(val)
```

where `val` is the desired Frobenius norm squared the channel matrices. If not set, the normalized channel energy is equal to the number of elements in the channel matrix (i.e., the product of the number of transmit antennas and the number of receive antennas).

With this, all realized channel matrices $$\mathbf{H}$$ will satisfy $$\lVert\mathbf{H}\rVert_{\mathrm{F}}^2 =$$ `val`.

By default channels do not normalize the channel matrices. 


### Shorthand Methods {#shorthand-methods}

To provide convenient ways to retrieve common MIMO-related quantities from a `channel` object `c`, there exist the following so-called shorthand methods.

- `c.H` --- Returns the channel matrix.
- `c.Nt` --- Returns the number of transmit antennas at the channel input.
- `c.Nr` --- Returns the number of receive antennas at the channel output.


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
