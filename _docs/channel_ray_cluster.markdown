---
layout: doc
title:  "Ray/Cluster Channel"
object: channel_ray_cluster
superclass: channel
category: channel
summary: A channel commprised of the combination of discrete rays.
file: obj/channel/channel_ray_cluster.m
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "object", page.superclass %}
{% unless subs == empty %}Superclass:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Video Tutorial](#video)
- [Creating a Ray/Cluster Channel](#creating)
- [Key Properties](#key-properties)
- [Setting the AoD and AoA Ranges](#setting-aod-aoa-ranges)
- [Setting the AoD and AoA Distribution](#setting-aod-aoa-distribution)
- [Snapping the AoDs and AoAs](#snapping-aod-aoa)
- [Invoking a Channel Realization](#realization)
- [Viewing the Beamspace](#viewing-beamspace)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

A ray/cluster channel is based on the Saleh-Valenzuela channel model, where the channel is comprised of the combination of a discrete set of rays. This is a common model for millimeter-wave channels, for example, which exhibit high reflectivity and low diffusion.

Mathematically, a ray/cluster channel matrix can be expressed as

$$
\mathbf{H} = \sum_{v=1}^{N_{\mathrm{clust}}} \sum_{u=1}^{N_{\mathrm{rays}}} \beta_{u,v} \mathbf{a}_{\mathrm{rx}}(\mathrm{AoA}_{u,v}) \mathbf{a}_{\mathrm{tx}}(\mathrm{AoD}_{u,v})^{*}
$$

where the channel is comprised of $$N_{\mathrm{clust}}$$ clusters, each having $$N_{\mathrm{rays}}$$ rays. The $$u$$-th ray in the $$v$$-th cluster exhibits some complex gain $$\beta_{u,v}$$ and has some angle of departure (from the transmit array) $$\mathrm{AoD}_{u,v}$$ and angle of arrival (at the receive array) $$\mathrm{AoA}_{u,v}$$. The consequent transmit and receive array responses are then $$\mathbf{a}_{\mathrm{tx}}(\mathrm{AoD}_{u,v})$$ and $$\mathbf{a}_{\mathrm{rx}}(\mathrm{AoA}_{u,v})$$, respectively.

The `channel_ray_cluster` object represents a ray/cluster channel model and is a subclass of the `channel` object.


### Video Tutorial {#video}

{% include youtube.html id='mURBHQgOCuw' %}


### Creating a Ray/Cluster Channel {#creating}

To create a ray/cluster channel, simply use

```
c = channel.create('ray-cluster')
```

where `c` is a `channel_ray_cluster` object.


### Key Properties

The `channel_ray_cluster` object is a subclass of the `channel` object and thus inherits all of its properties and methods.

The key properties it inherits are 

```matlab
c.array_transmit
c.array_receive
c.num_antennas_transmit
c.num_antennas_receive
c.carrier_frequency
c.carrier_wavelength
c.propagation_velocity
c.channel_matrix
```

In addition to these, it also contains the following properties.

The number of clusters and number of rays per cluster are stored in

```
c.num_clusters
c.num_rays_per_cluster
```

The AoDs and AoAs are stored in the 3-D arrays (tensors)

```
c.AoD
c.AoA
```

where the $$(i,j,1)$$-th element of each corresponds to the _**azimuth**_ AoD/AoA of the $$j$$-th ray within the $$i$$-th cluster, and the $$(i,j,2)$$-th element of each corresponds to the _**elevation**_ AoD/AoA of the $$j$$-th ray within the $$i$$-th cluster.

The gain of the rays are stored in 

```
c.ray_gains
```

whose $$(i,j)$$-th element is the complex gain of the $$j$$-th ray within the $$i$$-th cluster.


### Setting the AoD and AoA Ranges {#setting-aod-aoa-ranges}

To set the range that AoDs and AoAs can take, use

```
c.set_AoD_range(az_min,az_max,el_min,el_max)
```

and 

```
c.set_AoA_range(az_min,az_max,el_min,el_max)
```

where `az_min` and `az_max` are the minimum and maximum azimuth AoD/AoA (in radians) and `el_min` `el_max` are the minimum and maximum elevation AoD/AoA (in radians).


### Setting the AoD and AoA Distribution {#setting-aod-aoa-distribution}

To generate the AoD and AoA of the $$j$$-th ray within the $$i$$-th cluster, MFM first draws a so-called cluster AoD and cluster AoA, which can be used when realizing the each of its rays' AoD and AoA.

Cluster AoDs and cluster AoAs are drawn uniformly based on the AoD range and AoA range, respectively.

For example, the rays' AoD and AoA can be set to be Laplacian distributed around their respective cluster AoD and AoA via

```
opt.type = 'laplacian'
opt.std_dev = std_dev
c.set_ray_angle_distribution(opt)
```

where `std_dev` is the desired standard deviation of the Laplacian distribution. In this way, the cluster AoD and cluster AoA act as the mean when drawing from the Laplacian distribution.

Similarly, the rays can be drawn from a Gaussian distribution via

```
opt.type = 'gaussian'
opt.variance = [var_az,var_el]
c.set_ray_angle_distribution(opt)
```

where `var_az` and `var_el` are the desired variances of the Gaussian distribution for the azimuth and elevation angles, respectively.

Azimuth and elevation are drawn independently in all cases.


### Snapping the AoDs and AoAs {#snapping-aod-aoa}

To force the realized AoDs and AoAs to exist on some grid, use

```
c.set_snap_AoD(AoD_grid)
c.set_snap_AoA(AoA_grid)
```

where the first row of `AoD_grid` and of `AoA_grid` are grids of azimuth AoDs and azimuth AoAs, respectively, and the second rows are grids of elevation AoDs and elevation AoAs.

### Setting the Ray Gain Distribution {#setting-ray-gain-distribution}

To set the ray gains to be drawn from a complex Gaussian distribution with mean 0 and variance 1, use

```
opt.type = 'cgauss'
c.set_ray_angle_distribution(opt)
```

To force the ray gains to all be equal to 1, use

```
opt.type = 'unit'
c.set_ray_angle_distribution(opt)
```


### Example Setup {#example-setup}

The typical setup of a ray/cluster channel is of the form

```
c = channel.create('ray-cluster');
c.set_propagation_velocity(vel);
c.set_carrier_frequency(fc);
c.set_array_transmit(atx);
c.set_array_receive(arx);
ray_gain_opt.type = 'cgauss';
c.set_ray_gain_distribution(ray_gain_opt);
ray_angle_opt.type = 'laplacian';
ray_angle_opt.std_dev = [0.2,0.2];
c.set_ray_angle_distribution(ray_angle_opt);
c.set_AoD_range(az_min_AoD,az_max_AoD,el_min_AoD,el_max_AoD);
c.set_AoA_range(az_min_AoA,az_max_AoA,el_min_AoA,el_max_AoA);
c.set_num_clusters_range(num_clusters_range);
c.set_num_rays_per_cluster_range(num_rays_per_cluster_range);

```


### Invoking a Channel Realization {#realization}

To invoke a realization of the channel, use

```
H = c.realization()
```

where `H` is the realized channel matrix.


### Viewing the Beamspace {#viewing-beamspace}

To view the beamspace of the channel, use

```
c.show_beamspace_azimuth()
```

for the azimuth beamspace and

```
c.show_beamspace_elevation()
```

for the elevation beamspace.

{% include image.html file="feature_aod_aoa.svg" caption="Beamspace of a ray/cluster channel." %}



### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}

### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}


## References
- _[An Overview of Signal Processing Techniques for Millimeter Wave MIMO Systems](https://arxiv.org/pdf/1512.03007.pdf)_


