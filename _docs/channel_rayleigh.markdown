---
layout: doc
title:  "Rayleigh-Faded Channel"
object: channel_rayleigh
superclass: channel
category: channel
summary: A MIMO channel with Rayleigh-faded entries.
file: obj/channel/channel_rayleigh.m
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "object", page.superclass %}
{% unless subs == empty %}Superclass:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Video Tutorial](#video)
- [Creating a Rayleigh-Faded Channel](#creating)
- [Key Properties](#key-properties)
- [Invoking a Channel Realization](#realization)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

Rayleigh-faded channels are represented by the `channel_rayleigh` object, which is a subclass of the generic `channel` object.

{% include image.html file="obj/channel/channel.svg"  %}

A Rayleigh-faded channel is described by the fact that its entries are drawn i.i.d. from a complex Normal distribution with mean 0 and variance 1.

$$
[\mathbf{H}]_{m,n} \sim \mathcal{N}_{\mathbb{C}}(0,1)
$$


### Video Tutorial {#video}

{% include youtube.html id='Wx8oqj1Duho' %}


### Creating a Rayleigh-Faded Channel {#creating}

A Rayleigh-faded channel can be created via

```matlab
c = channel.create('Rayleigh')
```

which will create a `channel_rayleigh` object `c`.

The `channel_rayeligh` object is a subclass of the `channel` object, meaning it will inherit the properties and methods of the `channel` object. 


### Key Properties {#key-properties}

The `channel_rayleigh` object is a subclass of the `channel` object and thus inherits all of its properties and methods.

It does not contain any important properties beyond those inherited from the `channel` object, which are summarized below.

```matlab
c.array_transmit
c.array_receive
c.num_antennas_transmit
c.num_antennas_receive
c.carrier_frequency
c.carrier_wavelength
c.propagation_velocity
c.channel_matrix
```


### Example Setup {#example-setup}

The setup of a Rayleigh-faded channel, by virtue of its simplicity, is practically identical to that of a generic `channel` object, which looks similar to

```matlab
c = channel.create()
c.set_carrier_frequency(fc)
c.set_propagation_velocity(vel)
c.set_arrays(atx,arx)
```

where `fc` is the carrier frequency (in Hz), `vel` is the propagation velocity (in m/s), and `atx` and `arx` are `array` objects at the channel input and output, respectively.


### Invoking a Channel Realization {#realization}

Generating a realization of the Rayleigh-faded channel `c` is as simple as executing

```matlab
H = c.realization()
```

where `H` is the realized channel matrix whose entries are drawn i.i.d. from a complex Normal distribution with mean 0 and variance 1.

The channel matrix can also be retrieved via 

```matlab
H = c.get_channel_matrix()
```

though this does not invoke a new channel realization but rather merely returns the current channel matrix.


### Shorthand Methods {#shorthand-methods}

The `channel_rayleigh` object inherits the following shorthand methods from the `channel` object.

- `c.H` --- Returns the channel matrix.
- `c.Nt` --- Returns the number of transmit antennas at the channel input.
- `c.Nr` --- Returns the number of receive antennas at the channel output.


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}

### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}


---

## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
