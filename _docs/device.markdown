---
layout: doc
title:  "Device"
object: device
file: obj/device/device.m
summary: A device with transmit and/or receive capability.
super: true
---

#### On This Page
- [About](#about)
- [Creating a Device](#creating)
- [Key Properties](#key-properties)
- [Setting the Transmitter](#setting-transmitter)
- [Setting the Receiver](#setting-receiver)
- [Setting the Device Location](#setting-location)
- [Example Setup](#example-setup)
- [Checking Device Type](#checking-type)
- [Passthrough Methods](#passthrough)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `device` object is used to represent a wireless terminal with transmit and/or receive capability.

`device` objects having transmit capability are fittingly referred to as "transmitters". Those with receive capability are referred to as "receivers". Those with both transmit and receive capability are termed "transceivers".

The `device` object can be thought of as "one layer above" the `transmitter` and `receiver` objects within the architecture of MFM.




### Creating a Device {#creating}

To create a `device` object, use

```matlab
d = device.create(type)
```

where `type` is either `'transmitter'`, `'receiver'`, or `'transceiver'`. 

By default, this will create a `device` object having a fully-digital `transmitter` and fully-digital `receiver`.

To create a `device` with a hybrid digital/analog transmitter (`transmitter_hybrid`) and hybrid digital/analog receiver (`receiver_hybrid`), use

```matlab
d = device.create(type,'hybrid')
```


### Key Properties {#key-properties}

A `device` object `d` contains a few important properties.

The `transmitter` and `receiver` at `d` exist (as applicable) in its properties

```matlab
d.transmitter
d.receiver
```

The `type` property is a string describing the transmit/receive capability of the `device`

```matlab
d.type
```

which can be either `'transmitter'`, `'receiver'`, or `'transceiver'`.

The location of `d` is specified by its `coordinate` property

```matlab
d.coordinate
```

which is a vector representing the $$(x,y,z)$$ coordinate of the device in 3-D space (in meters).


### Setting the Transmitter {#setting-transmitter}

To set the transmitter at a `device` object `d`, use

```matlab
d.set_transmitter(tx)
```

where `tx` is a `transmitter` object.


### Setting the Receiver {#setting-receiver}

To set the receiver at a `device` object `d`, use

```matlab
d.set_receiver(rx)
```

where `rx` is a `receiver` object.


### Setting the Device Location {#setting-location}

The location of a `device` object `d` can be set using

```matlab
d.set_coordinate([x,y,z])
```

where `[x,y,z]` is a vector of x, y, and z coordinates (in meters).


### Example Setup {#example-setup}

The typical setup for a `device` object is of the form

```matlab
d = device.create('transceiver')
d.set_transmitter(tx)
d.set_receiver(rx)
d.set_coordinate([x,y,z])
```

### Checking Device Type {#checking-type}

To see if a `device` object `d` has transmit capability, use

```matlab
val = d.istransmitter()
```

To see if it has receive capability, use

```matlab
val = d.isreceiver()
```

To see if it has both transmit and receive capability, use

```matlab
val = d.istransceiver()
```

where, in all three cases, `val` is a boolean if `d` has the capability (`true`) or not (`false`).


### Passthrough Methods {#passthrough}

Since the `device` object contains a `transmitter` and/or `receiver`, we have supplied so-called passthrough methods that allow the user to set parameters of a `device`'s `transmitter` and/or `receiver` from the `device` itself---rather than having to set them at the `transmitter` or `receiver`.

For example, to set the transmit power of a `device` object `d`'s `transmitter` to 30 dBm, one could use

```matlab
d.transmitter.set_transmit_power(30,'dBm')
```

Using the `device`'s passthrough methods, this can also be achieved via

```matlab
d.set_transmit_power(30,'dBm')
```

#### For the Transmitter

The following passthrough methods exist pertaining to the `transmitter` of `device` object `d`:
- `d.set_transmit_symbol_bandwidth(B)`
- `d.set_transmit_num_streams(Ns)`
- `d.set_transmit_symbol(s)`
- `d.set_transmit_array(a)`
- `d.set_transmit_num_rf_chains(Lt)`
- `d.set_transmit_power(P,unit)`
- `d.set_transmit_symbol_covariance(Rs)`
- `d.set_transmit_channel_state_information(csi)`
- `d.set_transmit_strategy(strategy)`
- `d.configure_transmitter(strategy)`


#### For the Receiver

The following passthrough methods exist pertaining to the `receiver` of `device` object `d`:
- `d.set_receive_symbol_bandwidth(B)`
- `d.set_receive_num_streams(Ns)`
- `d.set_receive_symbol(s)`
- `d.set_received_signal(y)`
- `d.set_receive_array(a)`
- `d.set_receive_num_rf_chains(Lr)`
- `d.set_noise(n)`
- `d.set_noise_power_per_Hz(noise_psd,unit)`
- `d.set_receive_channel_state_information(csi)`
- `d.set_receive_strategy(strategy)`
- `d.configure_receiver(strategy)`


#### For the Transmitter and Receiver

The following passthrough methods can be used to set values at the `transmitter` and `receiver` of the `device` object `d` simultaneously:
- `d.set_symbol_bandwidth(B)`
- `d.set_num_streams(Ns)`
- `d.set_arrays(transmit_array,receive_array)`
- `d.set_num_rf_chains(Lt,Lr)`


### Shorthand Methods {#shorthand-methods}

To provide convenient ways to retrieve common MIMO-related quantities from a `device`'s `transmitter` and `receiver`, there exist the following so-called shorthand methods.

- `d.F` --- Returns the precoding matrix.
- `d.W` --- Returns the combining matrix.
- `d.Ns_tx` --- Returns the number of streams at the transmitter.
- `d.Ns_rx` --- Returns the number of streams at the receiver.
- `d.Nt` --- Returns the number of transmit antennas.
- `d.Nr` --- Returns the number of receive antennas.
- `d.Ptx` --- Returns the transmit energy per symbol (joules per symbol).
- `d.Rs` --- Returns the transmit symbol covariance matrix at the transmitter.
- `d.Rn` --- Returns the noise covariance matrix at the receiver.
- `d.N0` --- Returns the noise energy per symbol at the receiver.
- `d.s_tx` --- Returns the transmit symbol.
- `d.s_rx` --- Returns the receive symbol.
- `d.x` --- Returns the signal vector transmitted from the transmitter.
- `d.y` --- Returns the signal vector arriving at the receiver.



### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}

{% include /_autodocs/{{ page.object }}_methods.txt %}


## Source


