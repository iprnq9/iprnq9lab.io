---
layout: doc
title:  "Link"
object: link
file: obj/link/link.m
summary: A physical connection between two devices.
super: true
---


#### On This Page
- [About](#about)
- [Creating a Link](#creating)
- [Key Properties](#key-properties)
- [Symmetric Forward and Reverse Channels](#symmetric-channels)
- [Symmetric Forward and Reverse Path Loss](#symmetric-path-loss)
- [Setting the Forward and Reverse Channel Models](#setting-channel)
- [Setting the Forward and Reverse Path Loss Models](#setting-path-loss)
- [Forward and Reverse SNR](#snr)
- [Setting the Forward and Reverse SNR](#setting-snr)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)

The `link` object is used to represent the physical connection (i.e., propagation) between one `device` object and another.

$$
\hat{\mathbf{s}} = \mathbf{W}^{*} \left( \sqrt{P_{\mathrm{tx}}} \cdot \underbrace{G \cdot \mathbf{H}}_{\mathrm{link}} \mathbf{F} \mathbf{s} + \mathbf{n} \right)
$$

Propagation from one `device` to another is modeled by two components, as evidenced by common MIMO formulations: 
1. a channel matrix $$\mathbf{H}$$ 
2. a large-scale gain $$G$$

The `link` object uses a `channel` object---more likely, one of its subclasses---to handle generation of the channel matrix $$\mathbf{H}$$.

The `link` object uses a `path_loss` object---again, one of its subclasses---to handle generation of the large-scale gain $$G$$.

In general, it may be the case that the channel and large-scale gain between two devices is not symmetric---or reciprocal---meaning that the link observered in one direction from the first device to the second is not necessarily the link observed from the second device to the first. This can be due to a variety of factors including separate arrays used for transmission and reception or separate bands for uplink and downlink, for example.

To capture this assymetry, the `link` object actually contains two links:
1. a forward link
2. a reverse link

To define these two links, we must also introduce the nomenclature for the two devices connected by a link:
1. a "head" device
2. a "tail" device

The head of the link is a device with transmit capability---either a transmitter or a transceiver. The tail of the link is a device with receive capability---either a receiver or transceiver.

If both devices only had transmit capability or only had receive capability, a physical link may exist but would be immaterial. This motivates our "head" and "tail" conventions.

{% include image.html file="obj/link/link.svg" caption="An example of a link between two transceivers." %}

The forward link is defined as from the head to the tail and always has some practical meaning given the fact that the head can transmit and the tail can receive.

The reverse link is defined as from the tail to the head but only has a a practical meaning if both the head and the tail are transceivers.

Propagation between two devices is largely characterized by the carrier frequency of the signal propagating from one device to the other. In some cases, the link is "symmetric"---or "reciprocal"---where a signal going from one device to the other sees the same environment as if it had traveled in the reverse fashion. This is common for time-division duplex systems where transmission by two devices takes place on the same frequency resources (with sufficient stationarity) and thus experiences symmetric transmit and receive channels.


### Creating a Link {#creating}

A `link` object can be created in the follow fashion

```matlab
lnk = link.create(head,tail)
```

where `head` is a `device` object with transmit capability and `tail` is a `device` object with receive capability.


### Key Properties {#key-properties}

A `link` object `lnk` contains the following important properties, among others.

The head and tail devices are captured by

```matlab
lnk.head
lnk.tail
```

The `channel` object representing the channel model used on the forward link is

```matlab
lnk.channel_forward
```

The `channel` object representing the channel model used on the reverse link is

```matlab
lnk.channel_reverse
```

The actual channel matrices for the forward and reverse links, respectively, are stored in 

```matlab
lnk.channel_matrix_forward
lnk.channel_matrix_reverse
```

The `path_loss` object representing the path loss model used on the forward link is

```matlab
lnk.path_loss_forward
```

The `path_loss` object representing the path loss model used on the reverse link is

```matlab
lnk.path_loss_reverse
```

The large-scale gain on the forward link---which is a product of `lnk.path_loss_forward`---is

```matlab
lnk.large_scale_gain_foward
```

and likewise the large-scale gain on the reverse link is

```matlab
lnk.large_scale_gain_reverse
```

The distance of the link in meters---the separation of the head and tail devices---is critical to most path loss models and is stored in

```matlab
lnk.distance
```

and is automatically calculated based on the locations of the head and tail devices.


### Symmetric Forward and Reverse Channels {#symmetric-channels}

Sometimes it is the case that the forward and reverse channels should be symmetric, meaning reverse channel matrix is equal to the Hermitian of the forward channel matrix.

To achieve this, simply use

```matlab
lnk.set_channel_symmetric(true)
```

which will be reflected in the property

```matlab
lnk.channel_symmetric
```

Note that this is only possible when the transmit and receive arrays at the head are equal in size and likewise at the tail (and the head and tail are both transceivers). This is often the case in practical transceivers since the same antennas are used for transmission and reception.

This will ensure that `lnk.channel_matrix_forward` is always the Hermitian of `lnk.channel_matrix_reverse`.


### Symmetric Forward and Reverse Path Loss {#symmetric-path-loss}

Sometimes it is the case that the forward and reverse path losses should be symmetric, meaning reverse path loss (and thus the reverse large-scale gain) is equal to that of the forward link.

To achieve this, simply use

```matlab
lnk.set_path_loss_symmetric(true)
```

which will be reflected in the property

```matlab
lnk.path_loss_symmetric
```

This will ensure that `lnk.large_scale_gain_forward` is always equal to `lnk.large_scale_gain_reverse`.


### Setting the Forward and Reverse Channel Models {#setting-channel}

To set the channel model used on the forward and reverse links of a `link` object `lnk`, use

```matlab
lnk.set_channel(chan_fwd,chan_rev)
```

where `chan_fwd` and `chan_rev` are `channel` objects to use on the forward and reverse links, respectively. Note that `chan_fwd` and `chan_rev` can be different channel models if desired.

To set the forward and reverse links to use the same channel model---but not necessarily the same realizations---a simpler method is

```matlab
lnk.set_channel(chan)
```

where `chan` is of course a `channel` object.

Note that, in all cases, the `channel` objects that are passed to the `link` are deep copied before using them, meaning the same `chan`, `chan_fwd`, and `chan_rev` can be used when setting multiple links if desired.

Also, note that the transmit and receive arrays of the head and tail devices are automatically used to set up the forward and reverse `channel` objects.


### Setting the Forward and Reverse Path Loss Models {#setting-path-loss}

To set the path loss model used on the forward and reverse links of a `link` object `lnk`, use

```matlab
lnk.set_channel(path_fwd,path_rev)
```

where `path_fwd` and `path_rev` are `path_loss` objects to use on the forward and reverse links, respectively. Note that `path_fwd` and `path_rev` can be different path loss models if desired.

To set the forward and reverse links to use the same path loss model---but not necessarily the same realizations---a simpler method is

```matlab
lnk.set_path_loss(path)
```

where `path` is of course a `path_loss` object.

Note that, in all cases, the `path_loss` objects that are passed to the `link` are deep copied before using them, meaning the same `path`, `path_fwd`, and `path_rev` can be used when setting multiple links if desired.


### Forward and Reverse SNR {#snr}

A `link` object `lnk` also has properties

```matlab
lnk.snr_forward
lnk.snr_reverse
```

which are "large-scale" signal-to-noise ratios (SNRs) defined as

$$
\mathrm{SNR}_{\mathrm{fwd}} = \frac{P_{\mathrm{tx,head}} \cdot G_{\mathrm{fwd}}^2}{\sigma_{\mathrm{n,tail}}^2} \\
\mathrm{SNR}_{\mathrm{rev}} = \frac{P_{\mathrm{tx,tail}} \cdot G_{\mathrm{rev}}^2}{\sigma_{\mathrm{n,head}}^2}
$$

where $$P_{\mathrm{tx,head}}$$ and $$P_{\mathrm{tx,tail}}$$ are the transmit energy per symbol at the head and tail devices, respectively; $$G_{\mathrm{fwd}}$$ and $$G_{\mathrm{rev}}$$ are the forward and reverse large-scale gains, respectively; and $$\sigma_{\mathrm{n,head}}^2$$ and $$\sigma_{\mathrm{n,tail}}^2$$ are the noise energy per symbol at the receiver of the head and tail devices, respectively.

Note that the reverse link SNR is only applicable when the head and tail devices are both transceivers.


### Setting the Forward and Reverse SNR {#setting-snr}

It is common in MIMO research to evaluate performance for various large-scale SNR values.

Rather than force the user to find a triplet of transmit power, distance/path loss, and noise power to achieve a desired SNR, the user can force the SNR on a link to be a desired value using

```matlab
lnk.set_snr(snr_fwd,snr_rev,unit)
```

where `snr_fwd` and `snr_rev` are large-scale SNR values and `unit` is an optional string to specify the units of `snr_fwd` and `snr_rev`. If `snr_rev = []`, then `snr_fwd` will be set on the forward and reverse links.

For example, to set the large-scale SNR to 10 dB on the forward and reverse links, one would use

```matlab
lnk.set_snr(10,[],'dB')
```

The `set_snr` function adjusts the large-scale gain $$G$$ to be achieve a desired $$\mathrm{SNR}$$ based on the transmit energy per symbol and noise energy per symbol. Thus, to use this method, it is essential that the transmit energy per symbol and noise energy per symbol be set as desired and that the path loss not overwrite the newly set large-scale gain.


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}

{% include /_autodocs/{{ page.object }}_methods.txt %}


## Source


