---
layout: doc
title:  "Network"
object: network_mfm
file: obj/network/network_mfm.m
summary: A network of devices and the links connecting them.
super: true
---

#### On This Page
- [About](#about)
- [Creating a Network](#creating)
- [Key Properties](#key-properties)
- [Setting the Transmitter](#setting-transmitter)
- [Setting the Receiver](#setting-receiver)
- [Setting the Device Location](#setting-location)
- [Example Setup](#example-setup)
- [Checking Device Type](#checking-type)
- [Passthrough Methods](#passthrough)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

An MFM network, as its name suggests, is used to house multiple devices the links connecting them. MFM networks are represented by the `network_mfm` object.

The `network_mfm` object simplifies adding devices to the network and provides multiple ways to establish links between the devices.

While a network can be used in various fashions, the most straightforwad is to populate it with source devices and destination devices.

{% include image.html file="obj/network/network_show_3d_2_2.svg" caption="A network made up of two sources, two destinations, and four links." %}

Source devices are `device` objects with transmit capability that will be transmitting to a destination `device` object having receive capability.

When multiple source devices are present in the same network, interference manfifests via links connecting source devices to devices other than its intended destination.

For example, there are four devices in the network shown in the figure above. The source device `device-1` aims to transmit to the destination device `device-2`. The source device `device-3` aims to transmit to the destination device `device-4`. Four `link` objects are produced: 
1. two desired links shown in solid black---between `device-1` and `device-2` and frbetweenom `device-3` and `device-4`
2. two interference links shown in dashed red---between `device-1` and `device-4` and between `device-3` and `device-2`

Using knowledge of these sources, destinations, and links, the network handles high-level operations like acquiring and distributing channel state information (CSI) and reporting performance metrics like mutual information/spectral efficiency and received symbol error.

### Creating a Network {#creating}

To create a network in MFM, use

```matlab
n = network_mfm.create()
```

where `n` is an empty `network_mfm` object.


### Key Properties {#key-properties}

A `network_mfm` object `n` contains a few important properties.

First, as mentioned, each network contains source-destination pairs---pairs of `device` objects---that indicate which devices (sources) are transmitting to which devices (destinations). The sources are stored in

```matlab
n.sources
```

where `n.sources` is a cell of `device` objects having transmit capability. 

The number of sources in the network is

```matlab
n.num_sources
```

Similarly, destination devices are stored in 


```matlab
n.destinations
```

which is a cell of `device` objects having receive capability.

The number of destinations in the network---which should equal the number of sources---is stored in 

```matlab
n.num_destinations
```

The links in the network are stored in

```matlab
n.links
```

which is a cell of `link` objects.

The number of links in the network is stored in

```matlab
n.num_links
```

All (unique) devices in the network are stored in

```matlab
n.devices
```

which is a cell of `device` objects.


### Adding a Source-Destination Pair

In most cases, devices should be added to a network `n` using

```matlab
n.add_source_destination(source,destination)
```

where `source` and `destination` are `device` objects. Note that _**pairs**_ of devices are added to the network, not individual devices.

This will automatically populate the properties `n.sources` and `n.destinations` with the newly added devices.

This will also automatically include the `source` and `destination` devices in the property `n.devices` if they are not already present.


### Removing a Source-Destination Pair

To remove a source-destination pair from a network `n`, simply use

```matlab
n.remove_source_destination_pair(source,destination)
```

where `source` and `destination` are `device` objects to remove. Note that _**pairs**_ of devices are removed from the network, not individual devices.

To remove all source-destination pairs from a network `n`, use

```matlab
n.remove_all_source_destination()
```


### Adding Links to the Network

Recall that sources are transmitting devices and destinations are receiving devices.

The easiest and most practical way to establish links between devices in a network `n` is to use

```matlab
n.populate_links_from_source_destination()
```

which will automatically establish links between all sources and destinations. In other words, for a given source, a link will be created between it and each destination in the network.

With $$N$$ source-destination pairs, populating links between all sources and destinations will yield $$N^2$$ links.

While a physical link may exist between one source device and another source device, it does not hold much practical value since neither is necessarily receiving. Likewise, a physical link may exist between one destination device and another destination device, but it does not hold much practical value since neither is necessarily transmitting.

Nonetheless, MFM supplies functions to manually create links via

```matlab
n.add_link(lnk)
```

where `lnk` is a `link` object or via

```matlab
n.add_links(lnks)
```

where `lnks` is a vector of `link` objects.


### Viewing the Network

To view a network `n` in 3-D use, 

```matlab
n.show_3d()
```

which will display a figure similar to

{% include image.html file="obj/network/network_show_3d_2_2.svg" %}

### Passthrough Methods

Configuring each link and each device in a network can be quite cumbersome, especially as the network grows.

To make configuring objects network-wide more straightforward, we have provided the following passthrough methods for `network_mfm` objects.


#### Link Channel Model and Path Loss Model

Links within a network `n` can be configured one-by-one if desired by accessing the `i`-th link directly via `n.links{i}`.

It is often the case that the same channel model and path loss model will be used across all links in a network. In light of this, MFM supplies the functions

```matlab
n.set_channel(c)
n.set_path_loss(p)
```

where `c` is a `channel` object and `p` is a `path_loss` object. This will set each `link` object in the network to use the same channel model and path loss model, each with their own unique realizations.


#### Carrier Frequency, Propagation Velocity, and Symbol Bandwidth

To set the carrier frequency, propagation velocity, and symbol bandwidth at all devices and links within a network `n`, use

```matlab
n.set_carrier_frequency(fc);
n.set_propagation_velocity(vel);
n.set_symbol_bandwidth(B);
```

where `fc` is the carrier frequency (in Hz), `vel` is the propagation velocity (in m/s), and `B` is the symbol bandwidth (in Hz).


#### Transmit Power

To set the transmit power at all devices to a value of `P`, use

```matlab
n.set_transmit_power(P,unit)
```

where `unit` is a string specifying the units of `P` (e.g., `'dBm'`, `'watts'`).


#### Noise Power

To set the noise power at all devices to a value of `noise_psd`, use

```matlab
n.set_noise_power_per_Hz(noise_psd,unit)
```

where `unit` is a string specifying the units of `noise_psd` (e.g., `'dBm_Hz'`, `'watts_Hz'`).


#### Number of Streams

To set the number of streams at all devices to a value of `Ns`, use

```matlab
n.set_num_streams(Ns)
```


#### Transmit Symbol

T set the transmit symbol at all devices to a vector `s`, use

```matlab
n.set_transmit_symbol(s)
```

Note that this is only appropriate if all devices are transmitting with the same number of streams.


### Invoking a Network Realization

To invoke a realization of the channels on all links in a network `n`, use

```matlab
n.realization_channel()
```

To invoke a realization of the path loss on all links in a network `n`, use

```matlab
n.realization_path_loss()
```

To invoke a realization of all channels and path losses in one line, simply use

```matlab
n.realization()
```


### Channel State Information

To compute the channel state information for a realization of a network `n`, use

```matlab
n.compute_channel_state_information()
```

To supply each device with this computed channel state information, use

```matlab
n.supply_channel_state_information()
```


### Configuring Devices to Transmit and Receive

To configure all transmitters in the network to use a particular transmit strategy, use

```matlab
n.configure_transmitter(strategy)
```

where `strategy` is a string specifying which strategy the transmitters should use. This string must be one of the valid transmit strategies that have been defined for the `transmitter` objects being used in the network. Please refer to documentation on the `transmitter` object for more information on this.


To configure all receivers in the network to use a particular receive strategy, use

```matlab
n.configure_receiver(strategy)
```

where `strategy` is a string specifying which strategy the receivers should use. This string must be one of the valid receive strategies that have been defined for the `receiver` objects being used in the network. Please refer to documentation on the `receiver` object for more information on this.


### Received Signals at Each Device

With a realized network and configured transmitters and receivers, the received signals at each device can be computed via

```matlab
n.compute_received_signals()
```

### Performance Metrics

To evaluate the performance of a network, MFM can compute and report the mutual information and estimation error of the receive symbol versus the transmit symbol for each source-destination pair.


#### Mutual Information

To report the achieved Gaussian mutual information (spectral efficiency) between a source device and destination device, use

```matlab
mi = n.report_mutual_information(source,destination)
```

where `source` is a source device, `destination` is a destination device, and `mi` is the mutual information in bps/Hz.


#### Symbol Estimation Error

To report the achieved estimation error between the transmit symbol $$\mathbf{s}$$ at a source device and the receie symbol $$\hat{\mathbf{s}}$$ destination device, use

```matlab
[err,nerr] = n.report_symbol_estimation_error(source,destination)
```

where `source` is a source device, `destination` is a destination device, `err` is the symbol estimation error defined as


$$
\lVert \hat{\mathbf{s}} - \mathbf{s} \rVert_{2}^{2}
$$

and `nerr` is the symbol estimation error normalized to the transmit symbol energy defined as

$$
\frac{\lVert \hat{\mathbf{s}} - \mathbf{s} \rVert_{2}^{2}}{\lVert \mathbf{s} \rVert_{2}^{2}}
$$


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}

{% include /_autodocs/{{ page.object }}_methods.txt %}



