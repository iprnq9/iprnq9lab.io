---
layout: doc
title:  "Path Loss"
object: path_loss
category: path_loss
super: true
file: obj/path_loss/path_loss.m
summary: A generic path loss model.
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "superclass", page.object %}
{% unless subs == empty %}Subclasses:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Creating a Generic Path Loss Object](#creating)
- [Key Properties](#key-properties)
- [Setting the Carrier Frequency](#setting-carrier-frequency)
- [Setting the Propagation Velocity](#setting-propagation-velocity)
- [Setting the Distance](#setting-distance)
- [Example Setup](#example-setup)
- [Getting the Attenuation](#getting-attenuation)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `path_loss` object represents a generic path loss model. It does not hold any value in representing a particular path loss model but rather acts as the foundation for all other path loss models within MFM.

A path loss model is typically a function of carrier frequency, distance of the path, and other parameters (e.g., path loss exponent). Some path loss models are deterministic while other involve some degree of randomness (e.g., shadowing).

In the context of MIMO formulations, the `path_loss` object (and its subclasses) are responsible for realizing the large-scale gain $$G$$, which is the square root of the inverse of path loss.


### Creating a Generic Path Loss Object {#creating}

While the `path_loss` object on its own isn't all that useful, it can be created via

```matlab
p = path_loss.create()
```

As mentioned, more useful channel models are subclasses of the `path_loss` object, meaning they will inherit the properties and methods of the `path_loss` object. Moreover, the `path_loss` object includes some of the key setup functions that most---if not all---other channel objects will use.


### Key Properties {#key-properties}

The `path_loss` object (and its subclasses) contain the following important properties.

The realized path loss of a `path_loss` object `p` is stored in

```
p.attenuation
```

which is a power loss.

The carrier frequency (in Hertz), carrier wavelength (in wavelengths), and propagation velocity (in meters per second) of the channel---more accurately, of the signals we are interested in---are captured by

```matlab
p.carrier_frequency
p.carrier_wavelength
p.propagation_velocity
```

While not all channel models require knowledge of these three properties, several path loss models do and thus, for simplicity, MFM includes them in the generic `path_loss` object.

The distance of the path (in meters) is stored in 

```
p.distance
```


### Setting the Carrier Frequency {#setting-carrier-frequency}

To set the carrier frequency of a path loss model, use

```matlab
p.set_carrier_frequency(fc)
```

where `fc` is the carrier frequency (in Hz) of the signals of interest. This will automatically update the carrier wavelength based on the current propagation velocity.


### Setting the Propagation Velocity {#setting-propagation-velocity}

To set the propagation velocity of a path loss model, use

```matlab
p.set_propagation_velocity(vel)
```

where `vel` is the propagation velocity (in m/s) of the signals of interest.

It is common to use `vel = 3e8` for electromagnetic signals, for example. 

Since this parameter can be set by the user, MFM may support other communication systems and/or other propagation media, such as underwater acoustics where the propagation velocity is commonly approximated to `vel = 1.5e3` for oceanic environments.


### Setting the Distance {#setting-distance}

To set the distance of a `path_loss` object `p`, simply use

```matlab
p.set_distance(d)
```

where `d` is the distance of the path in meters.


### Example Setup {#example-setup}

A typical `path_loss` object setup looks something similar to

```matlab
p = path_loss.create()
p.set_carrier_frequency(fc)
p.set_propagation_velocity(vel)
p.set_distance(d)
```

### Getting the Attenuation {#getting-attenuation}

To get the realized attenuation of a `path_loss` object `p`, simply use

```matlab
atten = p.get_attenuation()
```

where `atten` is the attenuation of the path loss (power loss, linear scale).

Note that `atten` is related to the large-scale gain $$G$$ by simply `atten` $$ = G^{-2}$$.

As mentioned, the `path_loss` object on its own does not represent any path loss model but is a superclass/parent for all other path loss models in MFM. Thus, there is no way to realize the attenuation on a `path_loss` object itself; all other path loss models in MFM will, however, and after which, users can use `get_attenuation` to retrieve the realized attenuation.

### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}


