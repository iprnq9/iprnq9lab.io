---
layout: doc
title:  "Free-Space Path Loss"
object: path_loss_free_space
superclass: path_loss
category: path_loss
file: obj/path_loss/path_loss_free_space.m
summary: A free-space path loss model.
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "object", page.superclass %}
{% unless subs == empty %}Superclass:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Creating a Free-Space Path Loss Object](#creating)
- [Key Properties](#key-properties)
- [Setting the Path Loss Exponent](#setting-path-loss-exponent)
- [Example Setup](#example-setup)
- [Getting the Attenuation](#getting-attenuation)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `path_loss_free_space` object represents a free-space path loss model.

Free-space path loss can be described entirely by three parameters:
1. the carrier wavelength $$\lambda$$
2. the distance $$d$$
3. the path loss exponent $$\eta$$

The path loss (or attenuation) stemming from a free-space path loss model can be deterministically written as

$$
G^{2} = \left( \frac{\lambda}{4\pi} \right)^{2} \times \left( \frac{1}{d} \right)^{\eta}
$$

where $$G^2$$ is the path _**gain**_ and $$1/G^2$$ is the path _**loss**_.

When $$\eta = 2$$, this is the classical Friis formula for path loss. Typically, $$\eta \geq 2$$ except in rare settings with significant clutter. Most often, $$\eta \in [2,4]$$.


The `path_loss_free_space` object is a subclass of the `path_loss` object.


### Creating a Free-Space Path Loss Object {#creating}

A free-space path loss object `path_loss_free_space` can be created via

```matlab
p = path_loss.create('free-space')
```



### Key Properties {#key-properties}

The `path_loss_free_space` object inherits all properties of the `path_loss` object, the key ones being

```
p.attenuation
p.carrier_frequency
p.carrier_wavelength
p.propagation_velocity
p.distance
```

In addition to these, the `path_loss_free_space` object also has the property 

```
p.path_loss_exponent
```

to capture the path loss exponent ($$\eta$$).


### Setting the Path Loss Exponent {#setting-path-loss-exponent}

To set the path loss exponent of a free space path loss object `p`, use

```matlab
p.set_path_loss_exponent(ple)
```

where `ple` is the path loss exponent (a traditionally positive number).


### Example Setup {#example-setup}

A typical `path_loss_free_pace` object setup looks something similar to

```matlab
p = path_loss.create()
p.set_carrier_frequency(fc)
p.set_propagation_velocity(vel)
p.set_distance(d)
p.set_path_loss_exponent(ple)
```


### Invoking a Realization {#realization}

To realize the path loss, use

```
atten = p.realization()
```

which, when appropriately setup, will return the attenuation `atten` for the given carrier wavelength, distance, and path loss exponent.

Note that `atten` is related to the large-scale gain $$G$$ by simply `atten` $$ = G^{-2}$$.


### Getting the Attenuation {#getting-attenuation}

To get the realized attenuation of a `path_loss_free_space` object `p`, one can also use

```matlab
atten = p.get_attenuation()
```

where `atten` is the attenuation of the path loss (power loss, linear scale).



### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}


