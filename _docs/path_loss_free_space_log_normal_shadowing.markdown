---
layout: doc
title:  "Free-Space Path Loss with Log-Normal Shadowing"
object: path_loss_free_space_log_normal_shadowing
superclass: path_loss
category: path_loss
file: obj/path_loss/path_loss_free_space_log_normal_shadowing.m
summary: A free-space path loss model with log-normal shadowing.
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "object", page.superclass %}
{% unless subs == empty %}Superclass:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Creating a Free-Space Path Loss with Log-Normal Shadowing Object](#creating)
- [Key Properties](#key-properties)
- [Setting the Log-Normal Shadowing Variance](#setting-variance)
- [Example Setup](#example-setup)
- [Getting the Attenuation](#getting-attenuation)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `path_loss_free_space_log_normal_shadowing` object represents a free-space path loss model with log-normal shadowing. It is a subclass of the `path_loss_free_space` object (and thus of the `path_loss` object), supplying the deterministic loss of a free-space path loss model with a random shadowing term.

Recall that deterministic free-space path loss can be described entirely by three parameters:
1. the carrier wavelength $$\lambda$$
2. the distance $$d$$
3. the path loss exponent $$\eta$$

The path loss (or attenuation) stemming from a free-space path loss _**with log-normal shadowing**_ can be described as 

$$
G^{2} = \left( \frac{\lambda}{4\pi} \right)^{2} \times \left( \frac{1}{d} \right)^{\eta} \times \gamma
$$

where $$G^2$$ is the path _**gain**_ and $$1/G^2$$ is the path _**loss**_; and $$\gamma$$ is the shadowing parameter.

Typically, $$\eta \geq 2$$ except in rare settings with significant clutter. Most often, $$\eta \in [2,4]$$.

In log-normal shadowing, realizations $$\gamma$$ of the random variable $$\Gamma$$ are distributed as

$$
10 \times \log_{10} (\Gamma) \sim \mathcal{N}(0,\sigma_{\gamma}^2)
$$

where $$\sigma_{\gamma}^2$$ is the variance of log-normal shadowing.


### Creating a Free-Space Path Loss with Log-Normal Shadowing Object {#creating}

A free-space path loss with log-normal shadowing object `path_loss_free_space_log_normal_shadowing` can be created via

```matlab
p = path_loss.create('free-space-log-normal-shadowing')
```



### Key Properties {#key-properties}

The `path_loss_free_space_log_normal_shadowing` object inherits all properties of the `path_loss_free_space` object, the key ones being

```
p.attenuation
p.carrier_frequency
p.carrier_wavelength
p.propagation_velocity
p.distance
p.path_loss_exponent
```

In addition, it also has the property

```
p.log_normal_shadowing_variance
```

which captures the log-normal shadowing variance (i.e., $$\sigma_{\gamma}^2$$).


### Setting the Log-Normal Shadowing Variance {#setting-variance}

To set the variance of log-normal shadowing of a free space path loss with log-normal shadowing object `p`, use

```
p.set_log_normal_shadowing_variance(var)
```

where `var` is the variance of log-normal shadowing (a non-negative number).


### Example Setup {#example-setup}

A typical `path_loss_free_space_log_normal_shadowing` object setup looks something similar to

```
p = path_loss.create()
p.set_carrier_frequency(fc)
p.set_propagation_velocity(vel)
p.set_distance(d)
p.set_path_loss_exponent(ple)
p.set_log_normal_shadowing_variance(var)
```


### Invoking a Realization {#realization}

To realize the path loss, use

```
atten = p.realization()
```

which, when appropriately setup, will return the attenuation `atten` for the given carrier wavelength, distance, path loss exponent, and log-normal shadowing variance.

Note that `atten` is related to the large-scale gain $$G$$ by simply `atten` $$ = G^{-2}$$.


### Getting the Attenuation {#getting-attenuation}

To get the realized attenuation of a `path_loss_free_space_log_normal_shadowing` object `p`, one can also use

```matlab
atten = p.get_attenuation()
```

where `atten` is the attenuation of the path loss (power loss, linear scale).



### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}


