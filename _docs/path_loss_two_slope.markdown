---
layout: doc
title:  "Two-Slope Path Loss"
object: path_loss_two_slope
superclass: path_loss
category: path_loss
file: obj/path_loss/path_loss_two_slope.m
summary: A two-slope path loss model.
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "object", page.superclass %}
{% unless subs == empty %}Superclass:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Creating a Free-Space Path Loss Object](#creating)
- [Key Properties](#key-properties)
- [Setting the Path Loss Exponents](#setting-path-loss-exponents)
- [Setting the Reference Distance](#setting-reference-distance)
- [Setting the Reference Path Loss](#setting-reference-path-loss)
- [Example Setup](#example-setup)
- [Getting the Attenuation](#getting-attenuation)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `path_loss_two_slope` object represents a two-slope path loss model.

The path loss under a two-slope path loss model can be described entirely by five parameters:
1. the distance of the path $$d$$
2. the reference distance $$d_0$$
3. the path loss at the reference distance $$L_0$$
4. the path loss exponent within the reference distance $$\eta_1$$
4. the path loss exponent beyond the reference distance $$\eta_2$$

The path loss (or attenuation) stemming from a free-space path loss model can be deterministically written as

$$
\frac{1}{G^{2}} = 
\begin{cases} 
{L_0} \times \left( \frac{d}{d_0} \right)^{\eta_1}, & d \leq d_0 \\
{L_0} \times \left( \frac{d}{d_0} \right)^{\eta_2}, & d > d_0 \\
\end{cases}
$$

where $$G^2$$ is the path _**gain**_ and $$1/G^2$$ is the path _**loss**_.

The `path_loss_two_slope` object is a subclass of the `path_loss` object.


### Creating a Two-Slope Path Loss Object {#creating}

A two-slope path loss object `path_loss_two_slope` can be created via

```matlab
p = path_loss.create('two-slope')
```



### Key Properties {#key-properties}

The `path_loss_two_slope` object inherits all properties of the `path_loss` object, the key ones being

```
p.attenuation
p.carrier_frequency
p.carrier_wavelength
p.propagation_velocity
p.distance
```

In addition to these, the `path_loss_two_slope` object also has the property 

```
p.path_loss_exponents
```

to capture the path loss exponents $$\eta_1$$ and $$\eta_2$$ and 

```
p.reference_distance
p.reference_path_loss
```

to capture the reference distance $$d_0$$ and reference path loss $$L_0$$, respectively.


### Setting the Path Loss Exponents {#setting-path-loss-exponents}

To set the path loss exponents of a two-slope path loss object `p`, use

```matlab
p.set_path_loss_exponents(ple_1,ple_2)
```

where `ple_1` and `ple_2` are the path loss exponents $$\eta_1$$ and $$\eta_2$$.


### Example Setup {#example-setup}

A typical `path_loss_two_slope` object setup looks something similar to

```matlab
p = path_loss.create('two-slope')
p.set_carrier_frequency(fc)
p.set_propagation_velocity(vel)
p.set_distance(d)
p.set_path_loss_exponents(ple_1,ple_2)
p.set_reference_distance(d0)
p.set_reference_path_loss(L0,'dB')
```


### Invoking a Realization {#realization}

To realize the path loss, use

```
atten = p.realization()
```

which, when appropriately setup, will return the attenuation `atten` for the given carrier wavelength, distance, and path loss exponent.

Note that `atten` is related to the large-scale gain $$G$$ by simply `atten` $$ = G^{-2}$$.


### Getting the Attenuation {#getting-attenuation}

To get the realized attenuation of a `path_loss_free_space` object `p`, one can also use

```matlab
atten = p.get_attenuation()
```

where `atten` is the attenuation of the path loss (power loss, linear scale).



### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}


