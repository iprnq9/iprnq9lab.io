---
layout: doc
title:  "Receiver"
object: receiver
file: obj/receiver/receiver.m
summary: A MIMO receiver.
super: true
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "superclass", page.object %}
{% unless subs == empty %}Subclasses:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}


#### On This Page
- [About](#about)
- [Creating a Receiver](#creating)
- [Key Properties](#key-properties)
- [Setting the Number of Receive Streams](#setting-number-streams)
- [Setting the Symbol Bandwidth](#setting-symbol-bandwidth)
- [Setting the Noise Power](#setting-noise)
- [Example Setup](#example-setup)
- [Setting the Received Signal](#setting-received-signal)
- [Setting/Realizing Noise](#setting-realizing-noise)
- [Setting the Combiner](#setting-combiner)
- [Getting the Receive Symbol](#getting-receive-symbol)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)

### About {#about}

The `receiver` object captures the properties and responsibilities of reception---the estimation of a symbol vector. Its primary responsibility lay in introducing additive noise and executing MIMO combining to retrieve a symbol vector from a captured received signal vector.

{% include image.html file="obj/receiver/receiver.svg" %}

The $$N_{\mathrm{s}} \times 1$$ received symbol vector $$\hat{\mathbf{s}}$$ after applying the receiver's combiner can be written as

$$
\hat{\mathbf{s}} = \mathbf{W}^{*} (\mathbf{y} + \mathbf{n})
$$

where $$\mathbf{y}$$ is the $$N_{\mathrm{r}} \times 1$$ complex received signal vector striking the receive array, $$\mathbf{n}$$ is the $$N_{\mathrm{r}} \times 1$$ complex noise vector added per-antenna, and $$\mathbf{W}$$ is the $$N_{\mathrm{r}} \times N_{\mathrm{s}}$$ combining matrix.

The `receiver` object is used to represent the portion of a device responsible for reception, including MIMO combining.

The `receiver` object is responsible for incorporating additive noise (which, in practice, is primarily introduced and amplified by components in the receive chain). Currently, MFM only supports additive white Gaussian noise (AWGN) introduced i.i.d. per-antenna.


### Creating a Receiver {#creating}

To create a `receiver` object `rx`, a convenient way is to use

```matlab
rx = receiver.create();
```



### Key Properties {#key-properties}

The combining matrix $$\mathbf{W}$$ of a `receiver` object `rx` is captured by the property

```
rx.combiner
```

Its receive symbol $$\hat{\mathbf{s}}$$ is represented by

```
rx.receive_symbol
```

The symbol bandwidth $$B$$ (in Hz) and symbol period $$T$$ (in seconds) are captured by

```
rx.symbol_bandwidth
rx.symbol_period
```

The noise power spectral density (in watts per Hz) is captured by

```
rx.noise_power_per_Hz
```

The integrated noise power (in watts) is captured by

```
rx.noise_power
```

The noise energy per symbol (in joules) is captured by 

```
rx.noise_energy_per_symbol
```


### Setting the Number of Receive Streams {#setting-number-streams}

To set the number of streams multiplexed at the receiver $$N_{\mathrm{s}}$$, one can use

```matlab
rx.set_num_streams(Ns)
```

where the number of streams `Ns` should be a positive integer.


### Setting the Symbol Bandwidth {#setting-symbol-bandwidth} 

To set the symbol bandwidth at a receiver `rx`, use

```
rx.set_symbol_bandwidth(B)
```

where `B` is the symbol bandwidth in Hz.

This automatically updates the `receiver`'s symbol period according to

$$
B = T^{-1}
$$


### Setting the Noise Level {#setting-noise}

Additive noise is introduced per-antenna. Currently, only Gaussian noise is supported in MFM. The noise vector $$\mathbf{n}$$ is drawn from the complex Gaussian distribution as

$$
\mathbf{n} \sim \mathcal{N}_{\mathbb{C}}(\mathbf{0},\sigma_{\mathrm{n}}^2 \cdot \mathbf{I})
$$

where $$\sigma_{\mathrm{n}}^2$$ is the noise energy per symbol (in joules).

Rather than set the noise energy per symbol directly, it is more practical to set the noise power spectral density and symbol bandwidth, which togther define the noise power and noise energy per symbol.

To set the noise power spectral density of the additive noise at `receiver` object `rx`, use

```
rx.set_noise_power_per_Hz(noise_psd,unit)
```

where `noise_psd` is the noise power per Hz and `unit` is an optional string specifying the units of `noise_psd` (default of watts per Hz). 

For example, to set the noise power spectral density to -174 dBm/Hz, used

```matlab
rx.set_noise_power_per_Hz(-174,'dBm_Hz')
```

Based on the current symbol bandwidth, this will automatically update the noise energy per symbol and noise power properties. 

Since MFM assumes the relationship $$B = T^{-1}$$, the noise power spectral density (power per Hz) is equal to the noise energy per symbol. 

The noise power (in watts) is equal to the noise power spectral density times the bandwidth as

$$
\sigma_{\mathrm{n}}^2 \cdot B
$$


### Example Setup {#example-setup}

Setting up a `receiver` object `rx` typically looks something similar to

```
rx = receiver.create()
rx.set_num_streams(Ns)
rx.set_symbol_bandwidth(B)
rx.set_noise_power_per_Hz(noise_psd,unit)
```


### Setting the Received Signal {#setting-received-signal}

To set the received signal vector $$\mathbf{y}$$ striking the receive array, use

```
rx.set_received_signal(y)
```


### Setting/Realizing Noise {#setting-realizing-noise}

A new noise vector $$\mathbf{n}$$ can be realized, according to the current noise level, via 

```
rx.set_noise()
```

which is reflected in the property `rx.noise`.

If desired, the noise vector can be set manually to a vector `n` via

```
rx.set_noise(n)
```


### Setting the Combiner {#setting-combiner}

To set the combiner $$\mathbf{W}$$ of a receiver, use

```
rx.set_combiner(W)
```

where `W` is a combining matrix.


### Getting the Receive Symbol {#getting-receive-symbol}

The receive symbol $$\hat{\mathbf{s}}$$ can be fetched via

```
s = rx.get_receive_symbol()
```

where `s` is the receive symbol vector.


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}

