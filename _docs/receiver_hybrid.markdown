---
layout: doc
title:  "Hybrid Digital/Analog Receiver"
object: receiver_hybrid
file: obj/receiver/receiver_hybrid.m
summary: A MIMO receiver employing hybrid digital/analog combining.
super: false
superclass: receiver
category: receiver
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "object", page.superclass %}
{% unless subs == empty %}Superclass:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Creating a Hybrid Digital/Analog Receiver](#creating)
- [Key Properties](#key-properties)
- [Limited Phase Control](#limited-phase-control)
- [Limited Amplitude Control](#limited-amplitude-control)
- [Connected-ness of the Hybrid Receiver](#connected-ness)
- [Example Setup](#example-setup)
- [Setting the Digital Combiner](#setting-digital-combiner)
- [Setting the Analog Combiner](#setting-analog-combiner)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `receiver_hybrid` object captures reception from a hybrid digital/analog combining receiver. In addition to that of the `receiver` object, a `receiver_hybrid` object is responsible for achieving reception (i.e., estimating a symbol vector) via the combination of digital and analog combining (rather than solely digital combining like in the `receiver` object).

{% include image.html file="obj/receiver_hybrid/receiver_hybrid.svg" %}

The $$N_{\mathrm{s}} \times 1$$ received symbol vector $$\hat{\mathbf{s}}$$ after the hybrid receiver can be written as

$$
\hat{\mathbf{s}} = \mathbf{W}_{\mathrm{BB}}^{*} \mathbf{W}_{\mathrm{RF}}^{*} (\mathbf{y} + \mathbf{n})
$$

where $$\mathbf{y}$$ is the $$N_{\mathrm{r}} \times 1$$ complex received signal vector at the receive array, $$\mathbf{W}_{\mathrm{RF}}$$ is the $$N_{\mathrm{r}} \times L_{\mathrm{r}}$$ analog combining matrix, and $$\mathbf{W}_{\mathrm{BB}}$$ is the $$L_{\mathrm{r}} \times N_{\mathrm{s}}$$ digital combining matrix.

Digital combining---represented by the matrix $$\mathbf{W}_{\mathrm{BB}}$$---takes place in the digital domain at baseband (hence "BB"). 

Analog combining---represented by the matrix $$\mathbf{W}_{\mathrm{RF}}$$---takes place in the analog domain via a network of phase shifters and possibly attenuators at radio frequency (hence "RF").

This staged approach to combining is common for practical transceivers with many antennas (e.g., massive MIMO, millimeter-wave communication) to reduce the number of costly and power hungry RF chains while still supplying high beamforming gains and supporting spatial multiplexing. 

The received signal vector is mixed with the analog combining matrix before being digitized after each of the $$L_{\mathrm{r}}$$ RF chains. In order to support spatial multiplexing of $$N_{\mathrm{s}}$$ streams, it should be such that

$$
L_{\mathrm{r}} \geq N_{\mathrm{s}}
$$

The signals from each antenna are distributed throughout the analog combining network before being passed to each of the RF chains---how this is done varies depending on the architecture. 

{% include image.html file="obj/receiver_hybrid/analog_combiner.svg" caption="A fully-connected analog combining network." %}

### Creating a Hybrid Digital/Analog Receiver {#creating}

To create a hybrid digital/analog receiver, a convenient way is to use

```matlab
rx = receiver.create('hybrid')
```

This will instantiate a `receiver_hybrid` object, which is inherited from the `receiver` object definition.

As such, the `receiver_hybrid` object inherits all properties and methods of the `receiver` object. However, some methods are overwritten in the `receiver_hybrid` object definition to appropriately accommodate the nature of hybrid digital/analog beamforming.


### Key Properties {#key-properties}

A `receiver_hybrid` object inherits all properties of the `receiver` object. We summarize the key properties of such as follows; please see documentation on the `receiver` object for more details.

```matlab
rx.combiner
rx.receive_symbol
rx.received_signal
rx.noise
rx.noise_energy_per_symbol
rx.noise_power_per_Hz
rx.noise_covariance
rx.symbol_bandwidth
rx.symbol_period
rx.array
```

Naturally, the biggest difference between a fully-digital `receiver` object and a hybrid digital/analog `receiver_hybrid` object lay in combining. Recall, however, the effective combining matrix of a hybrid digital/analog receiver is the product of its digital and analog combiners.

$$
\mathbf{W} = \mathbf{W}_{\mathrm{RF}} \mathbf{W}_{\mathrm{BB}}
$$

To capture the digital combining matrix $$\mathbf{W}_{\mathrm{BB}}$$ and the analog combining matrix $$\mathbf{W}_{\mathrm{RF}}$$, the `receiver_hybrid` object has the following properties

```matlab
rx.combiner_digital
rx.combiner_analog
```

Their product $$\mathbf{W}_{\mathrm{RF}} \mathbf{W}_{\mathrm{BB}}$$ is captured by the inherited property

```matlab
rx.combiner
```

from the `receiver` object.

Capturing the number of RF chains at the hybrid receiver is the property

```matlab
rx.num_rf_chains
```

It is common for practical analog beamforming networks to be controlled digitally using limited-resolution phase shifters and attenuators. To capture this, MFM's `receiver_hybrid` object has the following properties

```matlab
rx.combiner_analog_phase_resolution_bits
rx.combiner_analog_amplitude_resolution_bits
rx.combiner_analog_amplitude_quantization_law
```

As their names suggest `rx.combiner_analog_phase_resolution_bits` and `rx.combiner_analog_amplitude_resolution_bits` are the number of bits used to control each phase shifter and each attenuator, respectively. The property `rx.combiner_analog_amplitude_quantization_law` provides some freedom in how MFM captures finite-resolution attenuators. We discuss more on phase and amplitude control in [Limited Phase Control](#limited-phase-control) and [Limited Amplitude Control](#limited-amplitude-control).


### Limited Phase Control {#limited-phase-control}

To set the number of bits used to configure each phase shifter in the analog beamforming network to `bits`, use

```matlab 
rx.set_combiner_analog_amplitude_resolution_bits(bits)
```

MFM assumes uniform phase shifter quantization, meaning the phases it can implement are on the range $$[0,2\pi)$$ with step size $$2\pi/2^{b_{\mathrm{phs}}}$$, where each phase shifter has $$b_{\mathrm{phs}}$$ bits of phase control.

To check to see if an analog combining matrix `W_RF` satisfies the phase contraints of the hybrid receiver's analog beamforming network, one can use

```matlab 
out = check_combiner_analog_phase_constraint(W_RF)
```

where `out` is `true` if satisfied and `false` if any entries of `W_RF` violate the phase constraints.

It is also common in academic research to assume phase shifters can take on any value to remove the impact of having limited phase resolution. To realize this in MFM, simply use

```matlab 
rx.set_combiner_analog_amplitude_resolution_bits(Inf)
```

This removes any phase constraint on the entries of the analog combiner.

### Limited Amplitude Control {#limited-amplitude-control}

To set the number of bits used to configure each attenuator in the analog beamforming network to `bits`, use

```matlab 
rx.set_combiner_analog_amplitude_resolution_bits(bits)
```

To capture the case where there are no attenuators in the analog combining network, use

```matlab 
rx.set_combiner_analog_amplitude_resolution_bits(0)
```

which will impose a unit amplitude constraint on the entries of the analog combining matrix.

To capture the case where the attenuators are not faced with amplitude quantization, use

```matlab 
rx.set_combiner_analog_amplitude_resolution_bits(Inf)
```

which allows the magnitude of the entries of the analog combining matrix to take on any value between $$[0,1]$$.

In a similar fashion, MFM assumes uniform attenuator quantization offers users two options: linear quantization or logarithmic quantization.



### Connected-ness of the Hybrid Receiver {#connected-ness}

Fully-connected architectures, for example, supply each receive antenna with a weighted version of each RF chain's signal. In other words, each RF chain is connected to each antenna.

Partially-connected architectures, on the other hand, only supply certain receive antennas (often groups of antennas) with a weighted version of the signals from certain RF chains.

The connected-ness of a hybrid digital/analog architecture is reflected by the structure of the analog combining matrix $$\mathbf{W}_{\mathrm{RF}}$$. The $$(i,j)$$-th entry is only nonzero if there exists a weighted connection between the $$i$$-th antenna and the $$j$$-th RF chain.

To set which RF chains are connected to which antennas, MFM supplies the function

```matlab
rx.set_combiner_hybrid_connections(M)
```

where `M` is an $$N_{\mathrm{r}} \times L_{\mathrm{r}}$$ matrix (like the analog combining matrix) whose $$(i,j)$$-th entry is `1` if there exists a weighted connection between the $$i$$-th antenna and the $$j$$-th RF chain and `0` if not.

Fully-connected architectures, which are assumed by default, have an `M` matrix of all ones.

Sub-array architectures, for example, have an `M` matrix with a block diagonal of ones.


### Example Setup {#example-setup}

A typical hybrid digital/analog receiver setup looks something similar to

```matlab
rx = receiver.create('hybrid')
rx.set_symbol_bandwidth(B)
rx.set_num_streams(Ns)
rx.set_set_array(a)
rx.set_noise_power_per_Hz(noise_psd,'dBm_Hz')
```

where `B` is the bandwidth in Hz, `Ns` is the number of streams to multiplex, `a` is an `array` object, `noise_psd` is the noise power per Hz in dBm/Hz (e.g., -174).

After this initial setup, the receiver `rx` can be configured to receive symbol vectors using its combiner.


### Setting the Digital Combiner {#setting-digital-combiner}

The most straightforward way to set the digital combining matrix is to use

```matlab
rx.set_combiner_digital(W_BB)
```

where `W_BB` is an $$L_{\mathrm{r}} \times N_{\mathrm{s}}$$ combining matrix; if the combining matrix is not of appropriate size based on the current number of RF chains and number of streams, MFM will alert the user and it will not set the digital combiner.


### Setting the Analog Combiner {#setting-analog-combiner}

The most straightforward way to set the analog combining matrix is to use

```matlab
rx.set_combiner_analog(W_RF)
```

where `W_RF` is an $$N_{\mathrm{r}} \times L_{\mathrm{r}}$$ combining matrix; if the combining matrix is not of appropriate size based on the current number of antennas and number of RF chains, MFM will alert the user and it will not set the analog combiner.

Additionally, MFM will require that `W_RF` satisfy the analog combining constraints associated with phase and amplitude control.


### Shorthand Methods {#shorthand-methods}

To provide convenient ways to retrieve common MIMO-related quantities from a `receiver_hybrid` object `rx`, there exist the following so-called shorthand methods.

- `rx.W` --- Returns the combining matrix.
- `rx.W_RF` --- Returns the analog combining matrix.
- `rx.W_BB` --- Returns the digital combining matrix.
- `rx.Ns` --- Returns the number of streams.
- `rx.Lr` --- Returns the number of RF chains.
- `rx.Nr` --- Returns the number of receive antennas.
- `rx.s` --- Returns the receive symbol.
- `rx.y` --- Returns the signal vector arriving at the receiver.


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}
