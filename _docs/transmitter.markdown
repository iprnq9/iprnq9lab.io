---
layout: doc
title:  "Transmitter"
object: transmitter
file: obj/transmitter/transmitter.m
summary: A MIMO transmitter.
super: true
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "superclass", page.object %}
{% unless subs == empty %}Subclasses:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Creating a Transmitter](#creating)
- [Key Properties](#key-properties)
- [Setting the Transmit Symbol](#setting-transmit-symbol)
- [Setting the Number of Transmit Streams](#setting-number-streams)
- [Setting the Transmit Array](#setting-array)
- [Setting the Transmit Power](#setting-transmit-power)
- [Example Setup](#example-setup)
- [Precoding Power Budget](#precoding-power-budget)
- [Setting the Precoder](#setting-precoder)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `transmitter` object captures the properties and responsibilities of transmission. Its primary responsibility lay in MIMO precoding and applying a transmit power to produce the signal that departs from its antennas. 

{% include image.html file="obj/transmitter/transmitter.svg" %}

The $$N_{\mathrm{t}} \times 1$$ signal vector $$\mathrm{x}$$ leaving the transmitter and entering the channel can be written as

$$
\mathbf{x} = \sqrt{P_{\mathrm{tx}}} \cdot \mathbf{F} \mathbf{s}
$$

where $$\mathbf{s}$$ is the $$N_{\mathrm{s}} \times 1$$ complex transmit symbol vector, $$\mathbf{F}$$ is the $$N_{\mathrm{t}} \times N_{\mathrm{s}}$$ precoding matrix, and $$P_{\mathrm{tx}}$$ is the transmit energy per symbol, all of which are housed by and the responsibility of the `transmitter` object. While not strictly followed by MFM, we maintain this notation throughout this documentation to better communicate the material.

The `transmitter` object represents a fully-digital precoder, in contrast to a hybrid digital/analog precoder. As such, this page will focus specifically on the `transmitter` object while further documentation can be found on the hybrid digital/analog transmitter on its page.


### Creating a Transmitter {#creating}

To create a `transmitter` object `tx`, a convenient way is to use

```matlab
tx = transmitter.create();
```


### Key Properties {#key-properties}

A `transmitter`'s precoding matrix is captured by the property

```matlab
tx.precoder
```

Its transmit symbol is captured by

```matlab
tx.transmit_symbol
```

Its transmit power (in watts) is captured by

```matlab
tx.transmit_power
```

The symbol bandwidth (in Hz) and symbol period (in seconds) are captured by

```matlab
tx.symbol_bandwidth
tx.symbol_period
```


### Setting the Transmit Symbol {#setting-transmit-symbol}

To set the $$N_{\mathrm{s}} \times 1$$ complex transmit symbol vector $$\mathbf{s}$$, one can use

```matlab
tx.set_transmit_symbol(s)
```

where `s` is an $$N_{\mathrm{s}} \times 1$$ column vector.


### Setting the Number of Transmit Streams {#setting-number-streams}

To set the number of streams multiplexed at the transmitter $$N_{\mathrm{s}}$$, one can use

```matlab
tx.set_num_streams(Ns)
```

where `Ns` should be a positive integer.


### Setting the Transmit Array {#setting-array}

Every `transmitter` object has its own `array` object, capturing---at the very least---how many antennas the transmitter has. To set the transmit array, simply use

```matlab
tx.set_array(a)
```

where `a` is an array object.

It may be important to note that MFM performs a deep copy of the array before setting it at the transmitter.


### Setting the Transmit Power {#setting-transmit-power}

The transmit power is a critical component of any transmitter. Typically, engineers regard transmit power in terms of watts (joules per second) or decibel milliwatts (dBm).

The variable $$P_{\mathrm{tx}}$$ is often used in single-letter (symbol-level) MIMO equations captures the transmit "power" in units joules per symbol rather than joules per second; note that this is also often termed the energy per symbol, joules being a unit of energy and not of power.

Let us define $$B$$ as the symbol bandwidth (in Hz). Then, it is common to take $$T = 1 / B$$ as the symbol period (in seconds).

Thus, expending $$P_{\mathrm{tx}}$$ joules per symbol reduces to expending $$P_{\mathrm{tx}} / T$$ or $$P_{\mathrm{tx}} \times B$$ watts (joules per second).

From this, it is clear that setting the transmit "power" at the symbol level requires defining a symbol bandwidth/period. Therefore, it is critical that users set a symbol bandwidth when setting the transmit power in terms of watts.

For example, if the bandwidth is 50 MHz and we want to transmit with 1 watt, we can achieve the desired transmit energy per symbol via

```matlab
tx.set_symbol_bandwidth(50e6)
tx.set_transmit_power(1)
```

Note that setting the symbol bandwidth also sets the symbol period.

The `set_transmit_power()` function also accepts input arguments of the form

```matlab
tx.set_transmit_power(P,'dBm')
```

where `P` is in units of dBm.

Note that the transmit energy per symbol is stored in the property

```matlab
tx.transmit_energy_per_symbol
```

and is automatically updated anytime the transmit power or the symbol bandwidth change.

### Example Setup {#example-setup}

A typical `transmitter` setup looks something similar to

```matlab
tx = transmitter.create()
tx.set_symbol_bandwidth(B)
tx.set_num_streams(Ns)
tx.set_set_array(a)
tx.set_transmit_power(Ptx,'dBm')
```

where `B` is the bandwidth in Hz, `Ns` is the number of streams to multiplex, `a` is an `array` object, and `Ptx` is the transmit power in dBm.

After this initial setup, the transmitter `tx` can be configured to transmit symbol vectors using its precoder.


### Precoding Power Budget {#precoding-power-budget}

MFM supports the enforcement of a precoding power budget, as is ubiquitous in MIMO literature to limit and abstract out the transmit power.

A precoding power budget takes the form

$$
\lVert \mathbf{F} \rVert_{\mathrm{F}}^2 \leq P
$$

By default, the `transmitter` sets $$P = N_{\mathrm{s}}$$ to ensure that the average power of the transmitted vector $$\mathbf{x}$$ is in fact $$P_{\mathrm{tx}}$$ joules per symbol when the covariance of the transmit symbols is

$$
\mathbf{R}_{\mathbf{s}} = \mathbb{E} [ \mathbf{s} \mathbf{s}^{*} ] = \frac{1}{N_{\mathrm{s}}} \cdot \mathbf{I}
$$

via

$$
\begin{align}
\mathbb{E}\left[ \lVert \mathbf{x} \rVert_{2}^2 \right] 
&= \mathbb{E}\left[ \lVert \sqrt{P_{\mathrm{tx}}} \mathbf{F} \mathbf{s} \rVert_{2}^2 \right] \\
&= P_{\mathrm{tx}} \cdot \mathbb{E}\left[ \lVert \mathbf{F} \mathbf{s} \rVert_{2}^2 \right] \\
&= P_{\mathrm{tx}} \cdot \mathbb{E}\left[ \mathrm{trace} \left( \mathbf{F} \mathbf{s} \mathbf{s}^{*} \mathbf{F}^{*} \right) \right] \\
&= P_{\mathrm{tx}} \cdot \mathrm{trace}\left( \mathbb{E} \left[ \mathbf{F} \mathbf{s} \mathbf{s}^{*} \mathbf{F}^{*} \right] \right) \\
&= P_{\mathrm{tx}} \cdot \mathrm{trace}\left( \mathbf{F} \mathbb{E} \left[ \mathbf{s} \mathbf{s}^{*} \right] \mathbf{F}^{*} \right) \\
&= P_{\mathrm{tx}} \cdot \mathrm{trace}\left( \frac{1}{N_{\mathrm{s}}} \mathbf{F} \mathbf{F}^{*} \right) \\
&= \frac{P_{\mathrm{tx}}}{N_{\mathrm{s}}} \cdot \mathrm{trace}\left( \mathbf{F} \mathbf{F}^{*} \right) \\
&= \frac{P_{\mathrm{tx}}}{N_{\mathrm{s}}} \cdot \lVert \mathbf{F} \rVert_{\mathrm{F}}^2  \\
&\leq P_{\mathrm{tx}}
\end{align}
$$

```matlab
tx.set_precoder_power_budget(P)
```


### Setting the Precoder {#setting-precoder}

There are multiple routes to setting a transmitter's precoding matrix $$\mathbf{F}$$. The most straightforward within the scope of this `transmitter` documentation is to simply use

```matlab
tx.set_precoder(F)
```

where `F` is an $$N_{\mathrm{t}} \times N_{\mathrm{s}}$$ precoding matrix; if the precoding matrix is not of appropriate size based on the current array or number of streams, MFM will alert the user and it will not set the precoder.


### Shorthand Methods {#shorthand-methods}

To provide convenient ways to retrieve common MIMO-related quantities from a `transmitter` object `tx`, there exist the following so-called shorthand methods.

- `tx.F` --- Returns the precoding matrix.
- `tx.Ns` --- Returns the number of streams.
- `tx.Nt` --- Returns the number of transmit antennas.
- `tx.Ptx` --- Returns the transmit energy per symbol (joules per symbol).
- `tx.Rs` --- Returns the transmit symbol covariance matrix.
- `tx.s` --- Returns the transmit symbol.
- `tx.x` --- Returns the signal vector transmitted from the transmitter.


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}
