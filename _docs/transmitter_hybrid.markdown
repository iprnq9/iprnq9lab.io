---
layout: doc
title:  "Hybrid Digital/Analog Transmitter"
object: transmitter_hybrid
file: obj/transmitter/transmitter_hybrid.m
summary: A MIMO transmitter employing hybrid digital/analog precoding.
super: false
superclass: transmitter
category: transmitter
---

{% assign obj = {{ object }} %}
{% assign subs = site.docs | where: "object", page.superclass %}
{% unless subs == empty %}Superclass:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}

#### On This Page
- [About](#about)
- [Creating a Hybrid Digital/Analog Transmitter](#creating)
- [Key Properties](#key-properties)
- [Limited Phase Control](#limited-phase-control)
- [Limited Amplitude Control](#limited-amplitude-control)
- [Connected-ness of the Hybrid Transmitter](#connected-ness)
- [Example Setup](#example-setup)
- [Digital Precoding Power Budget](#digital-precoding-power-budget)
- [Setting the Digital Precoder](#setting-digital-precoder)
- [Setting the Analog Precoder](#setting-analog-precoder)
- [Shorthand Methods](#shorthand-methods)
- [List of Properties](#properties)
- [List of Methods](#methods)
- [Methods Documentation](#methods-documentation)


### About {#about}

The `transmitter_hybrid` object captures transmission from a hybrid digital/analog precoding transmitter. In addition to that of the `transmitter` object, a `transmitter_hybrid` object is responsible for achieving transmission via the combination of digital and analog precoding (rather than solely digital precoding like in the `transmitter` object).

{% include image.html file="obj/transmitter_hybrid/transmitter_hybrid.svg" %}

The $$N_{\mathrm{t}} \times 1$$ signal vector $$\mathrm{x}$$ leaving the hybrid transmitter and entering the channel can be written as

$$
\mathbf{x} = \sqrt{P_{\mathrm{tx}}} \cdot \mathbf{F}_{\mathrm{RF}} \mathbf{F}_{\mathrm{BB}} \mathbf{s}
$$

where $$\mathbf{s}$$ is the $$N_{\mathrm{s}} \times 1$$ complex transmit symbol vector, $$\mathbf{F}_{\mathrm{RF}}$$ is the $$N_{\mathrm{t}} \times L_{\mathrm{t}}$$ analog precoding matrix, $$\mathbf{F}_{\mathrm{BB}}$$ is the $$L_{\mathrm{t}} \times N_{\mathrm{s}}$$ digital precoding matrix, and $$P_{\mathrm{tx}}$$ is the transmit energy per symbol.

Digital precoding---represented by the matrix $$\mathbf{F}_{\mathrm{BB}}$$---takes place in the digital domain at baseband (hence "BB"). 

Analog precoding---represented by the matrix $$\mathbf{F}_{\mathrm{RF}}$$---takes place in the analog domain via a network of phase shifters and possibly attenuators at radio frequency (hence "RF").

This staged approach to precoding is common for practical transceivers with many antennas (e.g., massive MIMO, millimeter-wave communication) to reduce the number of costly and power hungry RF chains while still supplying high beamforming gains and supporting spatial multiplexing. 

Transmit symbols are mixed with the digital precoding matrix before being physically realized by $$L_{\mathrm{t}}$$ RF chains. In order to support spatial multiplexing of $$N_{\mathrm{s}}$$ streams, it should be such that

$$
L_{\mathrm{t}} \geq N_{\mathrm{s}}
$$

The signals from each RF chain are then distributed throughout the analog precoding network---how this is done varies depending on the architecture. 

{% include image.html file="obj/transmitter_hybrid/analog_precoder.svg" caption="A fully-connected analog precoding network." %}


### Creating a Hybrid Digital/Analog Transmitter {#creating}

To create a hybrid digital/analog transmitter, a convenient way is to use

```matlab
tx = transmitter.create('hybrid')
```

This will instantiate a `transmitter_hybrid` object, which is inherited from the `transmitter` object definition.

As such, the `transmitter_hybrid` object inherits all properties and methods of the `transmitter` object. However, some methods are overwritten in the `transmitter_hybrid` object definition to appropriately accommodate the nature of hybrid digital/analog beamforming.


### Key Properties {#key-properties}

A `transmitter_hybrid` object inherits all properties of the `transmitter` object. We summarize the key properties of such as follows; please see documentation on the `transmitter` object for more details.

```matlab
tx.precoder
tx.transmit_symbol
tx.transmit_power
tx.symbol_bandwidth
tx.symbol_period
tx.array
```

Naturally, the biggest difference between a fully-digital `transmitter` object and a hybrid digital/analog `transmitter_hybrid` object lay in precoding. Recall, however, the effective precoding matrix of a hybrid digital/analog transmitter is the product of its digital and analog precoders.

$$
\mathbf{F} = \mathbf{F}_{\mathrm{RF}} \mathbf{F}_{\mathrm{BB}}
$$

To capture the digital precoding matrix $$\mathbf{F}_{\mathrm{BB}}$$ and the analog precoding matrix $$\mathbf{F}_{\mathrm{RF}}$$, the `transmitter_hybrid` object has the following properties

```matlab
tx.precoder_digital
tx.precoder_analog
```

Their product $$\mathbf{F}_{\mathrm{RF}} \mathbf{F}_{\mathrm{BB}}$$ is captured by the inherited property

```matlab
tx.precoder
```

from the `transmitter` object.

Capturing the number of RF chains at the hybrid transmitter is the property

```matlab
tx.num_rf_chains
```

It is common for practical analog beamforming networks to be controlled digitally using limited-resolution phase shifters and attenuators. To capture this, MFM's `transmitter_hybrid` object has the following properties

```matlab
tx.precoder_analog_phase_resolution_bits
tx.precoder_analog_amplitude_resolution_bits
tx.precoder_analog_amplitude_quantization_law
```

As their names suggest `tx.precoder_analog_phase_resolution_bits` and `tx.precoder_analog_amplitude_resolution_bits` are the number of bits used to control each phase shifter and each attenuator, respectively. The property `tx.precoder_analog_amplitude_quantization_law` provides some freedom in how MFM captures finite-resolution attenuators. We discuss more on phase and amplitude control in [Limited Phase Control](#limited-phase-control) and [Limited Amplitude Control](#limited-amplitude-control).


### Limited Phase Control {#limited-phase-control}

To set the number of bits used to configure each phase shifter in the analog beamforming network to `bits`, use

```matlab 
tx.set_precoder_analog_amplitude_resolution_bits(bits)
```

MFM assumes uniform phase shifter quantization, meaning the phases it can implement are on the range $$[0,2\pi)$$ with step size $$2\pi/2^{b_{\mathrm{phs}}}$$, where each phase shifter has $$b_{\mathrm{phs}}$$ bits of phase control.

To check to see if an analog precoding matrix `F_RF` satisfies the phase contraints of the hybrid transmitter's analog beamforming network, one can use

```matlab 
out = check_precoder_analog_phase_constraint(F_RF)
```

where `out` is `true` if satisfied and `false` if any entries of `F_RF` violate the phase constraints.

It is also common in academic research to assume phase shifters can take on any value to remove the impact of having limited phase resolution. To realize this in MFM, simply use

```matlab 
tx.set_precoder_analog_amplitude_resolution_bits(Inf)
```

This removes any phase constraint on the entries of the analog precoder.

### Limited Amplitude Control {#limited-amplitude-control}

To set the number of bits used to configure each attenuator in the analog beamforming network to `bits`, use

```matlab 
tx.set_precoder_analog_amplitude_resolution_bits(bits)
```

To capture the case where there are no attenuators in the analog precoding network, use

```matlab 
tx.set_precoder_analog_amplitude_resolution_bits(0)
```

which will impose a unit amplitude constraint on the entries of the analog precoding matrix.

To capture the case where the attenuators are not faced with amplitude quantization, use

```matlab 
tx.set_precoder_analog_amplitude_resolution_bits(Inf)
```

which allows the magnitude of the entries of the analog precoding matrix to take on any value between $$[0,1]$$.

In a similar fashion, MFM assumes uniform attenuator quantization offers users two options: linear quantization or logarithmic quantization.



### Connected-ness of the Hybrid Transmitter {#connected-ness}

Fully-connected architectures, for example, supply each transmit antenna with a weighted version of each RF chain's signal. In other words, each RF chain is connected to each antenna.

Partially-connected architectures, on the other hand, only supply certain transmit antennas (often groups of antennas) with a weighted version of the signals from certain RF chains.

The connected-ness of a hybrid digital/analog architecture is reflected by the structure of the analog precoding matrix $$\mathbf{F}_{\mathrm{RF}}$$. The $$(i,j)$$-th entry is only nonzero if there exists a weighted connection between the $$i$$-th antenna and the $$j$$-th RF chain.

To set which RF chains are connected to which antennas, MFM supplies the function

```matlab
tx.set_precoder_hybrid_connections(M)
```

where `M` is an $$N_{\mathrm{t}} \times L_{\mathrm{t}}$$ matrix (like the analog precoding matrix) whose $$(i,j)$$-th entry is `1` if there exists a weighted connection between the $$i$$-th antenna and the $$j$$-th RF chain and `0` if not.

Fully-connected architectures, which are assumed by default, have an `M` matrix of all ones.

Sub-array architectures, for example, have an `M` matrix with a block diagonal of ones.

### Example Setup {#example-setup}

A typical `transmitter` setup looks something similar to

```matlab
tx = transmitter.create()
tx.set_symbol_bandwidth(B)
tx.set_num_streams(Ns)
tx.set_set_array(a)
tx.set_transmit_power(Ptx,'dBm')
```

where `B` is the bandwidth in Hz, `Ns` is the number of streams to multiplex, `a` is an `array` object, and `Ptx` is the transmit power in dBm.

After this initial setup, the transmitter `tx` can be configured to transmit symbol vectors using its precoder.


### Digital Precoding Power Budget {#digital-precoding-power-budget}

MFM supports the enforcement of a power budget on the digital precoding matrix, similar to what was placed on the precoding matrix in the `transmitter`.

A digital precoding power budget takes the form

$$
\lVert \mathbf{F}_{\mathrm{BB}} \rVert_{\mathrm{F}}^2 \leq P
$$

where $$P$$ is the maximum power of the digital precoding matrix.

To set this, use

```matlab
tx.set_precoder_digital_power_budget(P)
```

### Setting the Digital Precoder {#setting-digital-precoder}

The most straightforward way to set the digital precoding matrix is to use

```matlab
tx.set_precoder_digital(F_BB)
```

where `F_BB` is an $$L_{\mathrm{t}} \times N_{\mathrm{s}}$$ precoding matrix; if the precoding matrix is not of appropriate size based on the current number of RF chains and number of streams, MFM will alert the user and it will not set the digital precoder.


### Setting the Analog Precoder {#setting-analog-precoder}

The most straightforward way to set the analog precoding matrix is to use

```matlab
tx.set_precoder_analog(F_RF)
```

where `F_RF` is an $$N_{\mathrm{t}} \times L_{\mathrm{t}}$$ precoding matrix; if the precoding matrix is not of appropriate size based on the current number of antennas and number of RF chains, MFM will alert the user and it will not set the analog precoder.

Additionally, MFM will require that `F_RF` satisfy the analog precoding constraints associated with phase and amplitude control.


### Shorthand Methods {#shorthand-methods}

To provide convenient ways to retrieve common MIMO-related quantities from a `transmitter_hybrid` object `tx`, there exist the following so-called shorthand methods.

- `tx.F` --- Returns the precoding matrix.
- `tx.F_RF` --- Returns the analog precoding matrix.
- `tx.F_BB` --- Returns the digital precoding matrix.
- `tx.Ns` --- Returns the number of streams.
- `tx.Lt` --- Returns the number of RF chains.
- `tx.Nt` --- Returns the number of transmit antennas.
- `tx.Ptx` --- Returns the transmit energy per symbol (joules per symbol).
- `tx.Rs` --- Returns the transmit symbol covariance matrix.
- `tx.s` --- Returns the transmit symbol.
- `tx.x` --- Returns the signal vector transmitted from the transmitter.


### List of Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### List of Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}


### Methods Documentation {#methods-documentation}

{% include /_autodocs/{{ page.object }}_methods.txt %}
