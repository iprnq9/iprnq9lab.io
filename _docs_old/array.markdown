---
layout: doc
title:  "Array"
object: array
file: obj/array/array.m
summary: An arbitrary antenna array.
super: true
---

The `array` object is used to represent an antenna array. The characteristics of the array are largely dictated by the geometry of its array elements. MFM's `array` object is not restricted to uniform linear arrays (ULAs) or uniform planar arrays (UPAs); the array's elements can be placed arbitrarily, allowing for users to experiment with different array designs.

{% include image.html file="feature_array.svg" caption="Array pattern generated from an 8-element ULA using MFM." %}


## Creation and Example Setup

To create a `array` object, simply call `array()` as shown below. Calling `array(Na)` will create a ULA with `Na` elements along the x-axis.

{% highlight matlab %}
a = array(8); % creates an 8-element ULA along the x-axis
{% endhighlight %}


## Viewing the Array

To view the array geometry, use the functions `array.show2d()` or `array.show3d()`.

{% include image.html file="array_space.svg" caption="An 8-element ULA shown in 3-D using `array.show3d()`." %}


## Viewing the Array Pattern

Several functions can be used to view the response of an array.
 - `array.show_array_pattern()` displays the array's magnitude and phase responses in the azimuth and elevation cuts.
 - `array.show_array_pattern_azimuth()` displays the array's magntidude response as a function of the azimuth angle.
 - `array.show_array_pattern_elevation()` displays the array's magntidude response as a function of the elevation angle.
 - `array.show_polar_array_pattern_azimuth()` displays the array's magntidude response as a function of the azimuth angle in a polar fashion.
 - `array.show_polar_array_pattern_elevation()` displays the array's magntidude response as a function of the elevation angle in a polar fashion.

Note that these all plot the *weighted array pattern*, meaning they account for the array response and any weights set to the array.


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
