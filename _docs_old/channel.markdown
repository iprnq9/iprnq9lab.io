---
layout: doc
title:  "Channel"
object: channel
file: obj/channel/channel.m
summary: A generic MIMO channel.
super: true
---

The `channel` object is an important object because it is the superclass (or parent) for the other channel models in MFM. The `channel` object does not incorporate any meaningful channel characteristics but rather the properties and functions that other, more useful channel objects will use.


## Creation and Example Setup

Before using the `channel` object (or any of the other channel objects), it is important to set a few core properties.

{% highlight matlab %}
C = channel();
C.set_propagation_velocity(propagation_velocity_meters_per_sec);
C.set_carrier_frequency(carrier_frequency_Hz);
C.set_transmit_array(tx_array);
C.set_receive_array(rx_array);
{% endhighlight %}

Given that a channel is comprised as the propagation from a transmit array to a receive array, it is necessary that the channel be aware of these arrays (in most cases).


## Important Properties

The `channel` object is created with the following properties, which will be common to all channel models.

{% highlight matlab %}
name; % human-readable identifier
H; % Nr-by-Nt channel matrix
Nt; % number of transmit antennas
Nr; % number of receive antennas
atx; % transmit array object
arx; % receive array object
carrier_frequency; % carrier frequency (Hz)
carrier_wavelength; % carrier wavelength (m)
propagation_velocity; % propagation velocity (m/s)
{% endhighlight %}

These properties can be accessed simply via `C.carrier_frequency` or `C.H`, for example. Anytime a channel model realizes a channel matrix, the property `H` is set.


## Subclasses

<ul>
    {% for doc in site.docs %}
    {% if doc.superclass contains 'channel' %}
    <li><a href="{{ doc.url }}">{{ doc.title }} ({{ doc.object }})</a></li>
    {% endif %}
    {% endfor %}
</ul>



## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
