---
layout: doc
title:  "Saleh-Valenzuela Channel Model"
object: channel_mimo_ray_cluster
superclass: channel
summary: A channel commprised of the combination of discrete rays.
file: obj/channel/channel_mimo_ray_cluster.m
---


The `{{ page.object }}` represents a channel comprised of the combination of discrete rays propagating from a transmit array to a receive array.

This is a common model for millimeter-wave channels, who are highly reflective.

{% if page.superclass %}
The `{{ page.object }}` object is a subclass of the `{{ page.superclass }}` object.
{% endif %}

## Saleh-Valenzuela Channel

The Saleh-Valenzuela (ray/cluster) channel representation is a popular way to model the propagation of far-field mmWave channels.

$$
\mathbf{H} = \sum_{v=1}^{N_{\mathrm{clust}}} \sum_{u=1}^{N_{\mathrm{rays}}} \beta_{u,v} \mathbf{a}_{r}(\mathrm{AoA}_{u,v}) \mathbf{a}_{t}(\mathrm{AoD}_{u,v})^{*}
$$

The channel between two arrays is modeled as the combination of multiple clusters of rays. Each ray has its own three unique characteristics:
1. complex gain
2. angle of departure (AoD)
3. angle of arrival (AoA)

The complex gain captures the magnitude and phase of propagation along the ray. The AoD captures the angle of departure from the transmit array. Likewise, the AoA captures the angle of arrival at the receive array. The transmit and receive arrays' responses at the AoD and AoA, respectively, construct a rank-1 channel matrix for that ray. The composition of multiple rays creates the overall channel matrix.

There are two quantities that dictate the number of rays that a channel has:
1. number of clusters
2. number of rays per cluster

The number of clusters represents a collection of rays. The rays within a given cluster may be correlated in some sense. In the case of MFM, the AoDs and AoAs associated with a given cluster are Laplacian distributed sharing a common mean. The gains of the rays are independent.

If you want completely independent rays (gain, AoD, and AoA), simply set the number of rays per cluster to one.


## Creation and Example Setup

To create a channel boject based on the Saleh-Valenzuela channel model, simply call `C = channel_mimo_ray_cluster()` as shown below. After its creation, several parameters must be set before the channel is ready for use.

{% highlight matlab %}
C = channel_mimo_ray_cluster();
C.set_propagation_velocity(propagation_velocity_meters_per_sec);
C.set_carrier_frequency(carrier_frequency_Hz);
C.set_ray_gain_distribution(ray_gain_opt);
C.set_ray_angle_distribution(ray_angle_opt);
C.set_AoD_range(AoD_range);
C.set_AoA_range(AoA_range);
C.set_num_clusters_range(num_clusters_range);
C.set_num_rays_per_cluster_range(num_rays_per_cluster_range);
C.set_transmit_array(tx_array);
C.set_receive_array(rx_array);
{% endhighlight %}

The necessary setup parameters are of the following fashion. These values, of course, can be tailored as desired.

{% highlight matlab %}
propagation_velocity_meters_per_sec = 3e8; % meters per second
carrier_frequency_Hz = 30e9; % 30 GHz
ray_gain_opt.type = 'cgauss'; % distribution of ray gain (complex Gaussian)
ray_angle_opt.type = 'laplacian'; % distribution of rays within a cluster
ray_angle_opt.std_dev = [0.2, 0.2]; % [azimuth, elevation]
AoD_range = [-pi/2, pi/2; 0, 0]; % [azimuth start, azimuth stop; elevation start, elevation stop]
AoA_range = [-pi/2, pi/2; 0, 0]; % [azimuth start, azimuth stop; elevation start, elevation stop]
num_clusters_range = [1,6]; % [min clusters, max clusters]
num_rays_per_cluster_range = [1,10]; % [min rays per cluster, max rays per cluster]
tx_array = array(32); % 32 element ULA
rx_array = array(16); % 16 element ULA
{% endhighlight %}


## Channel Realization

Now that the channel parameters have been set, generating a random channel is a single line. Random channel parameters will be realized based on the parameters set during its construction and setup. This makes it convenient to generate a new channel matrix---within a for-loop, for example.

{% highlight matlab %}
H = C.channel_realization(); % returns a channel matrix
{% endhighlight %}


## Show the Beamspace

The beamspace representation of a channel is captured by the gain of a ray as a function of its AoD and AoA. To view the azimuth beamspace and elevation beamspace use the following.

{% highlight matlab %}
C.show_beamspace_azimuth();
C.show_beamspace_elevation();
{% endhighlight %}

{% include image.html file="feature_aod_aoa.svg" caption="Beamspace respresentation of a realization of the Saleh-Valenzuela channel model." %}


## References
- _[An Overview of Signal Processing Techniques for Millimeter Wave MIMO Systems](https://arxiv.org/pdf/1512.03007.pdf)_


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
