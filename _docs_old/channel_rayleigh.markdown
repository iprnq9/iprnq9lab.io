---
layout: doc
title:  "Rayleigh-Faded Channel"
object: channel_rayleigh
superclass: channel
summary: A MIMO channel with Rayleigh-faded entries.
file: obj/channel/channel_rayleigh.m
---

The `channel_rayleigh` object is a simple model. It is used to represent channels whose channel matrix has entries that are Rayleigh-faded.

{% if page.superclass %}
The `{{ page.object }}` object is a subclass of the `{{ page.superclass }}` object.
{% endif %}


## Creation and Example Setup

Albeit a simple channel model, the `channel_rayleigh` object requires a few steps in its setup. While the propagation velocity and carrier frequency are not necessary, it is recommended they be set for completeness. The transmit and receive arrays should be set, however, as this dictates the size of the channel matrix.

{% highlight matlab %}
C = channel_rayleigh();
C.set_propagation_velocity(propagation_velocity_meters_per_sec);
C.set_carrier_frequency(carrier_frequency_Hz);
C.set_transmit_array(tx_array);
C.set_receive_array(rx_array);
{% endhighlight %}


## Channel Realization

Generating a random channel is a single line, which produces a channel matrix whose entries are drawn from a complex normal distribution (mean of 0 and variance of 1).

{% highlight matlab %}
H = C.channel_realization(); % returns a channel matrix
{% endhighlight %}


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
