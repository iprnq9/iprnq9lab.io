---
layout: doc
title:  "Spherical-Wave Channel Model"
object: channel_spherical_wave_mimo
superclass: channel
summary: A simple near-field MIMO channel model.
file: obj/channel/channel_spherical_wave_mimo.m
---

The `{{ page.object }}` is used to represent a spherical-wave channel where the spherical nature of a wavefront is considered, rather than the common planar wavefront assumption.

{% if page.superclass %}
The `{{ page.object }}` object is a subclass of the `{{ page.superclass }}` object.
{% endif %}


## Spherical-Wave MIMO

The spherical-wave channel model is a simple way to model the propagation of near-field channels. This channel, as its name suggest, captures the spherical shape that waves exhibit during propagation from an antenna (i.e., non-planar wave front). We often assume a far-field condition as been satisfied for many practical applications, which often make analysis, especially with arrays, much more straightforward. In the near-field, however, the effects of a non-planar wave front are exaggerated if ignored.

This channel, in its current form, has no randomness: the relative geometry between a transmit array and receive array and the carrier wavelength dictate the spherical-wave channel matrix. This channel simply draws a ray between each pair of transmit and receive antennas, calculating the gain and phase based on the separation of the two antennas.

Explicitly, the complex gain between transmit antenna $n$ and receive antenna $m$ is

$$
[\mathbf{H}]_{n,m} = \frac{\rho}{r_{m,n}}\exp (-\mathrm{j} 2 \pi \frac{r_{m,n}}{\lambda_{c}} )
$$

where $r_{m,n}$ is the distance between antenna elements, $\lambda_{c}$ is the carrier wavelength, and $\rho$ is a normalization factor.

{% include image.html file="spherical.png" caption="The spherical wave front propagates from a transmit antenna to a receive antenna." %}


## Creation and Example Setup

To create a channel boject based on the Saleh-Valenzuela channel model, simply call `C = channel_spherical_wave_mimo()` as shown below. After its creation, several parameters must be set before the channel is ready for use.

{% highlight matlab %}
C = channel_spherical_wave_mimo();
C.set_propagation_velocity(propagation_velocity_meters_per_sec);
C.set_carrier_frequency(carrier_frequency_Hz);
C.set_transmit_array(tx_array);
C.set_receive_array(rx_array);
{% endhighlight %}

The necessary setup parameters are of the following fashion. These values, of course, can be tailored as desired.

{% highlight matlab %}
propagation_velocity_meters_per_sec = 3e8; % meters per second
carrier_frequency_Hz = 30e9; % 30 GHz
tx_array = array(32); % 32 element ULA
rx_array = array(16); % 16 element ULA
{% endhighlight %}


## Channel Realization

Now that the channel parameters have been set, generating the channel matrix is a single line. Since this channel is completely based on geometry, its channel matrix is deterministic for a given relative transmit and receive geometry (and wavelength).

{% highlight matlab %}
H = C.channel_realization(); % returns a channel matrix
{% endhighlight %}


## References
- _[Spherical-Wave Model for Short-Range MIMO](http://wireless-systems.ece.gatech.edu/nsf3/pubs/ingram/spherical%20wave.pdf)_


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
