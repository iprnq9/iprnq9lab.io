---
layout: doc
title:  "Full-Duplex Node"
object: full_duplex_node
superclass: node
file: obj/node/full_duplex_node.m
summary: An in-band full-duplex MIMO transceiver.
---

The `{{ page.object }}` represents a transceiver that operates in an in-band full-duplex fashion (rather than in a half-duplex one).

{% if page.superclass %}
The `{{ page.object }}` object is a subclass of the `{{ page.superclass }}` object.
{% endif %}


## Creation and Example Setup

To create a `full_duplex_node` object, simply call `full_duplex_node()` as shown below.

{% highlight matlab %}
n = full_duplex_node();
{% endhighlight %}

The `full_duplex_node` object comes with some MIMO strategies for full-duplex that can be set using `set_bfc_options`. These strategies are not fleshed out, however, so it is suggested that precoding and combining be set explicitly.


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
