---
layout: doc
title:  "Half-Duplex Node"
object: half_duplex_node
superclass: node
file: obj/node/half_duplex_node.m
summary: A conventional half-duplex MIMO transceiver.
---

The `{{ page.object }}` object is used for the specific case when a `node` is half-duplex one (rather than a full-duplex one). This is, in general, the case since conventional transceivers operate in a half-duplex fashion (even if they are capable of transmission and reception).

{% if page.superclass %}
The `{{ page.object }}` object is a subclass of the `{{ page.superclass }}` object.
{% endif %}


## Creation and Example Setup

To create a `half_duplex_node` object, simply call `half_duplex_node()` as shown below.

{% highlight matlab %}
n = half_duplex_node();
{% endhighlight %}

It can be set up and used further as the `node` object is.


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
