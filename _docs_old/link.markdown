---
layout: doc
title:  "Link"
object: link
file: obj/link/link.m
summary: A physical connection between two devices.
super: true
---

The `transmitter` object is used to represent the portion of a device responsible for transmission, including MIMO precoding.


## Creation and Example Setup

To create a `transmitter` object, simply call `transmitter()` as shown below.

{% highlight matlab %}
tx = transmitter();
{% endhighlight %}


## Subclasses

<ul>
    {% for doc in site.docs %}
    {% if doc.categories contains 'transmitter' %}
    <li><a href="{{ doc.url }}">{{ doc.title }} ({{ doc.object }})</a></li>
    {% endif %}
    {% endfor %}
</ul>


### Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}

{% include /_autodocs/{{ page.object }}_methods.txt %}


## Source


