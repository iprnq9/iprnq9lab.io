---
layout: doc
title:  "Millimeter-Wave Receiver"
object: mmwave_receiver
superclass: receiver
file: obj/receiver/mmwave_receiver.m
summary: A millimeter-wave MIMO receiver.
---

The `mmwave_receiver` extends the `receiver` to include the qualities of a millimeter-wave receiver, including hybrid analog/digital combining.

{% if page.superclass %}
The `{{ page.object }}` object is a subclass of the `{{ page.superclass }}` object.
{% endif %}


## Creation and Example Setup

To create a `mmwave_receiver` object, simply call `mmwave_receiver()` as shown below.

```
rx = mmwave_receiver();
```


## Hybrid Combining

The `{{ page.object }}` implements hybrid combining using an analog combiner followed by a digital combiner. The properties `combiner_digital` and `combiner_analog` are matrices corresponding to the number of antennas in the object's array, the number of radio frequency (RF) chains, and the number of streams.

```
rx.combiner_digital;
rx.combiner_analog;
```

The matrix product of `precoder_analog` times `precoder_digital` is the effective combiner, which is an inherited property from the superclass `combiner`.

There are built-in hybrid approximation functions, though you are free to arbitrarily set the digital and analog stages as desired.

## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
