---
layout: doc
title:  "Millimeter-Wave Transmitter"
object: mmwave_transmitter
superclass: transmitter
file: obj/transmitter/mmwave_transmitter.m
summary: A millimeter-wave MIMO transmitter.
---

The `{{ page.object }}`  extends the `transmitter` object to include the qualities of a millimeter-wave transmitter, including hybrid analog/digital precoding.

{% if page.superclass %}
The `{{ page.object }}` object is a subclass of the `{{ page.superclass }}` object.
{% endif %}


## Creation and Example Setup

To create a `mmwave_transmitter` object, simply call `mmwave_transmitter()` as shown below.

```
tx = mmwave_transmitter();
```


## Hybrid Precoding

The `{{ page.object }}` implements hybrid precoding using a digital precoder followed by an analog precoder. The properties `precoder_digital` and `precoder_analog` are matrices corresponding to the number of antennas in the object's array, the number of radio frequency (RF) chains, and the number of streams.

```
tx.precoder_digital;
tx.precoder_analog;
```

The matrix product of `precoder_analog` times `precoder_digital` is the effective precoder, which is an inherited property from the superclass `transmitter`.

There are built-in hybrid approximation functions, though you are free to arbitrarily set the digital and analog stages as desired.


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
