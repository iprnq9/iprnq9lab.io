---
layout: doc
title:  "Network"
object: network_mfm
file: obj/network/network_mfm.m
summary: A network of devices and links.
super: true
---

## Creation and Example Setup

To create a `network_mfm` object, simply call `transmitter()` as shown below.

{% highlight matlab %}
tx = transmitter();
{% endhighlight %}


## Subclasses

<ul>
    {% for doc in site.docs %}
    {% if doc.categories contains 'transmitter' %}
    <li><a href="{{ doc.url }}">{{ doc.title }} ({{ doc.object }})</a></li>
    {% endif %}
    {% endfor %}
</ul>


### Properties {#properties}

{% include /_autodocs/{{ page.object }}_properties_list.txt %}


### Methods {#methods}

{% include /_autodocs/{{ page.object }}_methods_list.txt %}

{% include /_autodocs/{{ page.object }}_methods.txt %}


## Source


