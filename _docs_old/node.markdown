---
layout: doc
title:  "Node"
object: node
file: obj/node/node.m
summary: A device comprised of a transmitter and receiver.
super: true
---

The `node` object is used to represent a wireless transceiver (i.e., a device).


## Creation and Example Setup

To create a `node` object, simply call `node()` as shown below.

{% highlight matlab %}
n = node();
n.set_transmitter(tx); % a transmitter object
n.set_receiver(rx); % a receiver object
{% endhighlight %}


## Subclasses

<ul>
    {% for doc in site.docs %}
    {% if doc.superclass contains 'node' %}
    <li><a href="{{ doc.url }}">{{ doc.title }} ({{ doc.object }})</a></li>
    {% endif %}
    {% endfor %}
</ul>


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
