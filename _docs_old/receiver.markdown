---
layout: doc
title:  "Receiver"
object: receiver
file: obj/receiver/receiver.m
summary: A MIMO receiver.
super: true
---

The `receiver` object is used to represent the portion of a device responsible for reception, including MIMO combining.


## Creation and Example Setup

To create a `receiver` object, simply call `receiver()` as shown below.

{% highlight matlab %}
rx = receiver();
{% endhighlight %}


## Subclasses

<ul>
    {% for doc in site.docs %}
    {% if doc.superclass contains 'receiver' %}
    <li><a href="{{ doc.url }}">{{ doc.title }} ({{ doc.object }})</a></li>
    {% endif %}
    {% endfor %}
</ul>


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
