---
layout: doc
title:  "Space"
object: space
file: obj/space/space.m
summary: An object used to house nodes and channels in 3-D space.
super: true
---

The `space` object is used to house nodes and channels. It handles the interactions between nodes and channels. For example, the space object passes channel state information (CSI) to the transmitting and/or receiving node(s) based on the channel that exists between them. Additionally, evaluating performance between nodes (e.g., spectral efficiency) and automatically computing interference channels is handled by the space object. It is essentially a wrapper for the entire universe of nodes and channels to live.


## Creation and Example Setup

To create a `space` object, simply call `space()` as shown below. After its creation, some basic parameters must be set before it's ready for use.

{% highlight matlab %}
s = space();
s.set_propagation_velocity(propagation_velocity_meters_per_sec);
s.set_carrier_frequency(carrier_frequency_Hz);
{% endhighlight %}

The necessary setup parameters are of the following fashion. These values, of course, can be tailored as desired.

{% highlight matlab %}
propagation_velocity_meters_per_sec = 3e8; % meters per second
carrier_frequency_Hz = 30e9; % 30 GHz
{% endhighlight %}


## Source

{% highlight matlab %}
{% include /source/{{ page.file }} %}
{% endhighlight %}
