---
layout: default
title:  "Two User Scenario with Hybrid Beamforming at Millimeter-Wave"
tags: [mmwave]
---

# Two User Scenario with Hybrid Beamforming at Millimeter-Wave

Test

## Introduction

$$
\mathbf{y} = \mathbf{A} \mathbf{x}
$$


{% highlight matlab %}
clearvars; clc;

% Setup
N = 128; % length of basis
n = (0:N-1)';
K = 128; % number of basis functions
{% endhighlight %}



