---
layout: guide
title:  "3-D Geometry in MFM"
summary: Details on the 3-D geometry conventions used by MFM.
date: 2021-01-13
published: true
---

An important element of MFM is the convention used for describing 3-D geometry. This is particularly important to when placing devices, using antenna arrays, and working with channel objects.

MFM uses azimuth and elevation angles to describe 3-D angular information.

Azimuth---shown below as $$\theta$$---is defined as the angle from the y-axis to the projection of the vector of interest onto the x-y plane. Azimuth angles range from $$-\pi$$ to $$\pi$$.

Elevation---shown below as $$\phi$$---is defined as the angle from the x-y plane to the vector of interest. Elevation angles range from $$-\pi/2$$ to $$\pi/2$$.

{% include image.html file="geometry.png" caption="The 3-D geometry convention used by MFM." %}

Explicitly, the 3-D geometry can be summarized as follows, where $$r$$ is the radial distance, $$\theta$$ is the azimuth angle, and $$\phi$$ is the elevation angle.

A vector of length $$r$$ in the azimuth-elevation direction $$(\theta,\phi)$$ can be converted to Cartesian components $$(x,y,z)$$ as

$$
\begin{align}
x &= r \sin \theta \cos \phi \\
y &= r \cos \theta \cos \phi \\
z &= r \sin \phi
\end{align}
$$

A vector having Cartesian components $$(x,y,z)$$ can be described as having length $$r$$ in the azimuth-elevation direction $$(\theta,\phi)$$ via

$$
\begin{align}
r &= \sqrt{x^2 + y^2 + z^2} \\
\theta &= \arctan \left( \frac{x}{y} \right) \\
\phi &= \arctan \left( \frac{z}{\sqrt{x^2 + y^2}} \right)
\end{align}
$$

Uniform linear arrays are created along the x-axis by default, meaning that an azimuth of 0 degrees is directly in from the array (broadside).

{% include image.html file="obj/array/array_show_3d_ula_8.svg" caption="An 8-element uniform linear array along the x-axis created with MFM." %}


Uniform planar arrays are created in the x-z plane by default, meaning that an azimuth of 0 degrees and elevation of 0 degrees is broadside.

{% include image.html file="obj/array/array_show_3d_upa_8_8.svg" caption="An 8-by-8 uniform planar array in the x-z plane created with MFM." %}
