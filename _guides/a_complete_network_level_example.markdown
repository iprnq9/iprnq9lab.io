---
layout: guide
title:  "A Complete Network-Level Example"
summary: An example of how to use MFM at its network-level.
date: 2020-11-30
published: true
---

### Introduction

In this guide, we demonstrate MFM's network-level capability by considering a setting where each two links are present, each acting as interference onto the other.

The MFM script `main/examples/main_example_01.m` implements this example.

### Overview

Let us consider the following network scenario. We wish to model a network of four devices:
- device-1 placed at (0,0,0) meters
- device-2 placed at (100,0,0) meters
- device-3 placed at (0,100,0) meters
- device-4 placed at (100,100,0) meters

{% include image.html file="obj/network/network_show_3d_2_2.svg" caption="The network we will be simulating." %}

Device-1 transmits to device-2 and device-3 transmits to device-4.

Device-1 and device-2 are relatively close to one another (100 meters apart).

Device-3 and device-4 are also relatively close to one another (100 meters apart).

The two pairs are separated by 100 meters, meaning inter-user interference will likely be significant.

Suppose all devices are fully-digital transceivers, in general, even though they act only as a transmitter or receiver at a given time.

All devices operate using the same time-frequency resource, leading to interference from device-1 onto device-4 and from device-3 onto device-2.

All devices operate using 30 dBm of transmit power over a symbol bandwidth of 50 MHz at 5 GHz. The noise power at each receiver is -174 dBm/Hz.

Transmitters use 8 antennas and receivers use 4 antennas. Suppose 4 streams are being multiplexed by each device.

Let all channels be Rayleigh-faded and the path loss follow free-space path loss with an exponent of 2.


### Preliminaries

If not already done so, setup MFM by entering the MFM directory and executing

```
mfm.setup()
```

in the Command Window.

Now, let us open a new MATLAB script and save it as you'd like. 

At the top of the script, include the following to clear all variables.

```
clearvars;
```

### System Variables

Let us begin by defining our system variables.

```
symbol_bandwidth_Hz = 50e6;
carrier_frequency_Hz = 30e9;
propagation_velocity_meters_per_sec = 3e8;
noise_power_per_Hz_dBm_Hz = -174;
num_transmit_antennas = 8;
num_receive_antennas = 4;
num_streams = 4;
```

### Template Device

Since all devices are the same, we will make use of a "template device" that we will copy to create our four devices.

```
device_object = device.create('transceiver','digital');
```

Then, to propertly setup the device, let's exectute

```
device_object.set_transmit_power(transmit_power_dBm,'dBm');
device_object.set_arrays(array_transmit_object,array_receive_object);
```

### Creating the Channel Model

Having assumed Rayleigh-faded channels, let's create a channel object to use network-wide.

```
channel_object = channel.create('Rayleigh');
```

### Creating the Path Loss Model

We have assumed free-space path loss with a path loss exponent of 2. We can create a path loss object to capture this via

```
path_loss_object = path_loss.create('free-space');
path_loss_object.set_path_loss_exponent(2);
```

### Creating the Four Devices

Recall that all four devices in the network are of the same type. This allows us to leverage our template device when creating them.

We can create each of the four devices as follows, using the `copy_object` function, where each device has its coordinate set accordingly and its name and marker for convenience.

```matlab
dev_1 = copy_object(device_object);
dev_1.set_name('device-1');
dev_1.set_coordinate([0,0,0]);
dev_1.set_marker('bx');

dev_2 = copy_object(device_object);
dev_2.set_name('device-2');
dev_2.set_coordinate([0,100,0]);
dev_2.set_marker('bo');

dev_3 = copy_object(device_object);
dev_3.set_name('device-3');
dev_3.set_coordinate([100,0,0]);
dev_3.set_marker('mx');

dev_4 = copy_object(device_object);
dev_4.set_name('device-4');
dev_4.set_coordinate([100,100,0]);
dev_4.set_marker('mo');
```


### Creating the Network

To create an empty network, we use

```
net = network_mfm();
```


Then, to add our source-destination pairs, we can use

```
net.add_source_destination(dev_1,dev_2);
net.add_source_destination(dev_3,dev_4);
```

This informs the network that device 1 is communicating with device 2 and device 3 is communicating with device 4.


### Establishing the Network's Links

To automatically populate the links between transmitter-receiver pairs in the network, we can use

```
net.populate_links_from_source_destination();
```


### Setting Up the Network

To finish setting up the network, we can execute

```
net.set_path_loss(path_loss_object);
net.set_channel(channel_object);
```

which uses the previously created channel and path loss objects, along with

```
net.set_symbol_bandwidth(symbol_bandwidth_Hz);
net.set_propagation_velocity(propagation_velocity_meters_per_sec);
net.set_carrier_frequency(carrier_frequency_Hz);
net.set_num_streams(num_streams);
net.set_transmit_power(transmit_power_dBm,'dBm');
net.set_noise_power_per_Hz(noise_power_per_Hz_dBm_Hz,'dBm_Hz');
```

which uses our previously defined system variables.

This concludes the necessary network setup, meaning we are ready for a network realization.


### Viewing the Network

To visualize the network that has been created, use

```
net.show_3d();
```

which will produce a plot of the form

{% include image.html file="obj/network/network_show_3d_2_2.svg" %}


### Invoking a Network Realization

To invoke a complete network realization, simply use

```
net.realization();
```


### Handling Channel State Information

To compute channel state information across the entire network, use

```
net.compute_channel_state_information();
```

Then, to supply this channel state information to each device in the network, use

```
net.supply_channel_state_information();
```


### Configuring Transmitters and Receivers

To configure all transmitting devices in the network to use eigen-beamforming, we use

```
net.configure_transmitter('eigen');
```

To configure all receiving devices in the network to use MMSE-based combiners, we use

```
net.configure_receiver('mmse');
```

Both of these will automatically use the channel state information that is available at each device.


### Computing the Received Signals

With a realized network and configured devices, we can compute the received signals at each device using

```
net.compute_received_signals();
```


### Reporting the Mutual Information

To report the mutual information achieved between devices (in bps/Hz), we can use

```
mi_12 = net.report_mutual_information(dev_1,dev_2)
mi_34 = net.report_mutual_information(dev_3,dev_4)
```

where `mi_12` and `mi_34` are the mutual information values achieved from device 1 to device 2 and from device 3 to device 4, respectively.


### Reporting the Symbol Estimation Error

To report the symbol estimation error achieved, we can use

```
[err_12,nerr_12] = net.report_symbol_estimation_error(dev_1,dev_2)
[err_34,nerr_34] = net.report_symbol_estimation_error(dev_3,dev_4)
```

where `err_12` and `err_34` are the estimation errors associated with the received symbols versus the transmitted symbols between devices 1 and 2 and devices 3 and 4, respectively; `nerr_12` and `nerr_34` are the corresponding normalized estimation errors.


### That's It!

That concludes this basic run-through of a network-level example using MFM. 

To play around with this example further, try moving the devices closer together to see how increased interference impacts the mutual information and symbol estimation error.
