---
layout: guide
title:  "Capturing Antenna Array Imperfections"
summary: How MFM can capture phase, amplitude, and placement errors across array elements.
date: 2021-01-13
published: true
---


### Overview

While antenna arrays in MFM are treated as having relatively ideal behavior, the `array` object can be used to capture mild forms of errors and imperfections. These imperfections can be captured via the array's geometry (the placement of its elements) and optional weights applied to each element.

{% include image.html file="obj/array/array_with_weights.svg" %}



### Capturing Phase and Amplitude Inconsistency Across Elements/Feeds

A relevant issue in array technology is the possibility for phase and/or amplitude inconsistencies across antenna elements and/or their feeds. Often times, calibration is used to combat this. No calibration is perfect nor is necessarily long-term, leading to residual errors.

To capture phase and/or amplitude inconsistency across an array's elements/feeds, one can use the array weights. 

For example, to introduce a phase error $$\phi_i$$ and amplitude error $$a_i$$ at the $$i$$-th element/feed of the array, one can set the $$i$$-th weight to 

$$
w_i = a_i \cdot {e}^{\mathrm{j} \phi_i}
$$

A vector of weights can be set at an array `a` using the familiar

```
a.set_weights(w)
```

where `w` is the vector of weights.

If using the array weights along with this phase/amplitude inconsistency, however, the user must ensure both are reflected by the array weights, since MFM does not differentiate between the two.

To capture phase/amplitude error in the array response, be sure to use the `array.get_weighted_array_response` function (rather than `array.get_array_response`) via

```
v = a.get_weighted_array_response(az,el)
```

where `a` is an array object, `az` and `el` are the azimuth and elevation directions of interest, and `v` is the effective array response.

{% include image.html file="obj/array/array_weighted_response.svg" %}


### Capturing Errors in Element Placement

Imperfect placement of array elements---potentially due to manufacturing tolerances or implementation errors---can introduce significant distortions to the array response if severe enough. Therefore, it may be of interest to researchers to simulate the effects these errors have on their system.

To capture introduce randomness onto the placement of the array elements, it's as simple as adding noise to each element's (x,y,z) coordinate. Don't forget, however, that the array geometry is in units of **carrier wavelengths**.

For example, to add Gaussian distributed noise to the x coordinate of a uniform linear array, we can do the following.

Let's begin by creating the array.

```
a = array.create(8) % creates a ULA
```

Inspecting the x coordinates, we can see that the ideal values are as follows.

```
>> a.x

ans =

  -1.7500   -1.2500   -0.7500   -0.2500    0.2500    0.7500    1.2500    1.7500
```

To generate a vector of Gaussian distributed error terms for each of these 8 elements' x coordinates, we can use

```
x_error = gauss_rv(0,0.01,1,8) % mean, variance, num rows, num cols
```

where we have used for this example a variance of `0.01`. For this realization, we have the following error term.

```
>> x_error

x_error =

  -0.0915   -0.0747    0.0859   -0.2691    0.0234   -0.0155   -0.0783    0.0555
```

This error can then be applied to the ideal x coordinates via

```
a.x = a.x + x_error
```

Inspecting the x coordinates with the error, we can see that, for this realization, we have

```
>> a.x

ans =

  -1.8415   -1.3247   -0.6641   -0.5191    0.2734    0.7345    1.1717    1.8055
```


