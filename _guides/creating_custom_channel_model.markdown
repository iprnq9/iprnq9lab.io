---
layout: guide
title:  "Creating a Custom Channel Model"
summary: How to add your own custom channel model to use with MFM.
date: 2020-11-30
---

### Introduction

MFM comes with a few common channel models including the Rayleigh-faded channel, ray/cluster channel, and spherical-wave channel.

Adding your own channel to MFM is relatively straightforward, though a few rules must be followed to ensure it integrates with the rest of MFM.


### Naming Convention

Custom channel objects should be named with the prefix `channel_` and saved in the `mfm/obj/channel/` directory beside the other channel definitions.


### Subclass of the Generic Channel

A custom channel `channel_custom` should always be defined as a subclass of the `channel` object as follows

```
classdef channel_custom < channel
    ...
end
```



### Channel Realization

A custom channel `channel_custom` should have its own definition of `realization()` that is of the form.

```
function H = realization(obj)
    H = ... % realization definition
    obj.set_channel_matrix(H);
    H = obj.get_channel_matrix();
end
```

The `realization` function should take only one input argument (the object itself) and return the realized channel matrix `H`.

Within the `realization` function, the object should call `set_channel_matrix(H)`.


### Class Boilerplate

Below is a boilerplate class definition for creating a custom channel object in MFM.

```
classdef channel_custom < channel
    properties
        ... % extra properties beyond those inherited from the channel object
    end
    methods
        function obj = channel_custom(...)
            ... % constructor definition
        end
        
        ... % extra methods beyond those inherited from the channel object
        
        function H = realization(obj)
            H = ... % realization definition
            obj.set_channel_matrix(H);
            H = obj.get_channel_matrix();
        end
    end
end
```

Any properties or methods beyond those inherited from the `channel` object can be added to the class definition. Methods can be created to overwrite those inherited from the `channel` object, though this is discouraged in general.


### Extending the Create Function

Channel objects in MFM are often created via 

```
c = channel.create(type)
```

where `type` is a string specifying the channel to create (e.g., `'Rayleigh'`, '`ray-cluster'`, `'spherical-wave'`).

To extend this `channel.create` function to include a custom channel `channel_custom`, do the following.

Open the `channel` class definition in `mfm/obj/channel/channel.m`.

At the top of the class definition is the static function `create()`.

In its function definition, simply tack on the following `elseif` statement with whatever unique string you'd like to use for the custom channel model.

```matlab
elseif strcmpi(type,'my-custom-string')
    c = channel_custom();
```

Alternatively, you can create a `channel_custom` object without adding it to this `create` method by

```matlab
c = channel_custom()
```

### Setting Transmit and Receive Arrays

A custom channel object `c` should always use the functions

```matlab
c.set_transmit_array(atx)
c.set_receive_array(arx)
```

where `atx` and `arx` are `array` objects. These methods are already defined in the `channel` object and should be used anytime the custom channel is used.


### Using Your Custom Channel

To use your custom channel, simply execute the typical setup associated with a `channel` object, such as

```
c = channel.create('my-custom-channel')
c.set_transmit_array(atx)
c.set_receive_array(arx)
c.set_propagation_velocity(vel)
c.set_carrier_frequency(fc)
```

along with whatever other setup your custom channel requires.

Then, a realization of your custom channel can be used via

```
H = c.realization()
```

or in any other fashion that `channel` objects are used in MFM.
