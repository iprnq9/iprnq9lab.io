---
layout: guide
title:  "Units of Measure in MFM"
summary: A summary of the units of measure used throughout MFM.
date: 2021-01-13
published: true
---

Measures of length are always in **meters** except when describing antenna array elements. 

With antenna arrays, units of length are in **carrier wavelengths** since an array's behavior and characterization is its geometry relative to carrier wavelength. This convenience makes the antenna arrays in MFM agnostic to carrier wavelength---evidenced by the fact that antenna arrays in MFM are not informed of the carrier wavelength.

{% include image.html file="obj/array/array_show_3d_ula_8.svg" caption="An 8-element uniform linear array has elements placed in 3-D space relative to the carrier wavelength." %}

Measures of frequency and bandwidth are always in units **Hertz**.

Measures of time are always in units **seconds**.

Velocities are always in **meters/second**.

Azimuth and elevation angles are always in **radians** except when plotting (e.g., array patterns) since degrees are often easier to interpret.

Power quantities are always in **Watts** (except when specifying units otherwise).

Power spectral densities are always in **Watts/Hz** (except when specifying units otherwise).

**Linear** units are used throughout MFM rather than log-based units (e.g., dB) except on occasion when plotting.

Some gain values are in **amplitude gain** (e.g., a `link` object's large-scale gain) while other gain values are in **linear power loss** (e.g., a `path_loss` object's attenuation). Please refer to the documentation for clarification on a particular value.



