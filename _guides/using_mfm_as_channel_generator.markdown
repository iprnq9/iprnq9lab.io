---
layout: guide
title:  "Using MFM as a Channel Generator"
summary: How to generate channel realizations using MFM.
date: 2020-11-30
---

### Introduction

While MFM comes with higher-level objects like the `device`, `link`, and `network_mfm` objects, it can also be used in a pretty low-level sense as a channel generator.

In this guide, we outline how to use MFM's `channel` objects to easily realize channels, which can be particularly useful in simulations.


### Creating the Channel

Suppose we wish to create a channel for simulation.

```
c = channel.create(type)
```

where `type` is the string identifier for the channel we wish to create.


### Setting up the Channel

Once created, we need to perform an initial setup of the channel before beginning to use it.

Let us begin by setting the carrier frequency to `fc` Hz and the propagation velocity to `vel` m/s.

```matlab
c.set_carrier_frequency(fc)
c.set_propagation_velocity(vel)
```

Then, suppose we have $$N_{\mathrm{t}}$$ transmit antennas and $$N_{\mathrm{r}}$$ receive antennas. Then we can create two arrays via

```matlab
atx = array(Nt)
arx = array(Nr)
```

Note that in cases where the actual array geometry is immaterial, creating uniform linear arrays can be used without loss of generality.

Then, to set the arrays of `c`, we can use

```matlab
c.set_transmit_array(atx)
c.set_receive_array(arx)
```

Any remaining setup can be executed before proceeding.


### Generating Channels

Now that the channel `c` has been created, we can use it to generate channel realizations.

To create a single channel realization, we can execute

```matlab
H = c.realization()
```

where `H` is a $$N_{\mathrm{r}} \times N_{\mathrm{t}}$$ channel matrix.

Executing this in a `for`-loop exemplifies the conveience of MFM's channel objects. Generating a channel realization is a single line.

```matlab
...
atx = array(Nt);
arx = array(Nr);
...
c = channel.create(type);
c.set_carrier_frequency(fc);
c.set_propagation_velocity(vel);
c.set_transmit_array(atx);
c.set_receive_array(arx);
...
for i = 1:N
    ...
    H = c.realization();
    ...
end
```


