The `array` object contains the following properties:
 - `array.num_antennas` 
 - `array.x` 
 - `array.y` 
 - `array.z` 
 - `array.weights` 
 - `array.marker` 
