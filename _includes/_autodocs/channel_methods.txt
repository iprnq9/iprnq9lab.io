#### **`H()`** {#H}
> Returns the channel matrix.

Usage:
: `val = H()`



_Return Values_:
: `val` --- the channel matrix



[Back to methods](#methods)



#### **`Nr()`** {#Nr}
> Returns the number of receive antennas out of the channel.

Usage:
: `val = Nr()`



_Return Values_:
: `val` --- the number of receive antennas out of the channel



[Back to methods](#methods)



#### **`Nt()`** {#Nt}
> Returns the number of transmit antennas into the channel.

Usage:
: `val = Nt()`



_Return Values_:
: `val` --- the number of transmit antennas into the channel



[Back to methods](#methods)



#### **`channel(name)`** {#channel}
> Creates a MIMO channel object.

Usage:
: `obj = channel()`
: `obj = channel(name)`



_Input Arguments_:
: `name` --- an optional name for the object

_Return Values_:
: `obj` --- an object representing a MIMO channel



[Back to methods](#methods)



#### **`create(type)`** {#create}
> Creates a channel object of a specific type.

Usage:
: `c = channel.create()`
: `c = channel.create(type)`



_Input Arguments_:
: `type` --- (optional) a string specifying what type of channel to create

_Return Values_:
: `c` --- a channel object of the type specified



[Back to methods](#methods)



#### **`enforce_channel_energy_normalization(H)`** {#enforce_channel_energy_normalization}
> Normalizes the channel matrix so that its total energy (squared Frobenius norm) is equal the current normalized channel energy property. The default normalized channel energy is the product of the number of transmit antenans and the number of receive antennas.

Usage:
: `G = enforce_channel_energy_normalization()`
: `G = enforce_channel_energy_normalization(H)`



_Input Arguments_:
: `H` --- (optional) a channel matrix; if not passed, the current channel matrix will be used and overwritten with the  normalized version; if passed, the channel matrix property  will not be set

_Return Values_:
: `G` --- the normalized channel matrix



[Back to methods](#methods)



#### **`get_channel_matrix()`** {#get_channel_matrix}
> Returns the channel matrix.

Usage:
: `H = get_channel_matrix()`



_Return Values_:
: `H` --- channel matrix



[Back to methods](#methods)



#### **`initialize()`** {#initialize}
> Initializes a channel.

Usage:
: `initialize()`





[Back to methods](#methods)



#### **`set_array_receive(array)`** {#set_array_receive}
> Sets the receive array object. Also sets the number of receive antennas accordingly.

Usage:
: `set_array_receive(array)`



_Input Arguments_:
: `array` --- an array object



[Back to methods](#methods)



#### **`set_array_transmit(array)`** {#set_array_transmit}
> Sets the transmit array object. Also sets the number of transmit antennas accordingly.

Usage:
: `set_array_transmit(array)`



_Input Arguments_:
: `array` --- an array object



[Back to methods](#methods)



#### **`set_arrays(array_transmit,array_receive)`** {#set_arrays}
> Sets the transmit and receive arrays at the input and output of the channel.

Usage:
: `set_arrays(array_transmit,array_receive)`



_Input Arguments_:
: `array_transmit` --- an array object at the channel input
: `array_receive` --- an array object at the channel output



[Back to methods](#methods)



#### **`set_carrier_frequency(fc)`** {#set_carrier_frequency}
> Sets the carrier frequency of the channel.

Usage:
: `set_carrier_frequency(fc)`



_Input Arguments_:
: `fc` --- carrier frequency (Hz)

_Notes_:
: Also updates carrier wavelength.



[Back to methods](#methods)



#### **`set_channel_matrix(H)`** {#set_channel_matrix}
> Sets the channel matrix.

Usage:
: `set_channel_matrix(H)`



_Input Arguments_:
: `H` --- channel matrix



[Back to methods](#methods)



#### **`set_force_channel_energy_normalization(force)`** {#set_force_channel_energy_normalization}
> Sets the enforcement of channel energy normalization. If true, the channel matrix will always be normalized such that its energy is of the desired value.

Usage:
: `set_force_channel_energy_normalization(force)`



_Input Arguments_:
: `force` --- a boolean indicating if the channel matrix should be normalized or not



[Back to methods](#methods)



#### **`set_name(name)`** {#set_name}
> Sets the name of the channel.

Usage:
: `set_name()`
: `set_name(name)`



_Input Arguments_:
: `name` --- (optional) a string; if not passed, 'channel' is the default name used



[Back to methods](#methods)



#### **`set_normalized_channel_energy(E)`** {#set_normalized_channel_energy}
> Sets the normalized energy of the channel.

Usage:
: `set_normalized_channel_energy()`
: `set_normalized_channel_energy(E)`



_Input Arguments_:
: `E` --- (optional) the desired normalized channel energy; if not passed, the product of the number of transmit antennas and the number of receive antennas will be used



[Back to methods](#methods)



#### **`set_propagation_velocity(val)`** {#set_propagation_velocity}
> Sets the propagation velocity of the channel.

Usage:
: `set_propagation_velocity(val)`



_Input Arguments_:
: `val` --- propagation velocity (meters/sec)



[Back to methods](#methods)



#### **`set_receive_array(array)`** {#set_receive_array}
> Sets the receive array object (LEGACY).

Usage:
: `set_receive_array(array)`



_Input Arguments_:
: `array` --- an array object



[Back to methods](#methods)



#### **`set_transmit_array(array)`** {#set_transmit_array}
> Sets the transmit array object (LEGACY).

Usage:
: `set_transmit_array(array)`



_Input Arguments_:
: `array` --- an array object



[Back to methods](#methods)



