The `channel_ray_cluster` object contains the following properties:
 - `channel_ray_cluster.num_clusters` 
 - `channel_ray_cluster.num_clusters_range` 
 - `channel_ray_cluster.num_rays_per_cluster` 
 - `channel_ray_cluster.num_rays_per_cluster_range` 
 - `channel_ray_cluster.cluster_AoD` 
 - `channel_ray_cluster.cluster_AoA` 
 - `channel_ray_cluster.AoD` 
 - `channel_ray_cluster.AoA` 
 - `channel_ray_cluster.AoD_range` 
 - `channel_ray_cluster.AoA_range` 
 - `channel_ray_cluster.ray_angle_distribution` 
 - `channel_ray_cluster.ray_gain_distribution` 
 - `channel_ray_cluster.ray_gains` 
 - `channel_ray_cluster.snap_AoA` 
 - `channel_ray_cluster.snap_AoD` 
 - `channel_ray_cluster.name` 
 - `channel_ray_cluster.channel_matrix` 
 - `channel_ray_cluster.num_antennas_transmit` 
 - `channel_ray_cluster.num_antennas_receive` 
 - `channel_ray_cluster.array_transmit` 
 - `channel_ray_cluster.array_receive` 
 - `channel_ray_cluster.carrier_frequency` 
 - `channel_ray_cluster.carrier_wavelength` 
 - `channel_ray_cluster.propagation_velocity` 
 - `channel_ray_cluster.normalized_channel_energy` 
 - `channel_ray_cluster.force_channel_energy_normalization` 
