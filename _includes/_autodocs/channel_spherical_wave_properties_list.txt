The `channel_spherical_wave` object contains the following properties:
 - `channel_spherical_wave.large_scale_gain` 
 - `channel_spherical_wave.name` 
 - `channel_spherical_wave.channel_matrix` 
 - `channel_spherical_wave.num_antennas_transmit` 
 - `channel_spherical_wave.num_antennas_receive` 
 - `channel_spherical_wave.array_transmit` 
 - `channel_spherical_wave.array_receive` 
 - `channel_spherical_wave.carrier_frequency` 
 - `channel_spherical_wave.carrier_wavelength` 
 - `channel_spherical_wave.propagation_velocity` 
 - `channel_spherical_wave.normalized_channel_energy` 
 - `channel_spherical_wave.force_channel_energy_normalization` 
