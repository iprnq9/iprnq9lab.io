#### **`F()`** {#F}
> Returns the precoding matrix.

Usage:
: `val = F()`



_Return Values_:
: `val` --- the precoding matrix



[Back to methods](#methods)



#### **`N0()`** {#N0}
> Returns the noise energy per symbol of the receiver.

Usage:
: `val = N0()`



_Return Values_:
: `val` --- the noise energy per symbol



[Back to methods](#methods)



#### **`Nr()`** {#Nr}
> Returns the number of receive antennas.

Usage:
: `val = Nr()`



_Return Values_:
: `val` --- the number of receive antennas



[Back to methods](#methods)



#### **`Ns_rx()`** {#Ns_rx}
> Returns the number of streams at the receiver.

Usage:
: `val = Ns_rx()`



_Return Values_:
: `val` --- the number of streams at the receiver



[Back to methods](#methods)



#### **`Ns_tx()`** {#Ns_tx}
> Returns the number of streams at the transmitter.

Usage:
: `val = Ns_tx()`



_Return Values_:
: `val` --- the number of streams at the transmitter



[Back to methods](#methods)



#### **`Nt()`** {#Nt}
> Returns the number of transmit antennas.

Usage:
: `val = Nt()`



_Return Values_:
: `val` --- the number of transmit antennas



[Back to methods](#methods)



#### **`Rn()`** {#Rn}
> Returns the noise covariance matrix at the receiver.

Usage:
: `val = Rn()`



_Return Values_:
: `val` --- the noise covariance matrix



[Back to methods](#methods)



#### **`Rs()`** {#Rs}
> Returns the transmit symbol covariance matrix at the transmitter.

Usage:
: `val = Rs()`



_Return Values_:
: `val` --- the transmit symbol covariance matrix at the transmitter



[Back to methods](#methods)



#### **`W()`** {#W}
> Returns the combining matrix.

Usage:
: `val = W()`



_Return Values_:
: `val` --- the combining matrix



[Back to methods](#methods)



#### **`configure_receiver(strategy)`** {#configure_receiver}
> Configures the device's receiver according to the current/specified receive strategy, incorporating channel state information as applicable.

Usage:
: `configure_receiver()`
: `configure_receiver(strategy)`



_Input Arguments_:
: `strategy` --- (optional) receive strategy to use; if not passed, the current receive strategy will be used



[Back to methods](#methods)



#### **`configure_transmitter(strategy)`** {#configure_transmitter}
> Configures the device's transmitter according to the current/specified transmit strategy, incorporating channel state information as applicable.

Usage:
: `configure_transmitter()`
: `configure_transmitter(strategy)`



_Input Arguments_:
: `strategy` --- (optional) transmit strategy to use; if not passed, the current transmit strategy will be used



[Back to methods](#methods)



#### **`create(type,digital_hybrid)`** {#create}
> Creates a device of specific type.

Usage:
: `obj = create()`
: `obj = create(type)`
: `obj = create(type,digital_hybrid)`
: `obj = create([],digital_hybrid)`



_Input Arguments_:
: `type` --- (optional) 'transmitter', 'receiver', or 'transceiver'
: `digital_hybrid` --- (optional) 'digital' or 'hybrid'

_Return Values_:
: `obj` --- a device object of the desired type



[Back to methods](#methods)



#### **`device(name)`** {#device}
> Creates a device object.

Usage:
: `obj = device()`
: `obj = device(name)`



_Input Arguments_:
: `name` --- an optional name for the device

_Return Values_:
: `obj` --- an object representing a device



[Back to methods](#methods)



#### **`isreceiver()`** {#isreceiver}
> Returns a boolean indicating if the device has receive capability (i.e., is either a receiver or transceiver type).

Usage:
: `out = isreceiver()`



_Return Values_:
: `out` --- a boolean



[Back to methods](#methods)



#### **`istransceiver()`** {#istransceiver}
> Returns a boolean indicating if the device has transmit and receive capability (i.e., is a transceiver).

Usage:
: `out = istransceiver()`



_Return Values_:
: `out` --- a boolean



[Back to methods](#methods)



#### **`istransmitter()`** {#istransmitter}
> Returns a boolean indicating if the device has transmit capability (i.e., is either a transmitter or transceiver type).

Usage:
: `out = istransmitter()`



_Return Values_:
: `out` --- a boolean



[Back to methods](#methods)



#### **`n()`** {#n}
> Returns the noise vector (per-antenna).

Usage:
: `val = n()`



_Return Values_:
: `val` --- the noise vector (per-antenna)



[Back to methods](#methods)



#### **`s_rx()`** {#s_rx}
> Returns the receive symbol vector.

Usage:
: `val = s_rx()`



_Return Values_:
: `val` --- the receive symbol vector



[Back to methods](#methods)



#### **`s_tx()`** {#s_tx}
> Returns the transmit symbol vector.

Usage:
: `val = s_tx()`



_Return Values_:
: `val` --- the transmit symbol vector



[Back to methods](#methods)



#### **`set_arrays(transmit_array,receive_array)`** {#set_arrays}
> Sets the transmit and receive arrays.

Usage:
: `set_arrays(transmit_array)`
: `set_arrays(transmit_array,receive_array)`



_Input Arguments_:
: `transmit_array` --- array object to use at the transmitter
: `receive_array` --- array object to use at the receiver; if not  passed, the transmit array is used



[Back to methods](#methods)



#### **`set_carrier_frequency(fc)`** {#set_carrier_frequency}
> Sets the carrier frequency of the device.

Usage:
: `set_carrier_frequency(fc)`



_Input Arguments_:
: `fc` --- carrier frequency (Hz)

_Notes_:
: Also updates carrier wavelength.



[Back to methods](#methods)



#### **`set_combiner(W)`** {#set_combiner}
> Sets the combining matrix used at the receiver.

Usage:
: `set_combiner(W)`



_Input Arguments_:
: `W` --- a combining matrix



[Back to methods](#methods)



#### **`set_coordinate(x,y,z,move_arrays)`** {#set_coordinate}
> Sets the device's position in 3-D space.

Usage:
: `set_coordinate()`
: `set_coordinate(x)`
: `set_coordinate(x,y)`
: `set_coordinate(x,y,z)`
: `set_coordinate(x,y,z,move_arrays)`



_Input Arguments_:
: `x` --- x-coordinate of the device's position
: `y` --- y-coordinate of the device's position
: `z` --- z-coordinate of the device's position
: `move_arrays` --- (optional) a boolean indicating if the array(s) should be moved (relatively) to the new coordinate; if not  passed, true is used

_Notes_:
: If no arguments are passed, the device's current coordinate will be used.
: If only x and y are given, z is taken to be 0. Both x and y need to be singletons.
: If only x is given and is a singleton, y and z are taken to be 0. If only x is given and has two elements, y = x(2), x = x(1), and z = 0. If only x is given and has three elements, z = x(3), y = x(2), and x = x(1).
: All other options are invalid.
: When the arrays are shifted according to the device coordinate, they are shifted relative to the device's previous location. This maintains any relative geometry between the transmit and receive arrays, assuming they have already been added to the device before its coordinate is set.



[Back to methods](#methods)



#### **`set_destination(device_destination)`** {#set_destination}
> Sets the destination (the receive device the device aims to serve).

Usage:
: `set_destination(device_destination)`



_Input Arguments_:
: `device_destination` --- a device object



[Back to methods](#methods)



#### **`set_duplexing(duplex)`** {#set_duplexing}
> Sets the device as either a half-duplex or full-duplex device.

Usage:
: `set_duplexing(duplex)`



_Input Arguments_:
: `duplex` --- either 'half' or 'full', specifying the duplexing nature of the device

_Notes_:
: Setting the device to be full-duplex does not bestow it the ability to transmit and receive simultaneously and in-band losslessly, but rather sets it to attempt to. Its performance when doing so will depend on the design of the  transmitter and receiver.



[Back to methods](#methods)



#### **`set_marker(marker)`** {#set_marker}
> Sets the device marker.

Usage:
: `set_marker(marker)`



_Input Arguments_:
: `marker` --- a MATLAB-compliant plot marker



[Back to methods](#methods)



#### **`set_name()`** {#set_name}
> Sets the device's name.

Usage:
: `set_name()`
: `set_name(name)`





[Back to methods](#methods)



#### **`set_noise(n)`** {#set_noise}
> Sets the noise vector at the receive array.

Usage:
: `set_noise()`
: `set_noise(n)`



_Input Arguments_:
: `n` --- (optional) a noise vector; if not passed, noise will be generated according to the noise power



[Back to methods](#methods)



#### **`set_noise_power_per_Hz(noise_psd,unit)`** {#set_noise_power_per_Hz}
> Sets the noise power per Hz at the receiver.

Usage:
: `set_noise_power_per_Hz(noise_psd)`
: `set_noise_power_per_Hz(noise_psd,unit)`



_Input Arguments_:
: `noise_psd` --- the noise power spectral density
: `unit` --- (optional) a string specifying the units of noise_psd  (e.g., 'dBm_Hz' or 'watts_Hz'); if not passed, the default will be used



[Back to methods](#methods)



#### **`set_num_rf_chains(Lt,Lr)`** {#set_num_rf_chains}
> Sets the number of transmit and receive RF chains at the device.

Usage:
: `set_num_rf_chains(Lt,Lr)`



_Input Arguments_:
: `Lt` --- number of transmit RF chains
: `Lr` --- (optional) number of receive RF chains; if not passed, the number of transmit RF chains is used



[Back to methods](#methods)



#### **`set_num_streams(num_streams)`** {#set_num_streams}
> Sets the number of streams at the transmitter and receiver.

Usage:
: `set_num_streams(num_streams)`



_Input Arguments_:
: `num_streams` --- number of streams to multiplex at the transmitter and receiver



[Back to methods](#methods)



#### **`set_precoder(F)`** {#set_precoder}
> Sets the precoding matrix used at the transmitter.

Usage:
: `set_precoder(F)`



_Input Arguments_:
: `F` --- a precoding matrix



[Back to methods](#methods)



#### **`set_propagation_velocity(val)`** {#set_propagation_velocity}
> Sets the propagation velocity of the signals the device.

Usage:
: `set_propagation_velocity(val)`



_Input Arguments_:
: `val` --- propagation velocity (meters/sec)



[Back to methods](#methods)



#### **`set_receive_array(array)`** {#set_receive_array}
> Sets the receive array.

Usage:
: `set_receive_array(array)`



_Input Arguments_:
: `array` --- array object to use at the receiver



[Back to methods](#methods)



#### **`set_receive_channel_state_information(csi)`** {#set_receive_channel_state_information}
> Sets the channel state information at the receiver.

Usage:
: `set_receive_channel_state_information(csi)`



_Input Arguments_:
: `csi` --- a struct of channel state information



[Back to methods](#methods)



#### **`set_receive_num_rf_chains(Lr)`** {#set_receive_num_rf_chains}
> Sets the number of receive RF chains at the device.

Usage:
: `set_receive_num_rf_chains(Lr)`



_Input Arguments_:
: `Lr` --- number of receive RF chains



[Back to methods](#methods)



#### **`set_receive_num_streams(Ns)`** {#set_receive_num_streams}
> Sets the number of streams at the receiver.

Usage:
: `SET_NUM_STREAMS(Ns)`



_Input Arguments_:
: `Ns` --- number of streams to multiplex at the receiver



[Back to methods](#methods)



#### **`set_receive_strategy(strategy)`** {#set_receive_strategy}
> Sets the receive strategy.

Usage:
: `set_receive_strategy(strategy)`



_Input Arguments_:
: `strategy` --- a string specifying the receive strategy



[Back to methods](#methods)



#### **`set_receive_symbol(s)`** {#set_receive_symbol}
> Sets the receive symbol vector.

Usage:
: `set_receive_symbol(s)`



_Input Arguments_:
: `s` --- receive symbol vector



[Back to methods](#methods)



#### **`set_receive_symbol_bandwidth(B)`** {#set_receive_symbol_bandwidth}
> Sets the symbol bandwidth at the receiver.

Usage:
: `set_receive_symbol_bandwidth(B)`



_Input Arguments_:
: `B` --- the symbol bandwidth (in Hertz)



[Back to methods](#methods)



#### **`set_received_signal(y)`** {#set_received_signal}
> Sets the received vector.

Usage:
: `set_received_signal(y)`



_Input Arguments_:
: `y` --- received signal vector



[Back to methods](#methods)



#### **`set_receiver(rx)`** {#set_receiver}
> Sets the receiver for the device.

Usage:
: `set_receiver(rx)`



_Input Arguments_:
: `rx` --- a receiver object



[Back to methods](#methods)



#### **`set_source(device_source)`** {#set_source}
> Sets the source (the transmit device the device aims to serve).

Usage:
: `set_source(device_source)`



_Input Arguments_:
: `device_source` --- a device object



[Back to methods](#methods)



#### **`set_symbol_bandwidth(B)`** {#set_symbol_bandwidth}
> Sets the symbol bandwidth at the transmitter and receiver.

Usage:
: `set_symbol_bandwidth(B)`



_Input Arguments_:
: `B` --- the symbol bandwidth (in Hertz)



[Back to methods](#methods)



#### **`set_transmit_array(array)`** {#set_transmit_array}
> Sets the transmit array.

Usage:
: `set_transmit_array(array)`



_Input Arguments_:
: `array` --- array object to use at the transmitter



[Back to methods](#methods)



#### **`set_transmit_channel_state_information(csi)`** {#set_transmit_channel_state_information}
> Sets the channel state information at the transmitter.

Usage:
: `set_transmit_channel_state_information(csi)`



_Input Arguments_:
: `csi` --- a struct of channel state information



[Back to methods](#methods)



#### **`set_transmit_num_rf_chains(Lt)`** {#set_transmit_num_rf_chains}
> Sets the number of transmit RF chains at the device.

Usage:
: `set_transmit_num_rf_chains(Lt)`



_Input Arguments_:
: `Lt` --- number of transmit RF chains



[Back to methods](#methods)



#### **`set_transmit_num_streams(Ns)`** {#set_transmit_num_streams}
> Sets the number of streams at the transmitter.

Usage:
: `SET_NUM_STREAMS(Ns)`



_Input Arguments_:
: `Ns` --- number of streams to multiplex at the transmitter



[Back to methods](#methods)



#### **`set_transmit_power(P,unit)`** {#set_transmit_power}
> Sets the transmit power of the device.

Usage:
: `set_transmit_power(P)`
: `set_transmit_power(P,unit)`



_Input Arguments_:
: `P` --- transmit power (e.g., in watts or dBm)
: `unit` --- (optional) a string specifying the units of P (e.g.,  'dBm'); if not passed, the default is used.



[Back to methods](#methods)



#### **`set_transmit_strategy(strategy)`** {#set_transmit_strategy}
> Sets the transmit strategy.

Usage:
: `set_transmit_strategy(strategy)`



_Input Arguments_:
: `strategy` --- a string specifying the transmit strategy



[Back to methods](#methods)



#### **`set_transmit_symbol(s)`** {#set_transmit_symbol}
> Sets the transmit symbol vector.

Usage:
: `set_transmit_symbol(s)`



_Input Arguments_:
: `s` --- transmit symbol vector



[Back to methods](#methods)



#### **`set_transmit_symbol_bandwidth(B)`** {#set_transmit_symbol_bandwidth}
> Sets the symbol bandwidth at the transmitter.

Usage:
: `set_transmit_symbol_bandwidth(B)`



_Input Arguments_:
: `B` --- the symbol bandwidth (in Hertz)



[Back to methods](#methods)



#### **`set_transmit_symbol_covariance(Rs)`** {#set_transmit_symbol_covariance}
> Sets the transmit covariance of the device.

Usage:
: `set_transmit_symbol_covariance(Rs)`



_Input Arguments_:
: `Rs` --- a covariance matrix



[Back to methods](#methods)



#### **`set_transmitter(tx)`** {#set_transmitter}
> Sets the transmitter for the device.

Usage:
: `set_transmitter(tx)`



_Input Arguments_:
: `tx` --- a transmitter object



[Back to methods](#methods)



#### **`set_type(type,dh)`** {#set_type}
> Sets the device as either a fully-digital or hybrid digital/analog transmitter, receiver, or transceiver.

Usage:
: `set_type(type)`
: `set_type(type,dh)`



_Input Arguments_:
: `type` --- a string specifying the type of device; either 'transmitter', 'receiver', or 'transceiver' 
: `dh` --- a string specifying the digital or hybrid  digital/analog beamforming nature of the device; either  'digital' or 'hybrid'

_Notes_:
: Overwrites any existing transmitter or receiver that is present in the device.



[Back to methods](#methods)



#### **`show_transmit_receive_arrays_3d(fignum)`** {#show_transmit_receive_arrays_3d}
> Plots the transmit array and receive array in 3-D space.

Usage:
: `[fig,ax] = show_transmit_receive_arrays_3d()`
: `[fig,ax] = show_transmit_receive_arrays_3d(fignum)`



_Input Arguments_:
: `fignum` --- optional figure number to use when creating plotting

_Return Values_:
: `fig` --- a figure handle of the plot
: `ax` --- an axis handle of the plot



[Back to methods](#methods)



#### **`turn_off_receive()`** {#turn_off_receive}
> Turns off the device's receiver.

Usage:
: `turn_off_receive()`





[Back to methods](#methods)



#### **`turn_off_transmit()`** {#turn_off_transmit}
> Turns off the device's transmitter.

Usage:
: `turn_off_transmit()`





[Back to methods](#methods)



#### **`x()`** {#x}
> Returns the transmitted signal vector.

Usage:
: `val = x()`



_Return Values_:
: `val` --- the transmitted signal vector



[Back to methods](#methods)



#### **`y()`** {#y}
> Returns the received signal vector.

Usage:
: `val = y()`



_Return Values_:
: `val` --- the received signal vector



[Back to methods](#methods)



