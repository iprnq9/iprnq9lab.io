The `path_loss_free_space_log_normal_shadowing` object contains the following properties:
 - `path_loss_free_space_log_normal_shadowing.log_normal_shadowing_variance` 
 - `path_loss_free_space_log_normal_shadowing.path_loss_exponent` 
 - `path_loss_free_space_log_normal_shadowing.name` 
 - `path_loss_free_space_log_normal_shadowing.type` 
 - `path_loss_free_space_log_normal_shadowing.distance` 
 - `path_loss_free_space_log_normal_shadowing.attenuation` 
 - `path_loss_free_space_log_normal_shadowing.carrier_frequency` 
 - `path_loss_free_space_log_normal_shadowing.carrier_wavelength` 
 - `path_loss_free_space_log_normal_shadowing.propagation_velocity` 
