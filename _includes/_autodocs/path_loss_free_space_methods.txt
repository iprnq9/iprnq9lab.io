#### **`compute_path_loss_attenuation()`** {#compute_path_loss_attenuation}
> Computes the path attenuation (path loss) according to the free-space path loss (FSPL) formula.

Usage:
: `atten = compute_path_loss_attenuation()`



_Return Values_:
: `atten` --- the path attenuation (path loss) (a power loss)

_Notes_:
: The free-space path loss (FSPL) equation used is G = (lambda / (4*pi))^2 * (1/d)^ple, where G is the inverse path loss in terms of power.



[Back to methods](#methods)



#### **`create(type)`** {#create}
> Creates a path loss object of a specific type.

Usage:
: `obj = path_loss.create()`
: `obj = path_loss.create(type)`



_Input Arguments_:
: `type` --- (optional) a string specifying which path loss model to create

_Return Values_:
: `obj` --- a path loss object



[Back to methods](#methods)



#### **`get_attenuation()`** {#get_attenuation}
> Returns the realized attenuation of the path loss model.

Usage:
: `val = get_attenuation()`



_Return Values_:
: `val` --- the attenuation (power loss) of the path



[Back to methods](#methods)



#### **`initialize()`** {#initialize}
> Initializes a path loss object.

Usage:
: `initialize()`





[Back to methods](#methods)



#### **`initialize_free_space()`** {#initialize_free_space}
> Initializes a free-space path loss object.

Usage:
: `initialize_free_space()`





[Back to methods](#methods)



#### **`path_loss_free_space(name)`** {#path_loss_free_space}
> Creates a free-space path loss object.

Usage:
: `obj = path_loss_free_space()`
: `obj = path_loss_free_space(name)`



_Input Arguments_:
: `name` --- an optional name for the object

_Return Values_:
: `obj` --- an object representing free-space path loss



[Back to methods](#methods)



#### **`realization()`** {#realization}
> Invokes a realization of the path loss.

Usage:
: `atten = realization()`



_Return Values_:
: `atten` --- the realized path loss attenuation

_Notes_:
: Since no random variables are involved in the FSPL equation, it will be determininstic and thus fixed across realizations.



[Back to methods](#methods)



#### **`set_carrier_frequency(fc)`** {#set_carrier_frequency}
> Sets the carrier frequency of the channel. Also updates the carrier wavelength accordingly.

Usage:
: `set_carrier_frequency(fc)`



_Input Arguments_:
: `fc` --- carrier frequency (Hz)

_Notes_:
: Also updates carrier wavelength.



[Back to methods](#methods)



#### **`set_distance(d)`** {#set_distance}
> Sets the distance of the path (in meters).

Usage:
: `set_distance(d) Sets the distance of the link to a specific`
: `value.`



_Input Arguments_:
: `d` --- distance of the path (in meters)



[Back to methods](#methods)



#### **`set_name(name)`** {#set_name}
> Sets the name of the path loss model.

Usage:
: `set_name()`
: `set_name(name)`



_Input Arguments_:
: `name` --- (optional) a string; if not passed, 'path-loss' is the default name used



[Back to methods](#methods)



#### **`set_path_loss_attenuation(atten)`** {#set_path_loss_attenuation}
> Sets the attenuation of the path loss model.

Usage:
: `set_path_loss_attenuation(atten)`



_Input Arguments_:
: `atten` --- the attenuation (power loss) of the path



[Back to methods](#methods)



#### **`set_path_loss_exponent(ple)`** {#set_path_loss_exponent}
> Sets the path loss exponent.

Usage:
: `set_path_loss_exponent(ple)`



_Input Arguments_:
: `ple` --- path loss exponent (a positive number)



[Back to methods](#methods)



#### **`set_propagation_velocity(val)`** {#set_propagation_velocity}
> Sets the propagation velocity of the channel. Also updates the carrier wavelength accordingly.

Usage:
: `set_propagation_velocity(val)`



_Input Arguments_:
: `val` --- propagation velocity (meters/sec)



[Back to methods](#methods)



#### **`set_type(type)`** {#set_type}
> Sets the type of path loss model.

Usage:
: `set_type()`
: `set_type(type)`



_Input Arguments_:
: `type` --- (optional) a string; if not passed, 'default' is the default type used



[Back to methods](#methods)



