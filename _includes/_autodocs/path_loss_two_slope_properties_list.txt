The `path_loss_two_slope` object contains the following properties:
 - `path_loss_two_slope.reference_distance` 
 - `path_loss_two_slope.reference_path_loss` 
 - `path_loss_two_slope.path_loss_exponents` 
 - `path_loss_two_slope.name` 
 - `path_loss_two_slope.type` 
 - `path_loss_two_slope.distance` 
 - `path_loss_two_slope.attenuation` 
 - `path_loss_two_slope.carrier_frequency` 
 - `path_loss_two_slope.carrier_wavelength` 
 - `path_loss_two_slope.propagation_velocity` 
