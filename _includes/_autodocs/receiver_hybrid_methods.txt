#### **`N0()`** {#N0}
> Returns the noise energy per symbol.

Usage:
: `val = N0()`



_Return Values_:
: `val` --- the noise energy per symbol



[Back to methods](#methods)



#### **`Nr()`** {#Nr}
> Returns the number of receive antennas.

Usage:
: `val = Nr()`



_Return Values_:
: `val` --- the number of receive antennas



[Back to methods](#methods)



#### **`Ns()`** {#Ns}
> Returns the number of streams.

Usage:
: `val = Ns()`



_Return Values_:
: `val` --- the number of streams



[Back to methods](#methods)



#### **`Rn()`** {#Rn}
> Returns the noise covariance matrix.

Usage:
: `val = Rn()`



_Return Values_:
: `val` --- the noise covariance matrix



[Back to methods](#methods)



#### **`W()`** {#W}
> Returns the combining matrix.

Usage:
: `val = W()`



_Return Values_:
: `val` --- the combining matrix



[Back to methods](#methods)



#### **`check_combiner_analog_amplitude_constraint()`** {#check_combiner_analog_amplitude_constraint}
> Checks if the analog combiner has entries whose amplitudes have been quantized to the appropriate resolution.

Usage:
: `out = check_combiner_analog_amplitude_constraint()`



_Return Values_:
: `out` --- a boolean indicating whether or not the analog amplitude constraint has been met



[Back to methods](#methods)



#### **`check_combiner_analog_dimensions()`** {#check_combiner_analog_dimensions}
> Checks to see if the analog combining matrix has dimensions appropriate for the current number of RF chains and antennas.

Usage:
: `out = check_combiner_analog_dimensions()`



_Return Values_:
: `out` --- a boolean indicating if the analog combiner is of appropriate dimension



[Back to methods](#methods)



#### **`check_combiner_analog_phase_constraint()`** {#check_combiner_analog_phase_constraint}
> Checks if the analog combiner has entries whose phases have been quantized to the appropriate resolution.

Usage:
: `out = check_combiner_analog_phase_constraint()`



_Return Values_:
: `out` --- a boolean indicating whether or not the analog phase constraint has been met



[Back to methods](#methods)



#### **`check_combiner_digital_dimensions()`** {#check_combiner_digital_dimensions}
> Checks to see if the digital combining matrix has dimensions appropriate for the current number of RF chains and streams.

Usage:
: `out = check_combiner_digital_dimensions()`



_Return Values_:
: `out` --- a boolean indicating if the digital combiner is of appropriate dimension



[Back to methods](#methods)



#### **`check_combiner_dimensions()`** {#check_combiner_dimensions}
> Checks to see if the analog and digital combiners have compatible dimensions.

Usage:
: `out = check_combiner_dimensions()`



_Return Values_:
: `out` --- a boolean indicating if the analog and digital  combiners have compatible dimensions.



[Back to methods](#methods)



#### **`check_receive_strategy(strategy)`** {#check_receive_strategy}
> Returns a boolean indicating if a receive strategy is valid.

Usage:
: `out = check_receive_strategy(strategy)`



_Input Arguments_:
: `strategy` --- a string indentifying a specific receive strategy

_Return Values_:
: `out` --- a boolean



[Back to methods](#methods)



#### **`configure_receiver(strategy)`** {#configure_receiver}
> Configures the receiver's combiner according to the current/specified receive strategy, incorporating channel state information as applicable.

Usage:
: `configure_receiver()`
: `configure_receiver(strategy)`



_Input Arguments_:
: `strategy` --- (optional) receive strategy to use; if not passed, the current receive strategy will be used



[Back to methods](#methods)



#### **`configure_receiver_eigen()`** {#configure_receiver_eigen}
> Configures the receiver to receive along the strongest components of the desired channel, neglecting interference, etc.

Usage:
: `configure_receiver_eigen()`





[Back to methods](#methods)



#### **`configure_receiver_identity()`** {#configure_receiver_identity}
> Configures the receive combiner to an identity matrix.

Usage:
: `configure_receiver_identity()`





[Back to methods](#methods)



#### **`configure_receiver_mmse_desired()`** {#configure_receiver_mmse_desired}
> Configures the receiver in an LMMSE fashion to reject noise. Does not reject interference.

Usage:
: `configure_receiver_mmse_desired()`





[Back to methods](#methods)



#### **`configure_receiver_mmse_interference()`** {#configure_receiver_mmse_interference}
> Configures the receiver in an LMMSE fashion to reject noise and interference.

Usage:
: `configure_receiver_mmse_interference()`





[Back to methods](#methods)



#### **`create(type)`** {#create}
> Creates a receiver.

Usage:
: `obj = receiver.create()`
: `obj = receiver.create(type)`



_Input Arguments_:
: `type` --- (optional) a string specifying the type of receiver to create; either 'digital' or 'hybrid'

_Return Values_:
: `obj` --- a receiver object



[Back to methods](#methods)



#### **`enforce_combiner_power_budget()`** {#enforce_combiner_power_budget}
> Ensures that the digital combiner is normalized such that the total combining power budget is satisfied.

Usage:
: `enforce_combiner_power_budget()`



_Notes_:
: Supersedes the function with the same name in the parent.



[Back to methods](#methods)



#### **`get_receive_symbol()`** {#get_receive_symbol}
> Returns the receive symbol based on the curent received signal vector, noise, and combiner.

Usage:
: `s = get_receive_symbol()`



_Return Values_:
: `s` --- the receive symbol



[Back to methods](#methods)



#### **`get_source_channel_state_information_index()`** {#get_source_channel_state_information_index}
> Returns the index of the receive CSI entry whose transmit device corresponds to the receiver's source device.

Usage:
: `idx = get_source_channel_state_information_index()`



_Return Values_:
: `idx` --- an index; if zero, the CSI entry was not found



[Back to methods](#methods)



#### **`get_valid_receive_strategies()`** {#get_valid_receive_strategies}
> Returns a cell containing all valid receive strategy strings.

Usage:
: `out = get_valid_receive_strategies()`



_Return Values_:
: `out` --- a cell of strings

_Notes_:
: This function will differ between a fully-digital receiver and hybrid digital/analog receiver to account for receive strategies that are specific to each.
: This function will need to be updated anytime a custom/new receive strategy is added.
: The strings should be all lowercase.



[Back to methods](#methods)



#### **`hybrid_approximation(F,opt)`** {#hybrid_approximation}
> Performs hybrid approximation of a fully-digital beamforming matrix based on the hybrid approximation method and any associated options.

Usage:
: `[X,RF,BB,err] = hybrid_approximation(F,opt)`



_Input Arguments_:
: `F` --- fully-digital combining matrix
: `opt` --- struct of options specific to the hybrid approximation method

_Return Values_:
: `X` --- the effective fully-digital beamforming matrix
: `RF` --- the resulting analog beamforming matrix
: `BB` --- the resulting digital beamforming matrix
: `err` --- the squared Frobenius norm of the error in hyrbrid approximation

_Notes_:
: The analog and digital combiners are set after approximation.



[Back to methods](#methods)



#### **`impose_beamformer_power_constraint()`** {#impose_beamformer_power_constraint}
> Imposes the power constraint to the digital beamformer such that each stream's beamformer has Frobenius norm of square root of the number of transmit antennas.

Usage:
: `IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()`





[Back to methods](#methods)



#### **`initialize()`** {#initialize}
> Executes initializations for a receiver.

Usage:
: `initialize()`





[Back to methods](#methods)



#### **`n()`** {#n}
> Returns the noise vector (per-antenna).

Usage:
: `val = n()`



_Return Values_:
: `val` --- the noise vector (per-antenna)



[Back to methods](#methods)



#### **`omp_based_hybrid_approximation(A,B,C,Nrf,P)`** {#omp_based_hybrid_approximation}
> 

_Input Arguments_:
: `A` --- predefined RF beamforming vectors as columns (codebook)
: `B` --- desired fully-digital beamformer
: `C` --- identity of size Nr by Nr (eye(Nr)) (covariance matrix E[yy*]?)
: `Nrf` --- number of RF chains
: `P` --- power constraint (optional)



[Back to methods](#methods)



#### **`parse_channel_state_information()`** {#parse_channel_state_information}
> Splits the CSI into two components: (i) a struct containing CSI for receiving from the source and (ii) a cell of structs containing CSI for all other links (e.g., interference channels).

Usage:
: `[csi_des,csi_int] = parse_channel_state_information()`



_Return Values_:
: `csi_des` --- a CSI struct
: `csi_int` --- a cell of one or more CSI structs



[Back to methods](#methods)



#### **`receiver_hybrid()`** {#receiver_hybrid}
> 



[Back to methods](#methods)



#### **`s()`** {#s}
> Returns the receive symbol vector.

Usage:
: `val = s()`



_Return Values_:
: `val` --- the receive symbol vector



[Back to methods](#methods)



#### **`set_array(array)`** {#set_array}
> Sets the receiver's array object and updates the number of receive antennas.

Usage:
: `set_array(array)`



_Input Arguments_:
: `array` --- an array object

_Notes_:
: Also updates the noise covariance.



[Back to methods](#methods)



#### **`set_channel_state_information(csi)`** {#set_channel_state_information}
> Sets the receive channel state information (CSI).

Usage:
: `set_channel_state_information(csi)`



_Input Arguments_:
: `csi` --- a cell of channel state information structs



[Back to methods](#methods)



#### **`set_combiner(W)`** {#set_combiner}
> Sets the receive combining matrix.

Usage:
: `set_combiner(W)`



_Input Arguments_:
: `W` --- receive combining matrix



[Back to methods](#methods)



#### **`set_combiner_analog(W)`** {#set_combiner_analog}
> Sets the analog combining matrix.

Usage:
: `set_combiner_analog(W)`



_Input Arguments_:
: `W` --- analog combining matrix

_Notes_:
: Also updates the effective combining matrix.



[Back to methods](#methods)



#### **`set_combiner_analog_amplitude_quantization_law(law)`** {#set_combiner_analog_amplitude_quantization_law}
> Sets the analog combiner's amlitude quantization law (e.g., linear or log).

Usage:
: `set_combiner_analog_amplitude_quantization_law(law)`



_Input Arguments_:
: `law` --- either 'linear' for uniform linear amplitude quantization between (0,1]; or a negative number corresponding to the (amplitude) attenuation step size in dB

_Notes_:
: A law = -0.25 represents an analog combiner with attenuators having a step size of -0.25 dB, meaning with 3 bits of amplitude resolution, for example, the attenuators can attenuate at most -0.25 dB * 2^3 = -2 dB. Practical attenuators often are stepped in such a log-scale, motivating us to support both linear- and log-based amplitude quantization laws.



[Back to methods](#methods)



#### **`set_combiner_analog_amplitude_resolution_bits(bits)`** {#set_combiner_analog_amplitude_resolution_bits}
> Sets the resolution (in bits) of the attenuators in the analog combiner.

Usage:
: `set_combiner_analog_amplitude_resolution_bits(bits)`



_Input Arguments_:
: `bits` --- number of bits of amplitude control

_Notes_:
: When bits = Inf, amplitudes of analog beamforming weights can take on any value



[Back to methods](#methods)



#### **`set_combiner_analog_phase_resolution_bits(bits)`** {#set_combiner_analog_phase_resolution_bits}
> Sets the resolution (in bits) of the phase shifters in the analog combiner.

Usage:
: `set_combiner_analog_phase_resolution_bits(bits)`



_Input Arguments_:
: `bits` --- number of bits of phase control



[Back to methods](#methods)



#### **`set_combiner_digital(W)`** {#set_combiner_digital}
> Sets the digital combiner.

Usage:
: `set_combiner_digital(W)`



_Input Arguments_:
: `W` --- digital combining matrix



[Back to methods](#methods)



#### **`set_hybrid_approximation_method(method)`** {#set_hybrid_approximation_method}
> Sets the method to use during hybrid approximation of a fully-digital beamformer.

Usage:
: `set_hybrid_approximation_method(method)`



_Input Arguments_:
: `method` --- a string declaring which method to use



[Back to methods](#methods)



#### **`set_name(name)`** {#set_name}
> Sets the name of the receiver.

Usage:
: `set_name()`
: `set_name(name)`



_Input Arguments_:
: `name` --- (optional) a string; if not passed, 'receiver' is the default name used



[Back to methods](#methods)



#### **`set_noise(n)`** {#set_noise}
> Sets the noise vector at the receive array.

Usage:
: `set_noise()`
: `set_noise(n)`



_Input Arguments_:
: `n` --- (optional) a noise vector; if not passed, noise will be generated according to the noise power



[Back to methods](#methods)



#### **`set_noise_covariance(Rn)`** {#set_noise_covariance}
> Sets the noise covariance matrix.

Usage:
: `set_noise_covariance(Rn)`



_Input Arguments_:
: `Rn` --- noise covariance matrix



[Back to methods](#methods)



#### **`set_noise_power_per_Hz(noise_psd,unit)`** {#set_noise_power_per_Hz}
> Sets the noise power per Hz based for a given noise power spectral density.

Usage:
: `set_noise_power_per_Hz(noise_psd)`
: `set_noise_power_per_Hz(noise_psd,unit)`



_Input Arguments_:
: `noise_psd` --- noise power spectral density (power per Hz) (e.g., -174 dBm/Hz)
: `unit` --- an optional unit specifying the units of noise_psd (e.g., 'dBm_Hz' for dBm/Hz (default) or 'watts_Hz' for  watts/Hz)

_Notes_:
: Also updates the noise power and noise covariance.



[Back to methods](#methods)



#### **`set_num_rf_chains(L)`** {#set_num_rf_chains}
> Sets the number of RF chains in the receiver's hybrid beamforming structure.

Usage:
: `set_num_rf_chains(L)`



_Input Arguments_:
: `L` --- number of RF chains



[Back to methods](#methods)



#### **`set_num_streams(Ns)`** {#set_num_streams}
> Sets the number of data streams being received.

Usage:
: `set_num_streams(Ns)`



_Input Arguments_:
: `Ns` --- number of data streams



[Back to methods](#methods)



#### **`set_receive_strategy(strategy)`** {#set_receive_strategy}
> Sets the receive strategy.

Usage:
: `set_receive_strategy()`
: `set_receive_strategy(strategy)`



_Input Arguments_:
: `strategy` --- (optional) a string specifying the receive  strategy; options vary depending on receiver type  (digital vs. hybrid); if not passed, identity reception will be used



[Back to methods](#methods)



#### **`set_receive_symbol(s)`** {#set_receive_symbol}
> Sets the receive symbol.

Usage:
: `set_receive_symbol(s)`



_Input Arguments_:
: `s` --- the receive symbol (e.g., after combiner)



[Back to methods](#methods)



#### **`set_received_signal(y)`** {#set_received_signal}
> Sets the received signal vector.

Usage:
: `set_received_signal(y)`



_Input Arguments_:
: `y` --- the received signal vector (i.e., vector impinging the  receive array)



[Back to methods](#methods)



#### **`set_source(device_source)`** {#set_source}
> Sets the source (device the receiver aims to serve).

Usage:
: `set_source(device_source)`



_Input Arguments_:
: `device_source` --- a device object



[Back to methods](#methods)



#### **`set_symbol_bandwidth(B)`** {#set_symbol_bandwidth}
> Sets the receive symbol bandwidth.

Usage:
: `set_symbol_bandwidth(B)`



_Input Arguments_:
: `B` --- symbol bandwidth in Hertz

_Notes_:
: Also updates the symbol period and noise power.



[Back to methods](#methods)



#### **`set_type(type)`** {#set_type}
> Sets the type of the receiver.

Usage:
: `set_type()`
: `set_type(type)`



_Input Arguments_:
: `type` --- (optional) a string; if not passed, 'digital' is the default type used



[Back to methods](#methods)



#### **`show_analog_beamformer(idx)`** {#show_analog_beamformer}
> Plots one of the analog beamformers.

Usage:
: `show_analog_beamformer()`
: `show_analog_beamformer(idx)`



_Input Arguments_:
: `idx` --- an index specifying which beamformer in the analog combining matrix to plot (default of 1)



[Back to methods](#methods)



#### **`show_effective_beamformer(idx)`** {#show_effective_beamformer}
> Plots one of the effective beamformers (combination of analog and digital).

Usage:
: `show_effective_beamformer()`
: `show_effective_beamformer(idx)`



_Input Arguments_:
: `idx` --- an index specifying which beamformer in the effective combining matrix to plot (default of 1)



[Back to methods](#methods)



#### **`turn_off()`** {#turn_off}
> Turns off the receiver by setting its combiner to a zeros matrix.

Usage:
: `turn_off()`





[Back to methods](#methods)



#### **`update_combiner()`** {#update_combiner}
> Updates the effective combining matrix based on the current analog and digital combiners. Only updates if the dimensionality supports the matrix multiply.

Usage:
: `update_combiner()`





[Back to methods](#methods)



#### **`update_noise_covariance()`** {#update_noise_covariance}
> Updates the noise covariance matrix based on the current number of antennas and noise power.

Usage:
: `update_noise_covariance()`



_Notes_:
: We have assumed i.i.d. noise across antennas.



[Back to methods](#methods)



#### **`update_noise_power()`** {#update_noise_power}
> Updates the noise power based on the current noise power per Hz and symbol bandwidth.

Usage:
: `update_noise_power()`



_Notes_:
: Also updates the noise covariance matrix.
: As of now (Jan. 12, 2021), noise per symbol is equal to the noise power per Hz. Changes to notation or future  accommodations may change how we define these two quantities so we are leaving them both present for the time being.



[Back to methods](#methods)



#### **`update_receive_symbol()`** {#update_receive_symbol}
> Updates the receive symbol (after combiner) based on the current received signal and noise.

Usage:
: `update_receive_symbol()`



_Notes_:
: Should be called when the combiner, received signal, or  noise are changed.



[Back to methods](#methods)



#### **`y()`** {#y}
> Returns the received signal vector.

Usage:
: `val = y()`



_Return Values_:
: `val` --- the received signal vector



[Back to methods](#methods)



