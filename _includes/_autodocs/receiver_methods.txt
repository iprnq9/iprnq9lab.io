#### **`N0()`** {#N0}
> Returns the noise energy per symbol.

Usage:
: `val = N0()`



_Return Values_:
: `val` --- the noise energy per symbol



[Back to methods](#methods)



#### **`Nr()`** {#Nr}
> Returns the number of receive antennas.

Usage:
: `val = Nr()`



_Return Values_:
: `val` --- the number of receive antennas



[Back to methods](#methods)



#### **`Ns()`** {#Ns}
> Returns the number of streams.

Usage:
: `val = Ns()`



_Return Values_:
: `val` --- the number of streams



[Back to methods](#methods)



#### **`Rn()`** {#Rn}
> Returns the noise covariance matrix.

Usage:
: `val = Rn()`



_Return Values_:
: `val` --- the noise covariance matrix



[Back to methods](#methods)



#### **`W()`** {#W}
> Returns the combining matrix.

Usage:
: `val = W()`



_Return Values_:
: `val` --- the combining matrix



[Back to methods](#methods)



#### **`check_combiner_dimensions(W)`** {#check_combiner_dimensions}
> Checks to see if the combining matrix has dimensions appropriate for the current number of antennas and streams.

Usage:
: `out = check_combiner_dimensions()`
: `out = check_combiner_dimensions(W)`



_Input Arguments_:
: `W` --- (optional) a combining matrix; if not passed, the receivers's current combiner will be assessed

_Return Values_:
: `out` --- a boolean indicating if the combiner is of appropriate  dimension



[Back to methods](#methods)



#### **`check_receive_strategy(strategy)`** {#check_receive_strategy}
> Returns a boolean indicating if a receive strategy is valid.

Usage:
: `out = check_receive_strategy(strategy)`



_Input Arguments_:
: `strategy` --- a string indentifying a specific receive strategy

_Return Values_:
: `out` --- a boolean



[Back to methods](#methods)



#### **`configure_receiver(strategy)`** {#configure_receiver}
> Configures the receiver's combiner according to the current/specified receive strategy, incorporating channel state information as applicable.

Usage:
: `configure_receiver()`
: `configure_receiver(strategy)`



_Input Arguments_:
: `strategy` --- (optional) receive strategy to use; if not passed, the current receive strategy will be used



[Back to methods](#methods)



#### **`configure_receiver_eigen()`** {#configure_receiver_eigen}
> Configures the receiver to receive along the strongest components of the desired channel, neglecting interference, etc.

Usage:
: `configure_receiver_eigen()`





[Back to methods](#methods)



#### **`configure_receiver_identity()`** {#configure_receiver_identity}
> Configures the receive combiner to an identity matrix.

Usage:
: `configure_receiver_identity()`





[Back to methods](#methods)



#### **`configure_receiver_mmse_desired()`** {#configure_receiver_mmse_desired}
> Configures the receiver in an LMMSE fashion to reject noise. Does not reject interference.

Usage:
: `configure_receiver_mmse_desired()`





[Back to methods](#methods)



#### **`configure_receiver_mmse_interference()`** {#configure_receiver_mmse_interference}
> Configures the receiver in an LMMSE fashion to reject noise and interference.

Usage:
: `configure_receiver_mmse_interference()`





[Back to methods](#methods)



#### **`create(type)`** {#create}
> Creates a receiver.

Usage:
: `obj = receiver.create()`
: `obj = receiver.create(type)`



_Input Arguments_:
: `type` --- (optional) a string specifying the type of receiver to create; either 'digital' or 'hybrid'

_Return Values_:
: `obj` --- a receiver object



[Back to methods](#methods)



#### **`get_receive_symbol()`** {#get_receive_symbol}
> Returns the receive symbol based on the curent received signal vector, noise, and combiner.

Usage:
: `s = get_receive_symbol()`



_Return Values_:
: `s` --- the receive symbol



[Back to methods](#methods)



#### **`get_source_channel_state_information_index()`** {#get_source_channel_state_information_index}
> Returns the index of the receive CSI entry whose transmit device corresponds to the receiver's source device.

Usage:
: `idx = get_source_channel_state_information_index()`



_Return Values_:
: `idx` --- an index; if zero, the CSI entry was not found



[Back to methods](#methods)



#### **`get_valid_receive_strategies()`** {#get_valid_receive_strategies}
> Returns a cell containing all valid receive strategy strings.

Usage:
: `out = get_valid_receive_strategies()`



_Return Values_:
: `out` --- a cell of strings

_Notes_:
: This function will differ between a fully-digital receiver and hybrid digital/analog receiver to account for receive strategies that are specific to each.
: This function will need to be updated anytime a custom/new receive strategy is added.
: The strings should be all lowercase.



[Back to methods](#methods)



#### **`initialize()`** {#initialize}
> Executes initializations for a receiver.

Usage:
: `initialize()`





[Back to methods](#methods)



#### **`n()`** {#n}
> Returns the noise vector (per-antenna).

Usage:
: `val = n()`



_Return Values_:
: `val` --- the noise vector (per-antenna)



[Back to methods](#methods)



#### **`parse_channel_state_information()`** {#parse_channel_state_information}
> Splits the CSI into two components: (i) a struct containing CSI for receiving from the source and (ii) a cell of structs containing CSI for all other links (e.g., interference channels).

Usage:
: `[csi_des,csi_int] = parse_channel_state_information()`



_Return Values_:
: `csi_des` --- a CSI struct
: `csi_int` --- a cell of one or more CSI structs



[Back to methods](#methods)



#### **`receiver(name)`** {#receiver}
> Creates an instance of a receiver object.

Usage:
: `obj = receiver()`
: `obj = receiver(name)`



_Input Arguments_:
: `name` --- (optional) a name for the receiver

_Return Values_:
: `obj` --- a receiver object



[Back to methods](#methods)



#### **`s()`** {#s}
> Returns the receive symbol vector.

Usage:
: `val = s()`



_Return Values_:
: `val` --- the receive symbol vector



[Back to methods](#methods)



#### **`set_array(array)`** {#set_array}
> Sets the receiver's array object and updates the number of receive antennas.

Usage:
: `set_array(array)`



_Input Arguments_:
: `array` --- an array object

_Notes_:
: Also updates the noise covariance.



[Back to methods](#methods)



#### **`set_channel_state_information(csi)`** {#set_channel_state_information}
> Sets the receive channel state information (CSI).

Usage:
: `set_channel_state_information(csi)`



_Input Arguments_:
: `csi` --- a cell of channel state information structs



[Back to methods](#methods)



#### **`set_combiner(W)`** {#set_combiner}
> Sets the receive combining matrix.

Usage:
: `set_combiner(W)`



_Input Arguments_:
: `W` --- receive combining matrix



[Back to methods](#methods)



#### **`set_name(name)`** {#set_name}
> Sets the name of the receiver.

Usage:
: `set_name()`
: `set_name(name)`



_Input Arguments_:
: `name` --- (optional) a string; if not passed, 'receiver' is the default name used



[Back to methods](#methods)



#### **`set_noise(n)`** {#set_noise}
> Sets the noise vector at the receive array.

Usage:
: `set_noise()`
: `set_noise(n)`



_Input Arguments_:
: `n` --- (optional) a noise vector; if not passed, noise will be generated according to the noise power



[Back to methods](#methods)



#### **`set_noise_covariance(Rn)`** {#set_noise_covariance}
> Sets the noise covariance matrix.

Usage:
: `set_noise_covariance(Rn)`



_Input Arguments_:
: `Rn` --- noise covariance matrix



[Back to methods](#methods)



#### **`set_noise_power_per_Hz(noise_psd,unit)`** {#set_noise_power_per_Hz}
> Sets the noise power per Hz based for a given noise power spectral density.

Usage:
: `set_noise_power_per_Hz(noise_psd)`
: `set_noise_power_per_Hz(noise_psd,unit)`



_Input Arguments_:
: `noise_psd` --- noise power spectral density (power per Hz) (e.g., -174 dBm/Hz)
: `unit` --- an optional unit specifying the units of noise_psd (e.g., 'dBm_Hz' for dBm/Hz (default) or 'watts_Hz' for  watts/Hz)

_Notes_:
: Also updates the noise power and noise covariance.



[Back to methods](#methods)



#### **`set_num_streams(Ns)`** {#set_num_streams}
> Sets the number of data streams being received.

Usage:
: `set_num_streams(Ns)`



_Input Arguments_:
: `Ns` --- number of data streams



[Back to methods](#methods)



#### **`set_receive_strategy(strategy)`** {#set_receive_strategy}
> Sets the receive strategy.

Usage:
: `set_receive_strategy()`
: `set_receive_strategy(strategy)`



_Input Arguments_:
: `strategy` --- (optional) a string specifying the receive  strategy; options vary depending on receiver type  (digital vs. hybrid); if not passed, identity reception will be used



[Back to methods](#methods)



#### **`set_receive_symbol(s)`** {#set_receive_symbol}
> Sets the receive symbol.

Usage:
: `set_receive_symbol(s)`



_Input Arguments_:
: `s` --- the receive symbol (e.g., after combiner)



[Back to methods](#methods)



#### **`set_received_signal(y)`** {#set_received_signal}
> Sets the received signal vector.

Usage:
: `set_received_signal(y)`



_Input Arguments_:
: `y` --- the received signal vector (i.e., vector impinging the  receive array)



[Back to methods](#methods)



#### **`set_source(device_source)`** {#set_source}
> Sets the source (device the receiver aims to serve).

Usage:
: `set_source(device_source)`



_Input Arguments_:
: `device_source` --- a device object



[Back to methods](#methods)



#### **`set_symbol_bandwidth(B)`** {#set_symbol_bandwidth}
> Sets the receive symbol bandwidth.

Usage:
: `set_symbol_bandwidth(B)`



_Input Arguments_:
: `B` --- symbol bandwidth in Hertz

_Notes_:
: Also updates the symbol period and noise power.



[Back to methods](#methods)



#### **`set_type(type)`** {#set_type}
> Sets the type of the receiver.

Usage:
: `set_type()`
: `set_type(type)`



_Input Arguments_:
: `type` --- (optional) a string; if not passed, 'digital' is the default type used



[Back to methods](#methods)



#### **`turn_off()`** {#turn_off}
> Turns off the receiver by setting its combiner to a zeros matrix.

Usage:
: `turn_off()`





[Back to methods](#methods)



#### **`update_noise_covariance()`** {#update_noise_covariance}
> Updates the noise covariance matrix based on the current number of antennas and noise power.

Usage:
: `update_noise_covariance()`



_Notes_:
: We have assumed i.i.d. noise across antennas.



[Back to methods](#methods)



#### **`update_noise_power()`** {#update_noise_power}
> Updates the noise power based on the current noise power per Hz and symbol bandwidth.

Usage:
: `update_noise_power()`



_Notes_:
: Also updates the noise covariance matrix.
: As of now (Jan. 12, 2021), noise per symbol is equal to the noise power per Hz. Changes to notation or future  accommodations may change how we define these two quantities so we are leaving them both present for the time being.



[Back to methods](#methods)



#### **`update_receive_symbol()`** {#update_receive_symbol}
> Updates the receive symbol (after combiner) based on the current received signal and noise.

Usage:
: `update_receive_symbol()`



_Notes_:
: Should be called when the combiner, received signal, or  noise are changed.



[Back to methods](#methods)



#### **`y()`** {#y}
> Returns the received signal vector.

Usage:
: `val = y()`



_Return Values_:
: `val` --- the received signal vector



[Back to methods](#methods)



