classdef channel_rayleigh < channel
    % CHANNEL A Rayleigh-faded channel.
    properties
        % pass
    end
    methods
        function obj = channel_rayleigh(name)
            % CHANNEL Creates a MIMO channel object.
            % 
            % Usage:
            %  obj = CHANNEL()
            %  obj = CHANNEL(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing a MIMO channel
            if nargin < 1 || isempty(name)
                name = 'channel-rayleigh';
            end
            obj.name = name;
        end
        
        function H = channel_realization(obj)
            % CHANNEL_REALIZATION Realizes a Rayleigh channel matrix
            % where each entry in the channel matrix is drawn i.i.d. from a
            % complex normal distribution.
            %
            % Usage:
            %  H = CHANNEL_REALIZATION()
            % 
            % Returns:
            %  H: a channel matrix whose entries are Rayleigh-faded
            H = cgauss_rv(0,1,obj.Nr,obj.Nt);
            obj.set_channel_matrix(H);
        end
    end
end