classdef channel_self_interference < channel
    properties
        rician_factor; % Rician factor (linear)
        los_channel; % line-of-sight channel object
        nlos_channel; % non-line-of-sight channel object
    end
    methods
        function obj = channel_self_interference(name)
            % CHANNEL_SELF_INTERFERENCE Creates a self-interference MIMO 
            % channel object.
            % 
            % Usage:
            %  obj = CHANNEL_SELF_INTERFERENCE()
            %  obj = CHANNEL_SELF_INTERFERENCE(name)
            % 
            % Args:
            %  name: an optional name for the object
            % 
            % Returns:
            %  obj: an object representing a MIMO self-interference channel
            if nargin < 1 || isempty(name)
                name = 'channel-self-interference';
            end
            obj.name = name;
        end
        
        function set_propagation_velocity(obj,val)
            % SET_PROPAGATION_VELOCITY Sets the propagation velocity of the
            % channel.
            %
            % Usage:
            %  SET_PROPAGATION_VELOCITY(val)
            %
            % Args:
            %  val: propagation velocity (meters/sec)
            obj.propagation_velocity = val;
            if ~isempty(obj.los_channel)
                obj.los_channel.set_propagation_velocity(val);
            else
                warning('LOS channel not updated with propagation velocity since empty.');
            end
            if ~isempty(obj.nlos_channel)
                obj.nlos_channel.set_propagation_velocity(val);
            else
                warning('NLOS channel not updated with propagation velocity since empty.');
            end
        end
        
        function set_carrier_frequency(obj,fc)
            % SET_CARRIER_FREQUENCY Sets the carrier frequency of the
            % channel.
            %
            % Usage:
            %  SET_CARRIER_FREQUENCY(fc)
            %
            % Args:
            %  fc: carrier frequency (Hz)
            obj.carrier_frequency = fc;
            obj.carrier_wavelength = obj.propagation_velocity / fc;
            if ~isempty(obj.los_channel)
                obj.los_channel.set_carrier_frequency(fc);
            else
                warning('LOS channel not updated with carrier frequency since empty.');
            end
            if ~isempty(obj.nlos_channel)
                obj.nlos_channel.set_carrier_frequency(fc);
            else
                warning('NLOS channel not updated with carrier frequency since empty.');
            end
        end
        
        function set_transmit_array(obj,array)
            % SET_TRANSMIT_ARRAY Sets the transmit array object. Also sets
            % the number of transmit antennas accordingly.
            % 
            % Usage:
            %  SET_TRANSMIT_ARRAY(array)
            % 
            % Args:
            %  array: an array object
            obj.atx = array;
            obj.Nt = array.Na;
            if ~isempty(obj.los_channel)
                obj.los_channel.set_transmit_array(array);
            else
                warning('LOS channel not updated with transmit array since empty.');
            end
            if ~isempty(obj.nlos_channel)
                obj.nlos_channel.set_transmit_array(array);
            else
                warning('NLOS channel not updated with transmit array since empty.');
            end
        end
        
        function set_receive_array(obj,array)
            % SET_RECEIVE_ARRAY Sets the receive array object. Also sets
            % the number of receive antennas accordingly.
            % 
            % Usage:
            %  SET_RECEIVE_ARRAY(array)
            % 
            % Args:
            %  array: an array object
            obj.arx = array;
            obj.Nr = array.Na;
            if ~isempty(obj.los_channel)
                obj.los_channel.set_receive_array(array);
            else
                warning('LOS channel not updated with receive array since empty.');
            end
            if ~isempty(obj.nlos_channel)
                obj.nlos_channel.set_receive_array(array);
            else
                warning('NLOS channel not updated with receive array since empty.');
            end
        end
        
        function set_rician_factor(obj,K,unit)
            % SET_RICIAN_FACTOR Sets the Rician factor of the channel.
            %
            % Usage:
            %  SET_RICIAN_FACTOR(K)
            %  SET_RICIAN_FACTOR(K,unit)
            % 
            % Args:
            %  K: the Rician factor (power)
            %  unit: the unit of K (optional)
            %
            % Notes:
            %  Based on our model (and is common), the Rician factor is 
            %  considered to be a power factor. The Rician factor is often 
            %  given in units of dB, meaning such a value is a power gain 
            %  in dB.
            %
            % See also:
            %  CHANNEL_SELF_INTERFERENCE.RICIAN_ADD
            if nargin < 3 || isempty(unit)
                unit = 'linear';
            end
            if strcmp(unit,'dB')
                K = 10^(K/10);
            end
            obj.rician_factor = K;
        end
        
        function set_los_channel(obj,H)
            % SET_LOS_CHANNEL Sets the line-of-sight channel object.
            %
            % Usage:
            %  SET_LOS_CHANNEL(H)
            % 
            % Args:
            %  H: a channel object representing the line-of-sight channel
            obj.los_channel = H;
        end
        
        function set_nlos_channel(obj,H)
            % SET_NLOS_CHANNEL Sets the non-line-of-sight channel object.
            %
            % Usage:
            %  SET_NLOS_CHANNEL(H)
            % 
            % Args:
            %  H: a channel object representing the non-line-of-sight 
            %     channel
            obj.nlos_channel = H;
        end
        
        function H = rician_add(obj,LOS,NLOS,K)
            % RICIAN_ADD Adds a line-of-sight MIMO channel and non-line-of-
            % sight MIMO channel in a Rician fashion.
            %
            % Usage:
            %  H = RICIAN_ADD(LOS,NLOS,K)
            %
            % Args:
            %  LOS: line-of-sight MIMO channel
            %  NLOS: non-line-of-sight MIMO channel
            %  K: the Rician factor (linear power)
            %
            % Returns:
            %  H: the resulting channel matrix after Rician summation
            %
            % Notes:
            %  Notice that we are taking the square root below, indicating
            %  that K is a linear power term.
            [M,N] = size(LOS);
            [L,P] = size(NLOS);
            if ~(M == L) || ~(N == P)
                error('Sizes of LOS and NLOS channels must be equal.');
            end
            if K < 0
                error('Rician factor must be at least 0.');
            end
            H = sqrt(K/(K+1)) .* LOS + sqrt(1/(K+1)) .* NLOS;
        end
        
        function H = channel_realization(obj)
            % CHANNEL_REALIZATION Realizes a random instance of the channel
            % matrix based on the channel object's parameters. In this
            % case, the LOS and NLOS channels are realized and then summed
            % together in a Rician fashion.
            %
            % Usage:
            %  H = CHANNEL_REALIZATION()
            %
            % Returns:
            %  H: the resulting channel matrix
            H_LOS = obj.los_channel.channel_realization();
            H_NLOS = obj.nlos_channel.channel_realization();
            H = obj.rician_add(H_LOS,H_NLOS,obj.rician_factor);
            obj.set_channel_matrix(H);
        end
    end
end