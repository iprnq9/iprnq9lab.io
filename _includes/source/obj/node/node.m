classdef node < handle
    properties
        name; % human-readable identifier
        type; % 'full-duplex' or 'half-duplex'
        x; % x coordinate
        y; % y coordinate
        z; % z coordinate
        coordinate; % [x,y,z] coordinates
        tx; % transmitter
        rx; % receiver
        marker; % plot marker
        transmit_strategy; % struct specifying the precoding strategy
        receive_strategy; % struct specifying the combining strategy
        transmit_csi; % transmit channel state information
        receive_csi; % receive channel state information
        transmit_symbol;
        receive_symbol;
    end
    methods
        function obj = node(name)
            % NODE Creates a node object.
            % 
            % Usage:
            %  obj = NODE()
            %  obj = NODE(name)
            % 
            % Args:
            %  name: an optional name for the node
            % 
            % Returns:
            %  obj: an object representing a node
            if nargin < 1 || isempty(name)
                name = 'node';
            end
            obj.name = name;
            obj.marker = 'kx';
            obj.set_coordinate([0,0,0]);
            transmit_strategy.strategy = 'eigen';
            receive_strategy.strategy = 'eigen';
            obj.set_transmit_strategy(transmit_strategy);
            obj.set_receive_strategy(receive_strategy);
        end
        
        function set_coordinate(obj,x,y,z)
            % SET_COORDINATE Sets the node's position in 3-D space.
            % 
            % Usage:
            %  SET_COORDINATE(x)
            %  SET_COORDINATE(x,y)
            %  SET_COORDINATE(x,y,z)
            %
            % Args:
            %  x: x-coordinate of the node's position
            %  y: y-coordinate of the node's position
            %  z: z-coordinate of the node's position
            if nargin < 3
                if length(x) == 1
                    y = 0;
                    z = 0;
                elseif length(x) == 2
                    y = x(2);
                    x = x(1);
                    z = 0;
                elseif length(x) == 3
                    z = x(3);
                    y = x(2);
                    x = x(1);
                else
                    error('Invalid coordinate.')
                end
            elseif nargin < 4
                if length(x) == 1 && length(y) == 1
                    z = 0;
                else
                    error('Invalid coordinate.');
                end
            end
            obj.x = x;
            obj.y = y;
            obj.z = z;
            obj.coordinate = [x,y,z];
        end
        
        function set_marker(obj,marker)
            % SET_MARKER Sets the node marker.
            %
            % Usage:
            %  SET_MARKER(marker)
            %
            % Args:
            %  marker: a MATLAB-compliant plot marker
            obj.marker = marker;
        end
        
        function set_transmitter(obj,tx)
            % SET_TRANSMITTER Sets the transmitter for the node.
            %
            % Usage:
            %  SET_TRANSMITTER(tx)
            %
            % Args:
            %  tx: a transmitter object
            obj.tx = tx;
        end
        
        function set_receiver(obj,rx)
            % SET_RECEIVER Sets the receiver for the node.
            %
            % Usage:
            %  SET_RECEIVER(rx)
            %
            % Args:
            %  rx: a receiver object
            obj.rx = rx;
        end

        function set_transmit_symbol(obj,s)
            obj.transmit_symbol = s;
            obj.tx.set_transmit_symbol(s);
        end
        
        function set_receive_symbol(obj,s)
            obj.receive_symbol = s;
            obj.rx.set_receive_symbol(s);
        end
        
        function set_transmit_num_streams(obj,n)
            obj.tx.set_num_streams(n);
        end
        
        function set_receive_num_streams(obj,n)
            obj.rx.set_num_streams(n);
        end
        
        function set_transmit_array(obj,array)
            obj.tx.set_array(array);
        end
        
        function set_receive_array(obj,array)
            obj.rx.set_array(array);
        end
        
        function set_transmit_power(obj,P,unit)
            if nargin < 3 || isempty(unit)
                unit = 'Watts';
            end
            obj.tx.set_transmit_power(P,unit);
        end
        
        function set_transmit_symbols_covariance(obj,Rs)
            obj.tx.set_transmit_symbols_covariance(Rs);
        end
        
        function set_noise_psd(obj,noise_psd,unit)
            if nargin < 3 || isempty(unit)
                unit = 'dBm_Hz';
            end
            obj.rx.set_noise_psd(noise_psd,unit);
        end
        
        function set_transmit_csi(obj,csi)
            obj.transmit_csi = csi;
            obj.tx.set_csi(csi);
        end
        
        function set_receive_csi(obj,csi)
            obj.receive_csi = csi;
            obj.rx.set_csi(csi);
        end
                
        function set_transmit_strategy(obj,strategy)
            obj.transmit_strategy = strategy;
        end
        
        function set_receive_strategy(obj,strategy)
            obj.receive_strategy = strategy;
        end
        
        function set_transmit_hybrid_approximation_method(obj,method)
            obj.tx.set_hybrid_approximation_method(method);
        end
        
        function set_receive_hybrid_approximation_method(obj,method)
            obj.rx.set_hybrid_approximation_method(method);
        end
        
        function set_hybrid_approximation_method(obj,method)
            obj.set_transmit_hybrid_approximation_method(method);
            obj.set_receive_hybrid_approximation_method(method);
        end
        
        function transmit_beamform(obj)
            strategy = obj.transmit_strategy.strategy;
            if strcmp(strategy,'eigen')
                F = obj.tx.get_eigen_precoder();
                if strcmp(obj.tx.type,'hybrid')
                    obj.tx.hybrid_approximation(F);
                else
                    obj.tx.set_precoder(F);
                end
            elseif strcmp(strategy,'bfc')
                F = obj.tx.get_eigen_precoder();
                H_SI = obj.si_csi.H;
                P = get_projection_matrix(null(H_SI));
                F = P * F;
                if strcmp(obj.tx.type,'hybrid')
                    obj.tx.hybrid_approximation(F);
                else
                    obj.tx.set_precoder(F);
                end
            end
            obj.tx.impose_beamformer_power_constraint();
        end
        
        function receive_beamform(obj)
            strategy = obj.receive_strategy.strategy;
            if strcmp(strategy,'eigen')
                W = obj.rx.get_eigen_combiner();
                if strcmp(obj.rx.type,'hybrid')
                    obj.rx.hybrid_approximation(W);
                else
                    obj.rx.set_combiner(W);
                end
            end
        end
        
        function show_tx_rx_arrays_3d(obj,fignum)
            if nargin < 2 || isempty(fignum)
                figure();
            else
                figure(fignum);
            end
            ax = axes();
            hold(ax,'off');
            obj.tx.array.show3d(ax);
            hold(ax,'on');
            obj.rx.array.show3d(ax);
            hold(ax,'off');
        end
    end
end
        