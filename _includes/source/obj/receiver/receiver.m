classdef receiver < handle
    % RECEIVER A generic MIMO receiver.
    properties
        name; % human-readable identifier
        array; % array object
        Nr; % number of receive antennas
        num_streams; % number of data streams
        combiner; % effective (fully-digital) receive combining matrix
        csi;
        type;
        Rn;
        receive_symbol; 
        noise_psd_dBm_Hz;
        noise_psd_watts_Hz;
    end
    methods
        function obj = receiver(name)
            % RECEIVER Creates an instance of a receiver object.
            if nargin < 1 || isempty(name)
                name = 'receiver';
            end
            obj.name = name;
            obj.type = 'digital';
            obj.set_num_streams(1);
        end
        
        function set_receive_symbol(obj,s)
            obj.receive_symbol = s;
        end
        
        function set_num_streams(obj,Ns)
            % SET_NUM_STREAMS Sets the number of data streams being
            % received.
            %
            % Usage:
            %  SET_NUM_STREAMS(Ns)
            %
            % Args:
            %  Ns: number of data streams
            obj.num_streams = Ns;
        end
        
        function set_csi(obj,csi)
            obj.csi = csi;
        end
        
        function set_array(obj,array)
            % SET_ARRAY Sets the receiver's array object and updates the
            % number of receive antennas.
            %
            % Usage:
            %  SET_ARRAY(array)
            %
            % Args:
            %  array: an array object
            obj.array = array;
            obj.Nr = obj.array.Na;
            % obj.Rn = eye(obj.Nr);
        end

        function set_combiner(obj,W)
            % SET_COMBINER Sets the receive combining matrix.
            %
            % Usage:
            %  SET_COMBINER(W)
            %
            % Args:
            %  W: receive combining matrix
            obj.combiner = W;
        end
        
        function set_noise_psd(obj,noise_psd,unit)
            if nargin < 3 || isempty(unit)
                unit = 'dBm_Hz';
            end
            if strcmp(unit,'dBm_Hz')
                noise_psd_watts_Hz = 10^((noise_psd-30)/10);
                noise_psd_dBm_Hz = noise_psd;
            elseif strcmp(unit,'watts_Hz')
                noise_psd_watts_Hz = noise_psd;
                noise_psd_dBm_Hz = 10*log10(noise_psd);
            end
            obj.noise_psd_dBm_Hz = noise_psd_dBm_Hz;
            obj.noise_psd_watts_Hz = noise_psd_watts_Hz;
            obj.Rn = noise_psd_watts_Hz * eye(obj.Nr);
        end
        
        function [W,S] = get_eigen_combiner(obj,H)
            % GET_EIGEN_COMBINER Returns the left singular vectors
            % corresponding to the strongest eigenchannels of the channel.
            %
            % Usage:
            %  [W,S] = GET_EIGEN_COMBINER(H)
            %
            % Args:
            %  H: the channel matrix
            %
            % Returns:
            %  W: the left singular vectors corresponding to the strongest
            %  singular values of the channel matrix
            %  S: the strongest singular values of the channel
            if nargin < 2 || isempty(H)
                H = obj.csi.H; % use CSI
            end
            Ns = obj.num_streams;
            [U,S,~] = svd(H);
            S = diag(S);
            S = S(1:Ns);
            W = U(:,1:Ns);
        end
    end
end