classdef mmwave_transmitter < transmitter
    % MMWAVE_TRANSMITTER A millimeter-wave MIMO transmitter.
    properties
        num_rf_chains; % number of RF chains
        precoder_digital; % digital precoding matrix
        precoder_analog; % analog precoding matrix
        phase_shifter_resolution; % analog phase shifter resolution (bits)
        amplitude_control; % assume constant amplitude by default
        amplitude_resolution; % analog amplitude resolution (bits)
        hybrid_approximation_method; % hybrid approximation method
        hybrid_architecture; % TO-DO: fully-connected, subarray, etc. % make a struct? 
        % subarray struct would have number of antennas per subarray
    end
    methods
        function obj = mmwave_transmitter(name)
            % MMWAVE_TRANSMITTER Creates an instance of a millimeter-wave 
            % transmitter object.
            if nargin < 1 || isempty(name)
                name = 'mmwave-transmitter';
            end
            obj.name = name;
            obj.type = 'hybrid';
            obj.set_amplitude_control(false);
        end
        
        function set_num_rf_chains(obj,Nrf)
            % SET_NUM_RF_CHAINS Sets the number of RF chains in the
            % transmitter's hybrid beamforming structure.
            %
            % Usage:
            %  SET_NUM_RF_CHAINS(Nrf)
            % 
            % Args:
            %  Nrf: number of RF chains
            if Nrf < obj.num_streams
                warning('Number of RF chains is less than number of data streams.');
            end
            obj.num_rf_chains = Nrf;
        end
        
        function set_precoder_analog(obj,F)
            % SET_PRECODER_ANALOG Sets the analog precoding matrix.
            %
            % Usage:
            %  SET_PRECODER_ANALOG(F)
            % 
            % Args:
            %  F: analog precoding matrix
            [Nt,Nrf] = size(F);
            if ~(Nrf == obj.num_rf_chains)
                warning('The number of columns in F should be equal to the number of RF chains.');
            end
            if ~(Nt == obj.Nt)
                warning('The number of rows in F should be equal to the number of transmit antennas.');
            end
            obj.precoder_analog = F;
        end
        
        function set_precoder_digital(obj,F)
            % SET_PRECODER_DIGITAL Sets the digital precoder.
            %
            % Usage:
            %  SET_PRECODER_DIGITAL(F)
            %
            % Args:
            %  F: digital precoding matrix
            obj.precoder_digital = F;
        end
        
%         function impose_beamformer_power_constraint(obj)
%             % obj.impose_analog_beamformer_power_constraint();
%             % obj.impose_digital_beamformer_power_constraint();
%             F = obj.precoder_analog * obj.precoder_digital;
%             p = 1; % sqrt(obj.Nt);
%             Ns = obj.num_streams;
%             for i = 1:Ns
%                 f = F(:,i);
%                 n = norm(f);
%                 if n > 0
%                     % F(:,i) = f ./ n .* p;
%                     obj.precoder_digital(:,i) = obj.precoder_digital(:,i) ./ n * p;
%                 end
%             end
%         end
        
        function impose_analog_beamformer_power_constraint(obj)
            % IMPOSE_ANALOG_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the analog beamformer such that each beamformer 
            % has Frobenius norm  of one.
            %
            % Usage:
            %  IMPOSE_ANALOG_BEAMFORMER_POWER_CONSTRAINT()
            F = obj.precoder_analog;
            p = 1;
            Ns = obj.num_streams;
            for i = 1:Ns
                f = F(:,i);
                n = norm(f);
                if n > 0
                    F(:,i) = f ./ n .* p ./ sqrt(Ns);
                end
            end
            obj.precoder_analog = F;
        end
        
        function impose_digital_beamformer_power_constraint(obj)
            % IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the digital beamformer such that each stream's
            % beamformer has Frobenius norm  of square root of the number
            % of transmit antennas.
            %
            % Usage:
            %  IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()
            F = obj.precoder_digital;
            p = sqrt(obj.Nt);
            Ns = obj.num_streams;
            N = norm(F,'fro')^2;
            obj.precoder_digital = F ./ sqrt(N);
        end
        
        function set_phase_shifter_resolution(obj,bits)
            % SET_PHASE_SHIFTER_RESOLUTION Sets the resolution (in bits) of
            % the phase shifters in the analog beamformer.
            %
            % Usage:
            %  SET_PHASE_SHIFTER_RESOLUTION(bits)
            %
            % Args:
            %  bits: number of bits of phase control
            obj.phase_shifter_resolution = bits;
        end
        
        function set_amplitude_control(obj,option)
            % SET_AMPLITUDE_CONTROL Sets whether or not there is amplitude
            % control in the analog beamformer. If there is no amplitude
            % control, the analog beamformer must abide by a constant
            % amplitude constraint.
            %
            % Usage:
            %  SET_AMPLITUDE_CONTROL(option)
            %
            % Args:
            %  option: a boolean option representing if the analog
            %  beamformer has amplitude control or not
            obj.amplitude_control = option;
        end
        
        function set_amplitude_resolution(obj,bits)
            % SET_AMPLITUDE_RESOLUTION Sets the resolution (in 
            % bits) of the amplitude gain in the analog beamformer.
            %
            % Usage:
            %  SET_AMPLITUDE_RESOLUTION(bits)
            %
            % Args:
            %  bits: number of bits of amplitude control
            if ~obj.amplitude_control
                warning('The transmitter amplitude control is currently set to false. Setting resolution anyway.');
            end
            obj.amplitude_resolution = bits;
        end
        
        function set_hybrid_approximation_method(obj,method)
            % SET_HYBRID_APPROXIMATION_METHOD Sets the method to use during
            % hybrid approximation of a fully-digital beamformer.
            %
            % Usage:
            %  SET_HYBRID_APPROXIMATION_METHOD(method)
            % 
            % Args:
            %  method: a string declaring which method to use
            obj.hybrid_approximation_method = method;
        end
                
        function [X,RF,BB,err] = hybrid_approximation(obj,F)
            % HYBRID_APPROXIMATION Performs hybrid approximation of a
            % fully-digital beamforming matrix based on the hybrid
            % approximation method and any associated options.
            %
            % Usage:
            %  [X,RF,BB,err] = HYBRID_APPROXIMATION(F,opt)
            %
            % Args:
            %  F: fully-digital precoding matrix
            %  opt: struct of options specific to the hybrid approximation
            %       method
            %
            % Returns:
            %  X: the effective fully-digital beamforming matrix
            %  RF: the resulting analog beamforming matrix
            %  BB: the resulting digital beamforming matrix
            %  err: the squared Frobenius norm of the error in hyrbrid
            %       approximation
            % 
            % Notes:
            %  The analog and digital precoders are set after
            %  approximation.
            opt = obj.hybrid_approximation_method;
            method = opt.method;
            if strcmp(method,'omp')
                A = opt.codebook;
                [X,RF,BB] = obj.omp_based_hybrid_approximation(F,A);
            else
                error('Invalid hybrid approximation method.');
            end
            err = norm((F-X).^2,'fro');
            obj.set_precoder_analog(RF);
            obj.set_precoder_digital(BB);
            obj.set_precoder(X);
        end
        
        function [X,X_RF,X_BB] = omp_based_hybrid_approximation(obj,F,A)
            % Spatially Sparse Precoding/Combining Design via OMP
            % 
            % Args:
            %  A: predefined RF beamforming vectors as columns (codebook)
            %  B: desired fully-digital beamformer
            %  C: identity of size Nt by Nt (eye(Nt)) (covariance matrix E[yy*]?)
            %  Nrf: number of RF chains
            %  P: power constraint (optional)           
            P = 1;
            C = eye(obj.array.Na);
            Nrf = obj.num_rf_chains;
            X_res = F;
            X_RF = [];
            for r = 1:Nrf
                PHI = A' * C' * X_res;
                k = argmax(diag(PHI * PHI'));
                X_RF = [X_RF A(:,k)];
                X_BB = inv(X_RF' * C' * C * X_RF) * X_RF' * C' * F;
                num = F - C * X_RF * X_BB;
                X_res = num / norm(num,'fro');
            end
            X_BB = sqrt(P) * X_BB / norm(X_RF * X_BB,'fro');
            X = X_RF * X_BB;
        end
        
        function impose_beamformer_power_constraint(obj)
            % IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the digital beamformer such that each stream's
            % beamformer has Frobenius norm  of square root of the number
            % of transmit antennas.
            %
            % Usage:
            %  IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()
            F = obj.precoder;
            BB = obj.precoder_digital;
            p = sqrt(obj.Nt);
            p = 1;
            Ns = obj.num_streams;
            N = norm(F,'fro');
            obj.precoder = F ./ N;
            obj.precoder_digital = BB ./ N;
        end
        
        function show_analog_beamformer(obj,idx)
            % SHOW_ANALOG_BEAMFORMER Plots one of the analog beamformers.
            %
            % Usage:
            %  SHOW_ANALOG_BEAMFORMER()
            %  SHOW_ANALOG_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the analog
            %  precoding matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            f = obj.precoder_analog(:,idx);
            obj.array.show_beamformer_pattern(f);
        end
                
        function show_effective_beamformer(obj,idx)
            % SHOW_EFFECTIVE_BEAMFORMER Plots one of the effective 
            % beamformers (combination of analog and digital).
            %
            % Usage:
            %  SHOW_EFFECTIVE_BEAMFORMER()
            %  SHOW_EFFECTIVE_BEAMFORMER(idx)
            %
            % Args:
            %  idx: an index specifying which beamformer in the effective
            %  precoding matrix to plot (default of 1)
            if nargin < 2 || isempty(idx)
                idx = 1;
            end
            F = obj.precoder_analog * obj.precoder_digital;
            f = F(:,idx);
            obj.array.show_beamformer_pattern(f);
        end
    end
end