classdef transmitter < handle
    % TRANSMITTER A generic MIMO transmitter.
    properties
        name; % human-readable identifier
        array; % array object
        Nt; % number of transmit antennas
        num_streams; % number of data streams
        transmit_power; % transmit power (Watts)
        precoder; % transmit precoding matrix
        transmit_symbol; % transmit symbol vector
        csi; % transmit channel state information
        type; % 'digital', 'hybrid'
        Rs; % transmit symbols covariance matrix
        precoder_power_budget; % total power allotted to precoding matrix
    end
    methods
        function obj = transmitter(name)
            % TRANSMITTER Creates an instance of a transmitter object.
            %
            % Usage:
            %  obj = TRANSMITTER()
            %  obj = TRANSMITTER(name)
            %
            % Args:
            %  name: an optional human-readable identifier for the object
            %
            % Returns:
            %  obj: a transmitter object
            if nargin < 1 || isempty(name)
                name = 'transmitter';
            end
            obj.name = name;
            obj.type = 'digital';
            obj.set_num_streams(1);
            obj.set_transmit_power(1);
        end
        
        function set_transmit_symbol(obj,s)
            % SET_TRANSMIT_SYMBOL Sets the symbol vector to be transmitted.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOL(s)
            %
            % Args:
            %  s: the symbol vector transmitted
            obj.transmit_symbol = s;
        end
        
        function set_num_streams(obj,Ns)
            % SET_NUM_STREAMS Sets the number of data streams being
            % transmitted.
            %
            % Usage:
            %  SET_NUM_STREAMS(Ns)
            %
            % Args:
            %  Ns: number of data streams
            obj.num_streams = Ns;
            obj.Rs = eye(Ns) ./ sqrt(Ns);
        end
        
        function set_csi(obj,csi)
            % SET_CSI Sets the transmit channel state information (CSI).
            %
            % Usage:
            %  SET_CSI(csi)
            %
            % Args:
            %  csi: a struct containing the CSI (e.g., channel and SNR)
            obj.csi = csi;
        end
        
        function set_array(obj,array)
            % SET_ARRAY Sets the transmitter's array object and updates the
            % number of transmit antennas.
            %
            % Usage:
            %  SET_ARRAY(array)
            %
            % Args:
            %  array: an array object
            obj.array = array;
            obj.Nt = obj.array.Na;
        end
        
        function set_precoder_power_budget(obj,P)
            % SET_PRECODER_POWER_BUDGET Sets the 
        end
        
        function set_transmit_power(obj,P,unit)
            % SET_TRANSMIT_POWER Sets the transmit power.
            %
            % Usage:
            %  SET_TRANSMIT_POWER(P)
            %  SET_TRANSMIT_POWER(P,unit)
            %
            % Args:
            %  P: the transmit power
            %  unit: an optional unit specifying the units of P (default of
            %  Watts)
            if nargin < 3 || isempty(unit)
                unit = 'Watts';
            end
            if strcmp(unit,'dBm')
                P = 10^((P-30)/10);
            elseif strcmp(unit,'dBW')
                P = 10^(P/10);
            end
            obj.transmit_power = P;
        end
        
        function set_transmit_symbols_covariance(obj,Rs)
            % SET_TRANSMIT_SYMBOLS_COVARIANCE Sets the transmit symbols
            % covariance matrix.
            %
            % Usage:
            %  SET_TRANSMIT_SYMBOLS_COVARIANCE(Rs)
            %
            % Args:
            %  Rs: the covariance matrix of the transmit symbols
            if ~ishermitian(Rs)
                warning('Transmit symbols covariance matrix is not Hermitian.');
            end
            obj.Rs = Rs;
        end
        
        function set_precoder(obj,F)
            % SET_PRECODER Sets the transmit precoding matrix.
            %
            % Usage:
            %  SET_PRECODER(F)
            %
            % Args:
            %  F: transmit precoding matrix
            obj.precoder = F;
        end
                
        function [F,S] = get_eigen_precoder(obj,H,snr)
            % GET_EIGEN_PRECODER Returns the right singular vectors
            % corresponding to the strongest eigenchannels of the channel.
            %
            % Usage:
            %  [F,S] = GET_EIGEN_PRECODER(H)
            %
            % Args:
            %  H: the channel matrix
            %
            % Returns:
            %  F: the right singular vectors corresponding to the strongest
            %  singular values of the channel matrix
            %  S: the strongest singular values of the channel
            if nargin < 2 || isempty(H)
                H = obj.csi.H; % use CSI
            end
            if nargin < 3 || isempty(snr)
                snr = obj.csi.snr; % use CSI
            end
            Ns = obj.num_streams;
            P = obj.precoder_power_budget;
            error('fix this ian');
            p = waterfilling(H,P,SNR,Ns)
            [~,S,V] = svd(H);
            S = diag(S);
            S = S(1:Ns);
            F = V(:,1:Ns);
            for i = 1:Ns
                F(:,i) = F(:,i) ./ norm(F(:,i),'fro')^2 ./ sqrt(Ns); % * sqrt(obj.Nt);
            end
        end
        
        function impose_beamformer_power_constraint(obj)
            % IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT Imposes the power
            % constraint to the digital beamformer such that each stream's
            % beamformer has Frobenius norm  of square root of the number
            % of transmit antennas.
            %
            % Usage:
            %  IMPOSE_DIGITAL_BEAMFORMER_POWER_CONSTRAINT()
            F = obj.precoder;
            p = sqrt(obj.Nt);
            p = 1;
            Ns = obj.num_streams;
            for i = 1:Ns
                f = F(:,i);
                n = norm(f);
                if n > 0
                    F(:,i) = f ./ n ./ sqrt(Ns) .* p;
                    % obj.precoder_digital = obj.precoder_digital ./ n .* p;
                end
            end
            obj.precoder = F;
        end
    end
end