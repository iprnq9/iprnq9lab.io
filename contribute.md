---
layout: default
title: Contribute
categories: contribute
published: true
---

# Contributing to MFM

One of MFM's greatest qualities is that it welcomes customization by its users.

Please refer to the following guides on how to create custom objects in MFM.
- [Creating a Custom Channel Model](/guides/creating_custom_channel_model/)

If you have created a custom channel model, path loss model, etc. and would like it considered to be included into MFM, we encourage you to email {% include email.html %}. From there, discussion and any preparations will be made to ensure your contribution complies with MFM and is suitable to be included into MFM.

We appreciate those willing to contribute!




