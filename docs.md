---
layout: default
title: Documentation
categories: docs
permalink: docs/
---

# Documentation

The most complete form of documentation for MFM can be found on this website.

[This PDF](/pdf/mfm.pdf) also provides a centralized summary of MFM and its objects.

Feel free to email {% include email.html %} with questions and feedback.


## Objects

With the hopes of simplifying the use of MFM, an object-oriented approach was taken. Listed below are links to pages describing the different objects used in MFM.

{% assign supers = site.docs | where: 'super', 'true' %}
{% for super in supers %}
{% assign obj = super.object %}
{% assign subs = site.docs | where: "superclass", obj %}
[**{{ super.title }}**]({{ super.url }})  
_{{ super.summary }}_  
{% unless subs == empty %}Subclasses:&nbsp;
{% for sub in subs %}[{{ sub.title }}]({{ sub.url }}) &nbsp;&nbsp;
{% endfor %}
{% endunless %}
{% endfor %}


## Object-Oriented Relationships of MFM

To fully understand and use MFM, it is important that users know how the several objects are related.

{% include image.html file="mfm_oop_01.svg" caption="The object relationships of a two node network, for example." %}

There are four levels of object hierarchy in MFM. From lowest to highest we have:
1. the array/channel level
2. the transmitter/receiver level
3. the device/link level
4. the network level

Each `transmitter` and `receiver` has an `array`. `channels` capture propagation between transmit and receive arrays.

`devices`, which can transmit and/or receive, are comprised of a `transmitter` and/or `receiver` object. 

`links` capture the physical connection between `devices`, described by the channel and path loss between them.

A collection of `devices` and `links` comprises a `network_mfm` object.


## How MFM Can Be Used

MFM can be used in various capacities, depending on one's use-case. 

At the lowest level, for example, users may only use the `array` object's capabilities and no other components of MFM.

In another case, users may want to [use MFM as a channel generator](/guides/using_mfm_as_channel_generator/), meaning only the `array` and `channel` objects are necessary.

Taking it a step higher, users may be interested in using MFM at its `device` and `link` level, without using the `network` object at all.

At the highest level, MFM can be used at its `network` level, where effectively all components of MFM are being used, such as in the guide [A Complete Network-Level Example](/guides/a_complete_network_level_example/).


## Frequency-Selectivity

MFM was created under a frequency-flat assumption. It could be extended to frequency-selective channels, but the current version of MFM does not incorporate frequency-selectivity.
