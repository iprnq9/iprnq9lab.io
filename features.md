---
layout: default
title: Features
categories: features
permalink: features/
---


# Features

MFM was designed to simulate symbol-level MIMO formulations like the one shown below.

$$
\hat{\mathbf{s}} = \sqrt{P_{\mathrm{tx}}} \cdot G \cdot \mathbf{W}^{*} \mathbf{H} \mathbf{F} \mathbf{s} + \mathbf{W}^{*} \mathbf{n}
$$

While simulating a simple linear equation like the one above is typically straightforward in MATLAB, MFM aims to provide users with a framework that facilitates simplicity and accuracy, especially when networks grow to multiple users.


## Handling Many Users

When the number of transmitters and receivers in a setting increases, the complexity of the simulation can grow prohibitively. For instance, computing the precoding and combining matrices for dozens of users would likely be overwhelming to write in MATLAB. 

With MFM, users can place the dozens of users and their precoding and combining matrices will be computed automatically according to desired strategies. Likewise, all channel and path loss realizations can be handled behind-the-scenes by MFM based on user-specified channel and path loss models. 

MFM is particularly useful for Monte Carlo simulations, where hundreds or thousands of realizations are often needed.

{% include image.html file="network.svg" %}


## Channel and Path Loss Models

MFM supplies users with common channel and path loss models, rather than forcing them to write their own models from scratch. This ensures consistency and allows users to work confidently, without fear they have errors in the implementation of the models. The models that MFM comes supplied with are common in academic research and industry standards.

Consistent construction across channel and path loss models allows MFM users to easily change which channel and path loss models they use, in as little as one line each.


## Antenna Arrays

The `array` object in MFM makes working with arrays easy. Rather than restrict yourself to uniform linear/planar arrays, arbitrary arrays can be constructed in MFM by arranging its elements as desired. Once created, MFM will automatically compute the array response based on the array's geometry. MFM supports 1-D, 2-D, and 3-D array geometries.

In fact, MFM's `array` object on its own is a useful tool for many applications beyond MIMO.

{% include image.html file="feature_array.svg" caption="Array pattern generated from a uniform linear array using MFM." %}


## Hybrid Digital/Analog Beamforming

In addition to fully-digital MIMO, MFM also supports hybrid digital/analog beamforming for massive MIMO, mmWave, and THz communication.

MFM has the ability to enforce finite resolution phase shifters and attenuators---in addition to a lack of amplitude control---and arbitrarily connected analog beamforming networks (e.g., fully-connected, partially-connected, subarray).

{% include image.html file="hybrid_digital_analog.svg" caption="MFM supports hybrid digital/analog beamforming." %}


## Various Use Cases

MFM can be used in a variety of ways and in a variety of capacities. Users only interested in usings its channel models, for example, can do so, whereas other users wanting to use its higher-level capabilities can do so too.


## Continuous Improvement and Maintenance

MFM will continue to be updated, improved, and maintained. Its capabilities will continue to be expanded to meet the needs of the wireless community.

New channel models and path loss models will be incorporated to MFM as wireless research and standards bring them to light.


## Automatically Computing Performance Metrics

MFM is capable of automatically computing performance metrics like mutual information and symbol estimation error based on current realizations and device configurations.

Users of MFM can be confident that the reported metrics have been calculated correctly.


## Documentation

MFM's documentation extends across several forms. 

First, this website features documentation on how to use all of MFM's objects and their functions.

In addition, the guides on this website provide additional tutorials, insight, and clarification on how to use MFM.

Every single function in MFM also comes with inline documentation, meaning users can simply use MATLAB's `help` command to find out more about an object or function of interest. For example, to learn more about the `array` object, executing `help array` in MATLAB's command window will print the following.

```
>> help array
  array Creates an instance of an array object.
  
  Usage:
   obj = array()
   obj = array(N)
   obj = array(N,M)
   obj = array(N,[])
   obj = array(N,M,plane)
   obj = array(N,[],plane)
  
  Args:
   N: (optional) number of elements in the array; or number of
   rows in a planar array if a second argument is passed
   M: (optional) number of columns in a planar array
   plane: (optional) a string specifying the plane to create
   the planar array in (either 'xz', 'xy', or 'yz'); if not
   passed, 'xz' will be used
 
  Returns:
   obj: an array object
 
  Notes:
   obj = array() Creates an empty array object.
 
   obj = array(N) Creates an array object where N array
   elements are uniformly spaced one-half wavelength apart 
   along the x-axis.
 
   obj = array(N,M) Creates an array object representing a 
   uniform planar array comprised of N rows of M elements,
   uniformly spaced one-half wavelength apart 
   in the x-z plane.
```


## Video Tutorials

Video tutorials on MFM are hosted on [YouTube](https://www.youtube.com/playlist?list=PLQZnKEsUslUasrK1omM4gaN91d7N-0pZC). More videos will continue to be added.


## Examples

MFM comes with example scripts to introduce users to its use and to its possible applications. The current list of example scripts are as follows.
- `main/examples/main_example_array.m` Example setup and usage of the antenna array object.
- `main/examples/main_example_channel.m` Example setup of the generic channel object.
- `main/examples/main_example_channel_ray_cluster.m` Example setup and realization of the ray/cluster channel object.
- `main/examples/main_example_channel_rayleigh.m` Example setup and realization of the Rayleigh-faded channel object.

Examples will continue to be added to MFM.


## Customization

All the code behind MFM is available to its user, meaning it can be modified, improved, or supplemented as desired. Come up with a new precoding strategy? Add it to the `transmitter` definition and you can simulate it as if it was native to MFM. Want to try out a new channel model? Create its class definition and you can begin using it. Ultimately, MFM's capabilities are up to its users.

If you are interested in contributing your customizations to MFM, please vist [this page](/contribute/).






