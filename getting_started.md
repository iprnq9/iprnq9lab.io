---
layout: default
title: Getting Started
permalink: getting_started/
---

# Getting Started

To begin using MFM, download or clone [this repository](https://gitlab.com/iprnq9/mfm).

Open MATLAB with your working directory as the root of the MFM folder.

In the Command Window, type `mfm.setup`. This will run the necessary initializations for MFM. 

The command `mfm.setup` will need to be run at the start of each MFM session (i.e., anytime you re-open MATLAB).

That's it. You are ready to use MFM.

{% include youtube.html id='zI_QthXUUF8' %}


## Running Your First Script

After running `mfm.setup`, you are ready to run your first MFM script.

Examples have been provided in the `main/examples/` directory.

Open the script `main/examples/main_example_01.m`.

This example script follows the guide [A Complete Network-Level Example](/guides/a_complete_network_level_example/).

Take a look through the script and guide to familiarize yourself with what is going on.

When ready, hit `Run`.














