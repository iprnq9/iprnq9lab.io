---
layout: default
title: Guides
permalink: guides/
published: true
---


# Guides

Below are a list of how-to's, examples, and the like to help you better understand and use MFM.


{% assign guides = (site.guides | sort: 'date') | reverse %}
{% for p in guides %}
[**{{ p.title }}**]({{ p.url }})  
{% if p.summary %}_{{ p.summary }}_{% endif %}  
{% if p.objects %}{% assign objects = (p.objects) %}Objects: {% for object in objects %}[{{ object }}](/docs/{{ object }}) &nbsp;{% endfor %}
{% endif %}
{% endfor %}
