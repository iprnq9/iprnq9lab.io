---
title: MIMO for MATLAB
layout: default
---

![MIMO for MATLAB](/images/mimo_cover.svg#center, "MIMO for MATLAB")

## Updates to MFM!

Significant updates to MFM have been incorporated as of January 1, 2021, and are ongoing!

These changes to MFM aim to make it more user-friendly and flexible while correcting mistakes and improving its architecture. In addition, documentation, guides, and examples for MFM have been drastically improved. These supplementary materials are provided on this website and will continue to be supplied in various forms, including video tutorials.

## About

MIMO for MATLAB (MFM) is a suite of scripts, utilities, and models for simulating multiple-input multiple-output (MIMO) communication in MATLAB. 

MFM implements conventional MIMO signal processing at the symbol level, somtimes termed "single letter formulations". In short, MFM is a tool for executing the linear algebra involved in MIMO system design, simulation, and evaluation. While simulating simple MIMO systems is typically straightforward, simulation can become prohibitively complex and overwhelming when systems grow to even a moderate size.

In addition, MFM hopes to improve the reproducibility of MIMO research by providing a common framework for researchers to use when implementing their work. Out-of-the-box, MFM is equipped with a variety of widely used channel and path loss models. If a user needs particular channel or path loss model that is not included in MFM, custom objects that integrate with MFM can be created independently by users by following a few simple rules. These third-party customizations to MFM can then be shared within the research community for others to use and potentially integrated into MFM itself.

The initial motivation for MFM was to be a framework that could accelerate my ([Ian P. Roberts](https://ianproberts.com)) graduate research. Hoping that others may benefit from MFM, I decided to polish its code and release it to the public. Beyond research, MFM's potential also lay in educating students on MIMO. It could offer students and educators a tool to better explore MIMO concepts numerically, mathematically, and algorithmically.


## Download

The latest version of MFM can be cloned via

```
git clone git@gitlab.com:iprnq9/mfm.git
```

or downloaded from [this GitLab repository](https://gitlab.com/iprnq9/mfm).


## Questions and Feedback

Feel free to email {% include email.html %} with bug reports, questions, and feedback (including suggestions!).

Also, if you have created a custom object or add-on (e.g., a channel model) that you think could be useful to others using MFM, please also feel free to reach out regarding such.


## Citing MFM

It will be useful to track the reach and applications of MFM. If you use MFM, please cite it.

```
@misc{mfm,
  author = {Ian P. Roberts},
  title = { {MIMO} for {MATLAB}: A  Toolbox for Simulating {MIMO} 
            Communication Systems in {MATLAB} },
  howpublished = {\url{http://mimoformatlab.com}},
  month = jan,
  year = 2021
}
```


## License

MFM is protected by copyright and is released under the [MIT license](https://gitlab.com/iprnq9/mfm/-/blob/master/LICENSE).


## Acknowledgment

The development of MFM is supported by the National Science Foundation Graduate Research Fellowship Program (Grant No. DGE-1610403) awarded to [Ian P. Roberts](https://ianproberts.com) at the University of Texas at Austin.
Any opinions, findings, and conclusions or recommendations expressed in this material are those of the author(s) and do not necessarily reflect the views of the National Science Foundation.


## Suggested References on MIMO Communication

- _[Foundations of MIMO Communication](https://www.cambridge.org/core/books/foundations-of-mimo-communication/D1D999D61E48C62C44240EF2341A29A1)_ by Heath & Lozano
- _[An Overview of Signal Processing Techniques for Millimeter Wave MIMO Systems](https://arxiv.org/pdf/1512.03007.pdf)_ by Heath et al.
